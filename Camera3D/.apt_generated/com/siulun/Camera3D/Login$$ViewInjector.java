// Generated code from Butter Knife. Do not modify!
package com.siulun.Camera3D;

import android.view.View;
import butterknife.Views.Finder;

public class Login$$ViewInjector {
  public static void inject(Finder finder, final com.siulun.Camera3D.Login target, Object source) {
    View view;
    view = finder.findById(source, 2131165294);
    view.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        target.google();
      }
    });
    view = finder.findById(source, 2131165296);
    view.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        target.twitter();
      }
    });
    view = finder.findById(source, 2131165295);
    view.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        target.facebook();
      }
    });
  }

  public static void reset(com.siulun.Camera3D.Login target) {
  }
}
