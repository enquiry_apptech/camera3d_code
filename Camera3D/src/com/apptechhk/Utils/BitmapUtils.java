package com.apptechhk.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HttpContext;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;

public class BitmapUtils {
	private static final String TAG = "BitmapUtils";

	// TODO: for concurrent connections, DefaultHttpClient isn't great, consider
	// other options
	// that still allow for sharing resources across bitmap fetches.

	public static interface OnFetchCompleteListener {
		public void onFetchComplete(Object cookie, Bitmap result);
	}

	/**
	 * Only call this method from the main (UI) thread. The
	 * {@link OnFetchCompleteListener} callback be invoked on the UI thread, but
	 * image fetching will be done in an {@link AsyncTask}.
	 */
	public static void fetchImage(final Context context, final String url,
			final String key, final String pos,
			final OnFetchCompleteListener callback) {
		fetchImage(context, url, key, pos, null, null, callback);
	}

	/**
	 * Only call this method from the main (UI) thread. The
	 * {@link OnFetchCompleteListener} callback be invoked on the UI thread, but
	 * image fetching will be done in an {@link AsyncTask}.
	 * 
	 * @param cookie
	 *            An arbitrary object that will be passed to the callback.
	 */
	public static void fetchImage(final Context context, final String url,
			final String cacheKey, final String pos,
			final BitmapFactory.Options decodeOptions, final Object cookie,
			final OnFetchCompleteListener callback) {
		new AsyncTask<String, Void, Bitmap>() {
			@Override
			protected Bitmap doInBackground(String... params) {
				final String str_url = params[0];
				final String cacheKey = params[1];
				final String pos = params[2];
				if (TextUtils.isEmpty(url)) {
					return null;
				}

				// First compute the cache key and cache file path for this URL
				File cacheFile = null;
				if (Environment.MEDIA_MOUNTED.equals(Environment
						.getExternalStorageState())) {
					cacheFile = new File(
							Environment.getExternalStorageDirectory()
									+ File.separator + "Android"
									+ File.separator + "data" + File.separator
									+ context.getPackageName() + File.separator
									+ "cache" + File.separator + "map_"
									+ cacheKey + "_" + pos + ".tmp");
				}

				if (cacheFile != null && cacheFile.exists()) {
					Bitmap cachedBitmap = BitmapFactory.decodeFile(
							cacheFile.toString(), decodeOptions);
					if (cachedBitmap != null) {
						return cachedBitmap;
					}
				}

				try {
					URL url = new URL(str_url);
					byte[] respBytes = getUrlByte(url);

					// Write response bytes to cache.
					if (cacheFile != null) {
						try {
							cacheFile.getParentFile().mkdirs();
							cacheFile.createNewFile();
							FileOutputStream fos = new FileOutputStream(
									cacheFile);
							fos.write(respBytes);
							fos.close();
						} catch (FileNotFoundException e) {
							Log.w(TAG, "Error writing to bitmap cache: "
									+ cacheFile.toString(), e);
						} catch (IOException e) {
							Log.w(TAG, "Error writing to bitmap cache: "
									+ cacheFile.toString(), e);
						}
					}

					// Decode the bytes and return the bitmap.
					return BitmapFactory.decodeByteArray(respBytes, 0,
							respBytes.length, decodeOptions);
				} catch (Exception e) {
					Log.w(TAG, "Problem while loading image: " + e.toString(),
							e);
				}
				return null;
			}

			@Override
			protected void onPostExecute(Bitmap result) {
				callback.onFetchComplete(cookie, result);
			}
		}.execute(url, cacheKey, pos);
	}

	static byte[] getUrlByte(URL url) throws IOException {
		OkHttpClient client = new OkHttpClient();

		HttpURLConnection connection = client.open(url);
		InputStream in = null;
		byte[] respBytes;
		try {
			// Read the response.
			in = connection.getInputStream();
			respBytes = readFully(in);
			return respBytes;
		} finally {
			if (in != null)
				in.close();
		}
	}

	static byte[] readFully(InputStream in) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		for (int count; (count = in.read(buffer)) != -1;) {
			out.write(buffer, 0, count);
		}
		return out.toByteArray();
	}

	// private static String bytesToHexString(byte[] bytes) {
	// // http://stackoverflow.com/questions/332079
	// StringBuffer sb = new StringBuffer();
	// for (int i = 0; i < bytes.length; i++) {
	// String hex = Integer.toHexString(0xFF & bytes[i]);
	// if (hex.length() == 1) {
	// sb.append('0');
	// }
	// sb.append(hex);
	// }
	// return sb.toString();
	// }

	private static final int SECOND_IN_MILLIS = (int) DateUtils.SECOND_IN_MILLIS;
	private static final String HEADER_ACCEPT_ENCODING = "Accept-Encoding";
	private static final String ENCODING_GZIP = "gzip";

	/**
	 * Generate and return a {@link HttpClient} configured for general use,
	 * including setting an application-specific user-agent string.
	 */
	public static HttpClient getHttpClient(Context context) {
		final HttpParams params = new BasicHttpParams();

		// Use generous timeouts for slow mobile networks
		HttpConnectionParams
				.setConnectionTimeout(params, 20 * SECOND_IN_MILLIS);
		HttpConnectionParams.setSoTimeout(params, 20 * SECOND_IN_MILLIS);

		HttpConnectionParams.setSocketBufferSize(params, 8192);
		HttpProtocolParams.setUserAgent(params, buildUserAgent(context));

		final DefaultHttpClient client = new DefaultHttpClient(params);

		client.addRequestInterceptor(new HttpRequestInterceptor() {
			public void process(HttpRequest request, HttpContext context) {
				// Add header to accept gzip content
				if (!request.containsHeader(HEADER_ACCEPT_ENCODING)) {
					request.addHeader(HEADER_ACCEPT_ENCODING, ENCODING_GZIP);
				}
			}
		});

		client.addResponseInterceptor(new HttpResponseInterceptor() {
			public void process(HttpResponse response, HttpContext context) {
				// Inflate any responses compressed with gzip
				final HttpEntity entity = response.getEntity();
				final Header encoding = entity.getContentEncoding();
				if (encoding != null) {
					for (HeaderElement element : encoding.getElements()) {
						if (element.getName().equalsIgnoreCase(ENCODING_GZIP)) {
							response.setEntity(new InflatingEntity(response
									.getEntity()));
							break;
						}
					}
				}
			}
		});

		return client;
	}

	/**
	 * Build and return a user-agent string that can identify this application
	 * to remote servers. Contains the package name and version code.
	 */
	private static String buildUserAgent(Context context) {
		try {
			final PackageManager manager = context.getPackageManager();
			final PackageInfo info = manager.getPackageInfo(
					context.getPackageName(), 0);

			// Some APIs require "(gzip)" in the user-agent string.
			return info.packageName + "/" + info.versionName + " ("
					+ info.versionCode + ") (gzip)";
		} catch (NameNotFoundException e) {
			return null;
		}
	}

	/**
	 * Simple {@link HttpEntityWrapper} that inflates the wrapped
	 * {@link HttpEntity} by passing it through {@link GZIPInputStream}.
	 */
	private static class InflatingEntity extends HttpEntityWrapper {
		public InflatingEntity(HttpEntity wrapped) {
			super(wrapped);
		}

		@Override
		public InputStream getContent() throws IOException {
			return new GZIPInputStream(wrappedEntity.getContent());
		}

		@Override
		public long getContentLength() {
			return -1;
		}
	}

}
