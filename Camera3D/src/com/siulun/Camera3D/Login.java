package com.siulun.Camera3D;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FacebookApi;
import org.scribe.builder.api.GoogleApi;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import com.actionbarsherlock.view.Menu;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.google.analytics.tracking.android.EasyTracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends Activity {
	WebView webView;
	LinearLayout mWebViewContainer;
	LinearLayout mLoginContainer;
	RelativeLayout mSpinner;
	ProgressBar mPbar;
	Button mFBButton,mTwitterButton,mGoogleButton,mTipsButton,mAboutButton,mEmailButton;
	
	public static final int DIALOG_MENU_HELP = 0;
	public static final int DIALOG_MENU_ABOUT = 1;

	protected static final int MENU_LOGIN = Menu.FIRST;
	protected static final int MENU_HELP = Menu.FIRST + 1;
	protected static final int MENU_ABOUT = Menu.FIRST + 2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.login);
//	    Views.inject(this);
		AdView adView = (AdView) this.findViewById(R.id.adView);
		adView.loadAd(new AdRequest());
		
		webView = (WebView) findViewById(R.id.webView);
		mWebViewContainer = (LinearLayout) findViewById(R.id.webViewContainer);
		mLoginContainer = (LinearLayout) findViewById(R.id.login);
		mSpinner = (RelativeLayout) findViewById(R.id.dashboard_spinner);
		mPbar = (ProgressBar) findViewById(R.id.pbar);
		
		mTipsButton = (Button) findViewById(R.id.tips);
		mTipsButton.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			showDialog(DIALOG_MENU_HELP);
		}
		});	
		mAboutButton = (Button) findViewById(R.id.about);
		mAboutButton.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			showDialog(DIALOG_MENU_ABOUT);
		}
		});	
		
		mFBButton = (Button) findViewById(R.id.btn_facebook);
		mFBButton.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			facebook();
		}
		});	
		
		mEmailButton = (Button) findViewById(R.id.btn_email);
		mEmailButton.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent i = new Intent(Login.this, EmailRegister.class);
			startActivityForResult(i, LOGIN_REQUEST);
		}
		});	
		
		mTwitterButton = (Button) findViewById(R.id.btn_twitter);
		mTwitterButton.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			twitter();
		}
		});	
		
		mGoogleButton = (Button) findViewById(R.id.btn_google);
		mGoogleButton.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			google();
		}
		});	
	}
	
	final static int LOGIN_REQUEST = 1001;
	public final static String LOGIN_OK = "login_ok";
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    // Check which request we're responding to
	    if (requestCode == LOGIN_REQUEST) {
	        // Make sure the request was successful
	        if (resultCode == RESULT_OK) {
	            if (data.getBooleanExtra(LOGIN_OK, false)) {
	            	setResult(RESULT_OK);
	            	finish();
	            }
	        }
	    }
	}
	
	private void updateUi(int status) {
		switch (status) {
		case 0:
			mLoginContainer.setVisibility(View.GONE);
			mSpinner.setVisibility(View.GONE);
			mWebViewContainer.setVisibility(View.VISIBLE);
			break;
		case 1:
			mLoginContainer.setVisibility(View.GONE);
			mSpinner.setVisibility(View.VISIBLE);
			mWebViewContainer.setVisibility(View.GONE);
			break;
		case 2:
			mLoginContainer.setVisibility(View.VISIBLE);
			mSpinner.setVisibility(View.GONE);
			mWebViewContainer.setVisibility(View.GONE);
			break;			
		}
	}

	SharedPreferences settings;
	OAuthService service;
	Token requestToken;
	final Token EMPTY_TOKEN = null;
	final String callbackUrl = "http://camera3d.apptech.com.hk/";

	// final static String twitterCallbackUrl = "oauth://twitter";

	public void facebook() {
		updateUi(0);

		final String NETWORK_NAME = "Facebook";
		final Token EMPTY_TOKEN = null;

		// final String PROTECTED_RESOURCE_URL =
		// "https://graph.facebook.com/me";
		String apiKey = "474574509298139";
		String apiSecret = "bce76ca13f2d4f191fdbf3728e24e30d";
		service = new ServiceBuilder().provider(FacebookApi.class)
				.apiKey(apiKey).apiSecret(apiSecret).callback(callbackUrl)
				.scope("email").build();

		final String authURL = service.getAuthorizationUrl(EMPTY_TOKEN);
		initWebView(NETWORK_NAME, authURL);
	}

	public void google() {

		final String AUTHORIZE_URL = "https://www.google.com/accounts/OAuthAuthorizeToken?oauth_token=";
		updateUi(0);
		final String NETWORK_NAME = "Google";
		final String SCOPE = "https://docs.google.com/feeds/";

		service = new ServiceBuilder().provider(GoogleApi.class)
				.apiKey("anonymous").apiSecret("anonymous").scope(SCOPE)
				.build();

		new AsyncTask<String, Void, Void>() {
			@Override
			protected Void doInBackground(String... params) {
				requestToken = service.getRequestToken();
				return null;
			}

			protected void onPostExecute(Void result) {
				final String authURL = service
						.getAuthorizationUrl(requestToken);
				initWebView(NETWORK_NAME, authURL);
			};

		}.execute();
	}

	public void twitter() {
		updateUi(0);

		final String NETWORK_NAME = "Twitter";
		// final Token EMPTY_TOKEN = null;

		// final String PROTECTED_RESOURCE_URL =
		// "https://graph.facebook.com/me";
		String apiKey = "tfAdqUhdQewbbW74rclbw";
		String apiSecret = "C0WgycYs7ZisdbFulVF8Q7MrpQZyAq5FZy41WCIc8";
		// service = new ServiceBuilder().provider(FacebookApi.class)
		// .apiKey(apiKey).apiSecret(apiSecret).callback(callbackUrl)
		// .scope("email")
		// .build();
		service = new ServiceBuilder().provider(TwitterApi.class)
				.apiKey(apiKey).apiSecret(apiSecret).callback(callbackUrl)
				.build();

		new AsyncTask<String, Void, Void>() {
			@Override
			protected Void doInBackground(String... params) {
				requestToken = service.getRequestToken();
				return null;
			}

			protected void onPostExecute(Void result) {
				final String authURL = service
						.getAuthorizationUrl(requestToken);
				initWebView(NETWORK_NAME, authURL);
			};

		}.execute();

		// new AsyncTask<Void, Void, Void>() {
		// @Override
		// protected Void doInBackground(Void... voids) {
		//
		// OAuthService service = new ServiceBuilder()
		// .provider(TwitterApi.class)
		// .apiKey("tfAdqUhdQewbbW74rclbw")
		// .apiSecret("C0WgycYs7ZisdbFulVF8Q7MrpQZyAq5FZy41WCIc8")
		// .build();
		// Scanner in = new Scanner(System.in);
		//
		// System.out.println("=== Twitter's OAuth Workflow ===");
		// System.out.println();
		//
		// // Obtain the Request Token
		// System.out.println("Fetching the Request Token...");
		// Token requestToken = service.getRequestToken();
		// System.out.println("Got the Request Token!");
		// System.out.println();
		//
		// System.out.println("Now go and authorize Scribe here:");
		// System.out.println(service.getAuthorizationUrl(requestToken));
		// System.out.println("And paste the verifier here");
		// System.out.print(">>");
		// String val = new String();
		// if (in.hasNextLine())
		// val = in.nextLine();
		// System.out.println("VALUE" + val);
		// Verifier verifier = new Verifier(val);
		// System.out.println();
		// // Verifier verifier = new Verifier(in.nextLine());
		// // System.out.println();
		//
		// // Trade the Request Token and Verfier for the Access Token
		// System.out
		// .println("Trading the Request Token for an Access Token...");
		// Token accessToken = service.getAccessToken(requestToken,
		// verifier);
		// System.out.println("Got the Access Token!");
		// System.out.println("(if your curious it looks like this: "
		// + accessToken + " )");
		// System.out.println();
		//
		// // Now let's go and ask for a protected resource!
		// System.out
		// .println("Now we're going to access a protected resource...");
		// OAuthRequest request = new OAuthRequest(Verb.POST,
		// PROTECTED_RESOURCE_URL);
		// request.addBodyParameter("status", "this is sparta! *");
		// service.signRequest(accessToken, request);
		// Response response = request.send();
		// System.out.println("Got it! Lets see what we found...");
		// System.out.println();
		// System.out.println(response.getBody());
		//
		// System.out.println();
		// System.out
		// .println("Thats it man! Go and build something awesome with Scribe! :)");
		// return null;
		// }
		//
		// protected void onPostExecute(Void result) {
		// };
		//
		// }.execute();
	}

	private void initWebView(final String network, final String authURL) {
		webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) 
               {
               if(progress < 100 && mPbar.getVisibility() == ProgressBar.GONE){
            	   mPbar.setVisibility(ProgressBar.VISIBLE);
               }
               mPbar.setProgress(progress);
               if(progress == 100) {
            	   mPbar.setVisibility(ProgressBar.GONE);
               }
            }
        });
		webView.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {

				// Log.i("--- URL: " + url);

				// check for our custom callback protocol
				// otherwise use default behavior
				// String callback = "";
				// if (network.equals("Facebook")) {
				// callback = callbackUrl;
				// } else if (network.equals("Twitter")) {
				// callback = twitterCallbackUrl;
				// }

				if (url.startsWith(callbackUrl)
						|| url.startsWith("https://accounts.google.com/OAuthAuthorizeToken")) {
					// authorization complete hide webview for now.
					// updateUi(false);
					Uri uri = Uri.parse(url);
					updateUi(1);

					new AsyncTask<Uri, Void, String>() {
						@Override
						protected String doInBackground(Uri... params) {
							final Uri uri = params[0];
							if (network.equals("Facebook")) {
								return verifyFacebook(uri);
							} else if (network.equals("Twitter")) {
								verifyTwitter(uri);
							} else if (network.equals("Google")) {
								verifyGoogle(uri);
							}
							return null;
						}

						protected void onPostExecute(String result) {
							JSONObject json;
							try {
								json = (JSONObject) new JSONTokener(result).nextValue();
								if (json.get("status").toString().equals("done")) {
									Utils.setBoolean(Login.this, "isLogin", true);
									Utils.setString(Login.this, "userId", (String) json.get("id"));
									Toast.makeText(getApplicationContext(),"Login Success!",Toast.LENGTH_LONG).show();
									setResult(RESULT_OK);
									finish();
								} else {
									Toast.makeText(getApplicationContext(), (String) json.get("msg"), Toast.LENGTH_LONG).show();
								}
							} catch (NullPointerException e){
								e.printStackTrace();
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (ClassCastException e){}
							
//							if (mRegisterStatus.equals("done")) {
//							} else {
//							}
							updateUi(2);
						};

					}.execute(uri);

					return true;
				}
				return super.shouldOverrideUrlLoading(view, url);
			}
		});

		// send user to authorization page
		webView.loadUrl(authURL);
	}

	private void verifyGoogle(Uri uri) {
		final String PROTECTED_RESOURCE_URL = "https://docs.google.com/feeds/default/private/full/";
		String verifier = uri.getQueryParameter("oauth_token");
		Verifier v = new Verifier(verifier);

		// Token requestToken2 = new Token(
		// "1526637223-YqGTB8NVjc80Jzc0WkENr1OLe7K1wCAvHHP0O7M","zFpvgYb7B5X4VPxCIH55mNJ5qaC4hfuHJbNlcLaA");

		// save this token for practical use.
		Token accessToken = service.getAccessToken(requestToken, v);

		OAuthRequest request = new OAuthRequest(Verb.GET,
				PROTECTED_RESOURCE_URL);
		service.signRequest(accessToken, request);
		request.addHeader("GData-Version", "3.0");
		Response response = request.send();

		// xmlHandler xh = new xmlHandler(response.getBody());

		System.out.println("Got it! Lets see what we found...");
		System.out.println();
		System.out.println(response.getCode());
		System.out.println(response.getBody());

		System.out.println();
		System.out
				.println("Thats it man! Go and build something awesome with Scribe! :)");
	}

	private void verifyTwitter(Uri uri) {
		final String PROTECTED_RESOURCE_URL = "http://api.twitter.com/1.1/account/verify_credentials.json";
		// final String PROTECTED_RESOURCE_URL =
		// "https://api.twitter.com/1.1/statuses/update.json?include_entities=true";
		// final String PROTECTED_RESOURCE_URL =
		// "https://api.twitter.com/1/statuses/update.json";
		// http://api.twitter.com/1/account/verify_credentials.json")
		String verifier = uri.getQueryParameter("oauth_verifier");
		Verifier v = new Verifier(verifier);

		// Token requestToken2 = new Token(
		// "1526637223-YqGTB8NVjc80Jzc0WkENr1OLe7K1wCAvHHP0O7M","zFpvgYb7B5X4VPxCIH55mNJ5qaC4hfuHJbNlcLaA");

		// save this token for practical use.
		Token accessToken = service.getAccessToken(requestToken, v);

		OAuthRequest request = new OAuthRequest(Verb.GET,
				PROTECTED_RESOURCE_URL);
		service.signRequest(accessToken, request);
		Response response = request.send();

		// xmlHandler xh = new xmlHandler(response.getBody());

		System.out.println("Got it! Lets see what we found...");
		System.out.println();
		System.out.println(response.getCode());
		System.out.println(response.getBody());

		System.out.println();
		System.out
				.println("Thats it man! Go and build something awesome with Scribe! :)");
	}

	private String verifyFacebook(Uri uri) {
		final String PROTECTED_RESOURCE_URL = "https://graph.facebook.com/me";

		try {
			
			String verifier = uri.getQueryParameter("code");
			Verifier v = new Verifier(verifier);
	
			// save this token for practical use.
			Token accessToken = service.getAccessToken(requestToken, v);
	
			OAuthRequest request = new OAuthRequest(Verb.GET,
					PROTECTED_RESOURCE_URL);
			service.signRequest(accessToken, request);
			Response response = request.send();
	
			// xmlHandler xh = new xmlHandler(response.getBody());
	
	//		System.out.println("Got it! Lets see what we found...");
	//		System.out.println();
	//		System.out.println(response.getCode());
	//		System.out.println(response.getBody());
	//
	//		System.out.println();
	//		System.out
	//				.println("Thats it man! Go and build something awesome with Scribe! :)");
	
			return perpareDataToServer(accessToken, 1, response.getBody());
		} catch (RuntimeException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private String perpareDataToServer(Token token, int provider, String result) {
		try {
			JSONObject json = (JSONObject) new JSONTokener(result).nextValue();
			try {

				Utils.setInt(Login.this, "provider", provider);
				Utils.setString(Login.this, "provider_uid", (String) json.get("id"));
				Utils.setString(Login.this, "display_name", (String) json.get("name"));
				Utils.setString(Login.this, "first_name", (String) json.get("first_name"));
				Utils.setString(Login.this, "last_name", (String) json.get("last_name"));
				Utils.setString(Login.this, "email", (String) json.get("email"));
				Utils.setString(Login.this, "access_token", token.getToken());

				return sendToServer(Camera3D.SERVER_REGISTER_USER, provider, token.getToken(),
						token.getSecret(), (String) json.get("id"),
						(String) json.get("name"),
						URLEncoder.encode((String) json.get("email"), "UTF-8"),
						(String) json.get("first_name"),
						(String) json.get("last_name"), "");

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String sendToServer(String url, int provider, String token,
			String secret, String provider_uid, String name, String email,
			String first_name, String last_name, String password) throws IOException {
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
				2);
		nameValuePairs.add(new BasicNameValuePair("provider", "" + provider));
		nameValuePairs
				.add(new BasicNameValuePair("provider_uid", provider_uid));
		nameValuePairs.add(new BasicNameValuePair("token", token));
		nameValuePairs.add(new BasicNameValuePair("secret", secret));
		nameValuePairs.add(new BasicNameValuePair("name", name));
		nameValuePairs.add(new BasicNameValuePair("email", email));
		nameValuePairs.add(new BasicNameValuePair("first_name", first_name));
		nameValuePairs.add(new BasicNameValuePair("last_name", last_name));
		nameValuePairs.add(new BasicNameValuePair("password", password));

		try {
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();

			String str = Utils.convertStreamToString(entity.getContent());

			return str;
		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}
		return null;
	}
	
	@Override
	public Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_MENU_ABOUT:
			final TextView message = new TextView(this);
			SpannableString s = new SpannableString(
					this.getText(R.string.dialog_about_1));
			Linkify.addLinks(s, Linkify.EMAIL_ADDRESSES);
			message.setText(s);
			message.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
			s = new SpannableString(this.getText(R.string.dialog_about_2));
			Linkify.addLinks(s, Linkify.WEB_URLS);
			message.append(s);
			s = new SpannableString(this.getText(R.string.dialog_about_3));
			message.append(s);
			message.setMovementMethod(LinkMovementMethod.getInstance());
			message.setPadding(20, 20, 20, 20);
			return new AlertDialog.Builder(this)
					.setTitle(R.string.dialog_about)
					// .setMessage(R.string.dialog_about_detail)
					.setView(message)
					.setPositiveButton(R.string.OK,
							new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface dialoginterface, int i) {
									dialoginterface.cancel();
								}
							}).show();
		case DIALOG_MENU_HELP:
			try {
				InputStream is = getAssets().open("help.txt");
				int size = is.available();
				byte[] buffer = new byte[size];
				is.read(buffer);
				is.close();
				// Convert the buffer into a string.
				String text = new String(buffer);
				return new AlertDialog.Builder(this)
						.setTitle(R.string.dialog_instructions)
						.setMessage(text)
						.setPositiveButton(R.string.OK,
								new DialogInterface.OnClickListener() {
									public void onClick(
											DialogInterface dialoginterface,
											int i) {
										dialoginterface.cancel();
									}
								}).show();
			} catch (IOException e) {
				// Should never happen!
				throw new RuntimeException(e);
			}
		}
		return null;
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance().activityStop(this); // Add this method.
	}
}
