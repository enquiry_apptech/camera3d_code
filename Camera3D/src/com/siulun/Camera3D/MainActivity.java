package com.siulun.Camera3D; 

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.google.analytics.tracking.android.EasyTracker;
import com.siulun.Camera3D.adapter.FeedViewPrivateAdapter;
import com.siulun.Camera3D.adapter.FeedViewPublicAdapter;
import com.siulun.Camera3D.model.FeedSet;

public class MainActivity extends SherlockFragmentActivity implements ActionBar.TabListener, FeedViewPublicFragment.Callbacks,FeedViewPrivateFragment.Callbacks {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide fragments for each of the
     * three primary sections of the app. We use a {@link android.support.v4.app.FragmentPagerAdapter}
     * derivative, which will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */ 
	public static Class CLASS;
	int LOGIN_OUT = 1;
	private int currentPage;
    static AppSectionsPagerAdapter mAppSectionsPagerAdapter;
    ListView listview;
	String mFeedType;
	FeedViewPrivateAdapter mAdapter;
	FeedViewPublicAdapter mPublicAdapter;
	Boolean mStartScrolling = false;
	float lastX, lastY, distanceX, distanceY;
	ArrayList<FeedSet> mSocialSet = new ArrayList<FeedSet>();
	private final static String welcomeScreenShownPref = "welcomeScreenShownPref_201401070";
	private SharedPreferences mPrefs;
	private ProgressDialog mDialog;
	public static FeedViewPrivateFragment feedViewPrivateFragment;
	private UiLifecycleHelper uiHelper;
	
	/**
     * The {@link ViewPager} that will display the three primary sections of the app, one at a
     * time.
     */
    static CustomViewPager mViewPager;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        uiHelper = new UiLifecycleHelper(this, null);
	    uiHelper.onCreate(savedInstanceState);
        // Create the adapter that will return a fragment for each of the three primary sections
        // of the app.
        FragmentManager fm = getSupportFragmentManager();
        mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager());

        // Set up the action bar.
        final ActionBar actionBar = getSupportActionBar();

        // Specify that the Home/Up button should not be enabled, since there is no hierarchical
        // parent.
        actionBar.setHomeButtonEnabled(false);

        // Specify that we will be displaying tabs in the action bar.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Set up the ViewPager, attaching the adapter and setting up a listener for when the
        // user swipes between sections.
        mViewPager = (CustomViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mAppSectionsPagerAdapter);
        mAppSectionsPagerAdapter.notifyDataSetChanged();
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When swiping between different app sections, select the corresponding tab.
                // We can also use ActionBar.Tab#select() to do this if we have a reference to the
                // Tab.
            	currentPage = position;
            	if (position==1)
            		if (TextUtils.isEmpty(Utils.getString(MainActivity.this.getApplicationContext(), "userId"))) {
            			Toast.makeText(MainActivity.this.getApplicationContext(), "Please login to view uploaded photos.",Toast.LENGTH_LONG).show();
            		}
            	actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < 3; i++) {
            // Create a tab with text corresponding to the page title defined by the adapter.
            // Also specify this Activity object, which implements the TabListener interface, as the
            // listener for when this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mAppSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }
        
        
        File sdCard = Environment.getExternalStorageDirectory();
		String absPath;
		File f = new File(sdCard.getAbsolutePath() + "/sd");
		if (f.exists())
			absPath = sdCard.getAbsolutePath() + "/sd";
		else
			absPath = sdCard.getAbsolutePath();
		File from = new File(absPath+"/Camera3D");
		if (from.isDirectory()) {
			File to = new File(absPath+"/"+Camera3D.FOLDER_NAME);
			from.renameTo(to);
		}
        
        if (Utils.getBoolean(this, "makeSample", true)) {
			copyAssets("thumb");
			copyAssets("sample");
			Utils.setBoolean(this, "makeSample", false);
		}
        
		Intent intent = getIntent();
		mFeedType = intent.getStringExtra("feed_type");
		if (TextUtils.isEmpty(mFeedType)) {
			mFeedType = "";
		}
		listview = (ListView) findViewById(R.id.listView);
		
		
		mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		Boolean welcomeScreenShown = mPrefs.getBoolean(welcomeScreenShownPref,
				false);
		if (!welcomeScreenShown) {
			mDialog = ProgressDialog.show(MainActivity.this, "",
					getText(R.string.dialog_loading), true);
			Thread t = new Thread() {
				public void run() {
					upgradRenameFile();
					mHandler.post(mDialogDismiss);
				}
			};
			t.start();
			try {
				InputStream is = getAssets().open("change_log.txt");
				int size = is.available();
				byte[] buffer = new byte[size];
				is.read(buffer);
				is.close();
				// Convert the buffer into a string.
				String text = new String(buffer);
				final AlertDialog alertDialog = getAlertDialog(
						getText(R.string.dialog_log_title), text);
				alertDialog.show();
			} catch (IOException e) {
				// Should never happen!
				throw new RuntimeException(e);
			}
			
			//Promt the user to login
			if (TextUtils.isEmpty(Utils.getString(MainActivity.this.getApplicationContext(), "userId"))) {
			new AlertDialog.Builder(this)
			.setTitle(getString(R.string.str_login))
			.setMessage(getString(R.string.dialog_login_prompt))
			.setPositiveButton(android.R.string.ok,
			new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface d, int w) {
			Intent i = new Intent(MainActivity.this, Login.class);
			startActivity(i);
			}

			}).setNegativeButton(android.R.string.cancel, null)
			.show();
			}
		}
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
     * sections of the app.
     */
    public static class AppSectionsPagerAdapter extends FragmentStatePagerAdapter {
    	private Context context;
        public AppSectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    // The first section of the app is the most interesting -- it offers
                    // a launchpad into the other demonstrations in this example application.
                    return new FeedViewPublicFragment();
                case 1:
                	feedViewPrivateFragment = new FeedViewPrivateFragment();
                	return feedViewPrivateFragment;
                case 2:
                	return new GalleryFragment();
                	
                default:
                    // The other sections of the app are dummy placeholders.
                    Fragment fragment = new FeedViewPublicFragment();
                    return fragment;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
        
        @Override
        public int getItemPosition(Object item) {
        	if(CLASS!=null && item.getClass()==CLASS) 
        		return POSITION_NONE;
        	else
        		return super.getItemPosition(item);
        }

        @Override
        public CharSequence getPageTitle(int position) {
        	if (position == 0)
        		return "Feed";
        	else if (position == 1)
        		return "My Upload";
        	else if (position == 2)
        		return "Local";
        	return "Camera3D";
        }
    }


	@Override
	public void onTabSelected(Tab tab,
			android.support.v4.app.FragmentTransaction ft) {
		// TODO Auto-generated method stub
		 mViewPager.setCurrentItem(tab.getPosition());
		
	}

	@Override
	public void onTabUnselected(Tab tab,
			android.support.v4.app.FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabReselected(Tab tab,
			android.support.v4.app.FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this); // Add this method.
		IntentFilter f = new IntentFilter();
		f.addAction(UploadService.UPLOAD_STATE_CHANGED_ACTION);
		registerReceiver(uploadStateReceiver, f);
	}


	private AlertDialog getAlertDialog(CharSequence title, CharSequence message) {
		Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				SharedPreferences.Editor editor = mPrefs.edit();
				editor.putBoolean(welcomeScreenShownPref, true);
				editor.commit();
				dialog.dismiss();
			}
		});
		return builder.create();
	}

	public static final int DIALOG_MENU_HELP = 0;
	public static final int DIALOG_MENU_ABOUT = 1;

	protected static final int MENU_LOGIN = Menu.FIRST + 1 ;
	protected static final int MENU_CAMERA = Menu.FIRST + 2;
	protected static final int MENU_REFRESH = Menu.FIRST;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// menu.add(0, MENU_PREF, 0,
		// getText(R.string.set_preferences)).setIcon(android.R.drawable.ic_menu_preferences);
		menu.add(0, MENU_REFRESH, 0, "Refresh")
				.setIcon(R.drawable.ic_menu_refresh)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		
		menu.add(0, MENU_LOGIN, 0, "Login")
				.setIcon(android.R.drawable.ic_menu_manage)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		
		menu.add(0, MENU_CAMERA, 0, "Camera")
		.setIcon(android.R.drawable.ic_menu_camera)
		.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		switch (item.getItemId()) {
		/*
		 * case MENU_PREF: Intent settingsActivity = new
		 * Intent(getBaseContext(), Preferences.class);
		 * startActivity(settingsActivity); break;
		 */
		case MENU_REFRESH:
			switch (currentPage) {
				case 0:
					CLASS = FeedViewPublicFragment.class; break;
				case 1:
					CLASS = FeedViewPrivateFragment.class; break;
				case 2:
					CLASS = GalleryFragment.class; break;
			}
			mAppSectionsPagerAdapter.notifyDataSetChanged();
			CLASS = null;
			break;
			
		case MENU_LOGIN:
			Intent intent = null;
			if (!TextUtils.isEmpty(Utils.getString(this, "userId"))){
				intent = new Intent(getBaseContext(), Logout.class);
			} else {
				intent = new Intent(getBaseContext(), Login.class);
			}
			startActivityForResult(intent, LOGIN_OUT);
			break;
		case MENU_CAMERA:
			Intent i = null;
			switch (getResources().getConfiguration().orientation) {
				case 1:
					i = new Intent(MainActivity.this, TakePhoto.class);
					break;
				case 2:
					i = new Intent(MainActivity.this, TakePhotoLand.class);
					break;				
			}
			startActivity(i);
			break;
		
		}
		return true;
	}
	
	public void upgradRenameFile() {
		File sdCard = Environment.getExternalStorageDirectory();
		String absPath;
		File f = new File(sdCard.getAbsolutePath() + "/sd");
		if (f.exists())
			absPath = sdCard.getAbsolutePath() + "/sd";
		else
			absPath = sdCard.getAbsolutePath();
		String dirPath = absPath + "/" + Camera3D.FOLDER_NAME;

		f = new File(dirPath);
		File[] files = f.listFiles();
		if (files == null)
			return;

		File newf;
		for (File file : files) {
			if (!file.isDirectory()) {
				if (file.getName().indexOf("_LL.jpg") > 0) {
					newf = new File(file.getAbsolutePath()
							.replace("_LL", "_00"));
					file.renameTo(newf);
				} else if (file.getName().indexOf("_L.jpg") > 0) {
					newf = new File(file.getAbsolutePath().replace("_L", "_01"));
					file.renameTo(newf);
				} else if (file.getName().indexOf("_C.jpg") > 0) {
					newf = new File(file.getAbsolutePath().replace("_C", "_02"));
					file.renameTo(newf);
				} else if (file.getName().indexOf("_R.jpg") > 0) {
					newf = new File(file.getAbsolutePath().replace("_R", "_03"));
					file.renameTo(newf);
				} else if (file.getName().indexOf("_RR.jpg") > 0) {
					newf = new File(file.getAbsolutePath()
							.replace("_RR", "_04"));
					file.renameTo(newf);
				}
			}
		}

		File s;
		f = new File(dirPath);
		files = f.listFiles();
		for (File file : files) {
			if (!file.isDirectory()) {
				if (file.getName().indexOf("_01.jpg") > 0) {
					s = new File(file.getAbsolutePath().replace("_01.jpg",
							"_00.jpg"));
					if (!s.exists()) {
						newf = new File(file.getAbsolutePath().replace(
								"_01.jpg", "_00.jpg"));
						file.renameTo(newf);
						s = new File(file.getAbsolutePath().replace("_01.jpg",
								"_03.jpg"));
						newf = new File(file.getAbsolutePath().replace(
								"_03.jpg", "_01.jpg"));
						s.renameTo(newf);
					}
				}
			}
		}
	}
	
	
	private void copyAssets(String folder) {
        String mAbsPath = Utils.getAbsPath();
		File f = new File(mAbsPath, Camera3D.FOLDER_NAME);
		if (!f.exists()) {
			f.mkdir();
		}
		File thumb_f = new File(mAbsPath, Camera3D.FOLDER_NAME_THUMB);
		if (!thumb_f.exists()) {
			thumb_f.mkdir();
		}
	    AssetManager assetManager = getAssets();
	    String[] files = null;
	    try {
	        files = assetManager.list(folder);
	    } catch (IOException e) {
            e.printStackTrace();
	    }
	    for(String filename : files) {
	        InputStream in = null;
	        OutputStream out = null;
	        try {
	          in = assetManager.open(folder+"/"+filename);
	          File outFile;
	          if (folder.equals("sample"))
	        	  outFile = new File(f, filename);
	          else
	        	  outFile = new File(thumb_f, filename);
	          out = new FileOutputStream(outFile);
	          copyFile(in, out);
	          in.close();
	          in = null;
	          out.flush();
	          out.close();
	          out = null;
	        } catch(IOException e) {
				e.printStackTrace();
	        }       
	    }
	}
	
	private void copyFile(InputStream in, OutputStream out) throws IOException {
	    byte[] buffer = new byte[1024];
	    int read;
	    while((read = in.read(buffer)) != -1){
	      out.write(buffer, 0, read);
	    }
	}
	
	// Create runnable for posting
		final Runnable mDialogDismiss = new Runnable() {
			public void run() {
				mDialog.dismiss();
			}
		};

	private BroadcastReceiver uploadStateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle b = intent.getExtras();
			// status.setText(b.getString(UploadService.MSG_EXTRA));
			int percent = b.getInt(UploadService.PERCENT_EXTRA);
			// progress.setIndeterminate(percent < 0);
			// progress.setProgress(percent);
		}
	};
	
	Handler mHandler = new Handler();

	Runnable mCheckScroll = new Runnable() {
		@Override
		public void run() {
			mHandler.removeCallbacks(mCheckScroll);
			mHandler.postDelayed(mCheckScroll, 300);
			// Log.i("getTouchingStatus : " + mAdapter.getTouchingStatus());
			if (mFeedType.equals("private")) {
				if (mAdapter.getTouchingStatus()) {
					listview.setScrollContainer(false);
				} else {
					mHandler.postDelayed(mRestoreScroll, 600);
				}
			} else {
				if (mPublicAdapter.getTouchingStatus()) {
					listview.setScrollContainer(false);
				} else {
					mHandler.postDelayed(mRestoreScroll, 600);
				}
			}
		}
	};

	Runnable mRestoreScroll = new Runnable() {
		@Override
		public void run() {
			listview.setScrollContainer(true);
		}
	};
	
	@Override
	protected void onResume() {
		super.onResume();
		uiHelper.onResume();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    uiHelper.onSaveInstanceState(outState);
	}
	
	@Override
	public void onPause() {
	    super.onPause();
	    uiHelper.onPause();
	    mHandler.removeCallbacks(mCheckScroll);
		mHandler.removeCallbacks(mRestoreScroll);
		mHandler.removeCallbacks(mDialogDismiss);
	}
	
	@Override
	public void onStop() {
	    super.onStop();
	    unregisterReceiver(uploadStateReceiver);
		EasyTracker.getInstance().activityStop(this); // Add this method.
	    mHandler.removeCallbacks(mCheckScroll);
		mHandler.removeCallbacks(mRestoreScroll);
		mHandler.removeCallbacks(mDialogDismiss);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mHandler.removeCallbacks(mCheckScroll);
		mHandler.removeCallbacks(mRestoreScroll);
		mHandler.removeCallbacks(mDialogDismiss);
		uiHelper.onDestroy();
	};
	

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);

	    if (requestCode == LOGIN_OUT && resultCode == RESULT_OK) {
	    	refresh();
	    }
	    
	    uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
	        @Override
	        public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
	        }

	        @Override
	        public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
	        }
	    });
	}
	
	@Override
	public void onShareButtonClicked(String link,String name) {
		if (FacebookDialog.canPresentShareDialog(getApplicationContext(), 
                FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {
			FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(this)
	        .setLink(link).setName(name)
	        .build();
			uiHelper.trackPendingDialogCall(shareDialog.present());
		}
	}
	
	public void refresh(){
		CLASS = FeedViewPublicFragment.class;
		mAppSectionsPagerAdapter.notifyDataSetChanged();
		CLASS = FeedViewPrivateFragment.class;
		mAppSectionsPagerAdapter.notifyDataSetChanged();
		CLASS = null;
	}
	
	public static void setSwipeable(boolean swipeable){
		mViewPager.setSwipeable(swipeable);
	}
	
}
