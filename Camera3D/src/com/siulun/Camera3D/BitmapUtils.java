package com.siulun.Camera3D;

import java.io.File;

public class BitmapUtils {
    
    public static boolean delete_file(String path) {
    	File file = new File(path);
    	return delete_file(file);
    }
    
    public static boolean delete_file(File file) {
    	boolean deleted = false;
    	if(file.exists()) 
    		deleted = file.delete();
    	return deleted;
    }
    	
}
