package com.siulun.Camera3D;

import java.util.List;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;

public class CameraParameters {
    private static final int FLASH_OFF = 0;
    private static final int FLASH_AUTO = 1;
    private static final int FLASH_ON = 2;
    private static final int FLASH_TORCH = 3;
    
	public static Camera.Parameters setPictureMaxSize(Camera.Parameters parameters)
	{
		List<Camera.Size> supportedSizes = parameters.getSupportedPictureSizes();
		parameters.setPictureSize(supportedSizes.get(0).width, supportedSizes.get(0).height);

		return parameters;	
	}
    
	public static Camera.Parameters setPictureSizes(Camera.Parameters parameters)
	{
		List<Camera.Size> supportedSizes = parameters.getSupportedPictureSizes();					
		int len = supportedSizes.size();
		int w,h;
		for (int x=0; x< len; x++) {
			if (supportedSizes.get(x).width <= 1200) {
				w = supportedSizes.get(x).width;
				h = supportedSizes.get(x).height;
				parameters.setPictureSize(w, h);
				break;
			}
		}

		return parameters;	
	}
	
	public static boolean getFlashSupport(Context c) {
		PackageManager pm = c.getPackageManager();		
        if(pm.hasSystemFeature(PackageManager.FEATURE_CAMERA) && pm.hasSystemFeature("android.hardware.camera.flash")){
        //if(pm.hasSystemFeature(PackageManager.FEATURE_CAMERA) && pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)){
        	return true;
        } else {
        	return false;        	
        }
	}
	
	public static boolean getAutoFocusSupport(Context c) {
		PackageManager pm = c.getPackageManager();		
		if(pm.hasSystemFeature(PackageManager.FEATURE_CAMERA) && pm.hasSystemFeature("android.hardware.camera.autofocus")){
        //if(pm.hasSystemFeature(PackageManager.FEATURE_CAMERA) && pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_AUTOFOCUS)){
        	return true;
        } else {
        	return false;        	
        }
	}

	public static Camera.Parameters setFocusModeInfinity(Camera.Parameters parameters) {
		List<String> fmodes = parameters.getSupportedFocusModes();				
		
		int idx = fmodes.indexOf(Camera.Parameters.FOCUS_MODE_INFINITY);
		if(idx != -1){
			parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_INFINITY);
		}
		return parameters;
	}
	
	public static Camera.Parameters setFocusModeAuto(Camera.Parameters parameters) {
		List<String> fmodes = parameters.getSupportedFocusModes();				
		
		int idx = fmodes.indexOf(Camera.Parameters.FOCUS_MODE_AUTO);
		if(idx != -1){
			parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
		}
		return parameters;
	}
	
	public static Camera.Parameters setFlashMode(Camera.Parameters parameters, int arg) {
		switch (arg) {
			case (FLASH_OFF):
				parameters.setFlashMode(Parameters.FLASH_MODE_OFF);
				break;
			case (FLASH_AUTO):
				parameters.setFlashMode(Parameters.FLASH_MODE_AUTO);
				break;
			case (FLASH_ON):
				parameters.setFlashMode(Parameters.FLASH_MODE_ON);
				break;
			case (FLASH_TORCH):
				parameters.setFlashMode(Parameters.FLASH_MODE_TORCH);
				break;
		}
		return parameters;
	}
	
	public static List<String> getFlashModes(Camera.Parameters parameters){
		List<String> modes = parameters.getSupportedFlashModes();	
		return modes;
	}
	
	public static Size setPreviewSizes(Camera.Parameters parameters, int x, int y)
	{
		List<Size> sizes = parameters.getSupportedPreviewSizes();
		Size optimalSize = getOptimalPreviewSize(sizes, x, y);
		return optimalSize;
	}
	
    private static Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.05;
        double targetRatio = (double) w / h;
        if (sizes == null) return null;

        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        // Try to find an size match aspect ratio and size
        for (Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        // Cannot find the one match the aspect ratio, ignore the requirement
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

}
