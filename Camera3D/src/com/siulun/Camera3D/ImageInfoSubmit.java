package com.siulun.Camera3D;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;
import butterknife.OnClick;
import butterknife.Views;

import com.actionbarsherlock.app.SherlockActivity;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.google.analytics.tracking.android.EasyTracker;

public class ImageInfoSubmit extends SherlockActivity {

	private String mImageId;
	ArrayList<String> files = new ArrayList<String>();
	RelativeLayout mSpinner;
	EditText mTitle;
	EditText mDesc;
	EditText mTag1;
	EditText mTag2;
	EditText mTag3;
	Button mSubmitButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.image_info);
		
		AdView adView = (AdView) this.findViewById(R.id.adView);
		adView.loadAd(new AdRequest());
		
		mSpinner = (RelativeLayout) findViewById(R.id.dashboard_spinner);
		mTitle = (EditText) findViewById(R.id.title);
		mDesc = (EditText) findViewById(R.id.desc);
		mTag1 = (EditText) findViewById(R.id.tag1);
		mTag2 = (EditText) findViewById(R.id.tag2);
		mTag3 = (EditText) findViewById(R.id.tag3);
		
		
		final Intent intent = getIntent();
		String file_path = intent.getStringExtra("file_path");

		file_path = file_path.replace("/.thumbnails", "");
		
		int count = 0;
		boolean done = true;
		File f;
		do {
			f = new File(file_path.replace("_T", "_"
					+ Utils.addZero(count, 2)));
			if (f.exists()) {
				files.add(f.getAbsolutePath());
				done = true;
				count++;
			} else
				done = false;
		} while (done);

		
		
		mSubmitButton = (Button) findViewById(R.id.submit);
		mSubmitButton.setOnClickListener(new View.OnClickListener() {
			
		@Override
		public void onClick(View v) {
			if ( TextUtils.isEmpty(mTitle.getText().toString())) {
				Toast.makeText(getApplicationContext(), "Title cannot be empty.", Toast.LENGTH_LONG).show();
				return;
			}
			
			mSpinner.setVisibility(View.VISIBLE);
			new AsyncTask<Void, Void, String>() {
				@Override
				protected String doInBackground(Void... params) {
					return getImageId(intent);
				}

				protected void onPostExecute(String result) {
//					Log.i(Camera3D.TAG, result);
					JSONObject json;
					try {
						json = (JSONObject) new JSONTokener(result).nextValue();
						if (json.get("status").toString().equals("done")) {
							mImageId = (String) json.get("userId");

				            Intent i = new Intent(ImageInfoSubmit.this, UploadService.class);
				            i.putStringArrayListExtra(UploadService.ARG_FILE_PATH, files);
				            i.putExtra("mImageId", mImageId);
				            startService(i);
				            uploadInfo();
						} else {
							Toast.makeText(getApplicationContext(), "Sorry, fail to upload. Please try again later.", Toast.LENGTH_LONG).show();
							finish();
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				};

			}.execute();
			

		}
	});	
		
	}
	
	private void uploadInfo(){
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				try {
					return sendToServer();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
			}

			protected void onPostExecute(String result) {
//				Log.i(Camera3D.TAG, result);
				JSONObject json;
				try {
					if (!TextUtils.isEmpty(result)) {
						json = (JSONObject) new JSONTokener(result).nextValue();
						if (json.get("status").toString().equals("done")) {
							Toast.makeText(getApplicationContext(), "Uploaded! :)", Toast.LENGTH_LONG).show();
							mSpinner.setVisibility(View.GONE);
							finish();
						} else {
							Toast.makeText(getApplicationContext(), (String) json.get("msg"), Toast.LENGTH_LONG).show();
						}
					} else {
						Toast.makeText(getApplicationContext(), "Sorry,no response...", Toast.LENGTH_LONG).show();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			};

		}.execute();
		
	}

	

	private String sendToServer() throws IOException {
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(Camera3D.SERVER_SET_IMAGE_INFO);

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
				2);
		nameValuePairs.add(new BasicNameValuePair("userId", Utils.getString(ImageInfoSubmit.this, "userId")));
		nameValuePairs.add(new BasicNameValuePair("title", "" + mTitle.getText().toString()));
		nameValuePairs.add(new BasicNameValuePair("desc", mDesc.getText().toString()));
		nameValuePairs.add(new BasicNameValuePair("tag1", mTag1.getText().toString()));
		nameValuePairs.add(new BasicNameValuePair("tag2", mTag2.getText().toString()));
		nameValuePairs.add(new BasicNameValuePair("tag3", mTag3.getText().toString()));
		nameValuePairs.add(new BasicNameValuePair("image_id", mImageId));
		
		try {
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();

			String str = Utils.convertStreamToString(entity.getContent());

			return str;
		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}
		return null;
	}
	
	private String getImageId(Intent intent) {
//		ArrayList<String> filePathList = intent.getStringArrayListExtra(UploadService.ARG_FILE_PATH);
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;

		//Returns null, sizes are in the options variable
//		BitmapFactory.decodeFile(filePathList.get(0), options);
		BitmapFactory.decodeFile(files.get(0), options);
		int width = options.outWidth;
		int height = options.outHeight;
		
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(Camera3D.SERVER_SET_IMAGEID);

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
				2);
		nameValuePairs.add(new BasicNameValuePair("userId", Utils.getString(ImageInfoSubmit.this, "userId")));
		nameValuePairs.add(new BasicNameValuePair("width", ""+width));
		nameValuePairs.add(new BasicNameValuePair("height", ""+height));
		nameValuePairs.add(new BasicNameValuePair("size", ""+files.size()));
		nameValuePairs.add(new BasicNameValuePair("localname", ""+new File(files.get(0)).getName()));

		try {
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();

			String str = Utils.convertStreamToString(entity.getContent());

			return str;
		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}
		return null;
	}


	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance().activityStop(this); // Add this method.
	}

}
