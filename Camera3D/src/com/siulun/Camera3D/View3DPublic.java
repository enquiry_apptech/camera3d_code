package com.siulun.Camera3D;

import java.lang.ref.SoftReference;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.apptechhk.Utils.PicassoUtils;
import com.google.analytics.tracking.android.EasyTracker;

public class View3DPublic extends Activity {
	private ImageView mImageView, mIndicator;
//	private Button mBtnAccel;
	private TextView mFrame,commentBtn;
	RelativeLayout mContentLayout;
	RelativeLayout mSpinner;
	private static Context CONTEXT;
	// private ArrayList<Bitmap> mImage = new ArrayList<Bitmap>();
	private static final SparseArray<SoftReference<Bitmap>> mImage = new SparseArray<SoftReference<Bitmap>>();
	private ArrayList<String> mImagePath = new ArrayList<String>();
	// private Bitmap[] mImage;
	private int mHalf, mImageCount, mSpeed;
	// private boolean mSeekOk = false;
	private String filename;
	private boolean mIsLand;
	private int mCurrent = Camera3D.CENTER;

	private SharedPreferences mPrefs;
	private ProgressDialog mDialog;
	private int mWidth = 480;
	private float mLast = 0, mLastX = 0;
	private final static int BUFFER_COUNT = 30;
	
	int mCurrentFrame = 0, frame, size;
	private final static int TOUCH_DISTANCE = 5;
	public boolean isTouching = false;
	float lastX, lastY, distanceX, distanceY;
	private final static int DEFAULT_DURATION = 100;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		
		
		mIsLand = (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE);
		
		
		setContentView(R.layout.view3d_public);
		
		final Intent i = getIntent();
		
		filename = i.getStringExtra("filename");
		frame = i.getIntExtra("frame", 0);
		size = i.getIntExtra("size", 0);
		mImageCount =  (frame == 0) ? size : frame;
		mHalf = Math.round(mImageCount / 2);
		
		mContentLayout = (RelativeLayout) findViewById(R.id.content);
		mSpinner = (RelativeLayout) findViewById(R.id.dashboard_spinner);
		mFrame = (TextView) findViewById(R.id.mFrame);
		mImageView = (ImageView) findViewById(R.id.ImageView);
		mIndicator = (ImageView) findViewById(R.id.mIndicator);
		commentBtn = (TextView) findViewById(R.id.commentBtn);
		
		
		commentBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(View3DPublic.this, Comment.class);
				intent.putExtra("image_id", i.getStringExtra("image_id"));
				intent.putExtra("url", i.getStringExtra("url"));
				intent.putExtra("filename", filename);
				intent.putExtra("frame", frame);
				intent.putExtra("size", size);
				intent.putExtra("title", i.getStringExtra("title"));
				intent.putExtra("desc", i.getStringExtra("desc"));
				startActivity(intent);
			}
			
		});
		
		
		mImageView.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				distanceX = event.getX() - lastX;
				distanceY = event.getY() - lastY;
				lastX = event.getX();
				lastY = event.getY();

				int action = event.getAction();

				switch (action) {
				case MotionEvent.ACTION_DOWN:
					isTouching = true;
					break;

				case MotionEvent.ACTION_MOVE:
					if (distanceX < -TOUCH_DISTANCE) {
						Log.i("viewNextFrame");
						viewNextFrame();
					} else if (distanceX > TOUCH_DISTANCE) {
						Log.i("viewLastFrame");
						viewLastFrame();
					} 
					
					if (mCurrentFrame < mHalf - mHalf / 2)
						mIndicator.setImageResource(R.drawable.s1);
					else if (mCurrentFrame < mHalf)
						mIndicator.setImageResource(R.drawable.s2);
					else if (mCurrentFrame > mHalf + mHalf / 2)
						mIndicator.setImageResource(R.drawable.s5);
					else if (mCurrentFrame > mHalf)
						mIndicator.setImageResource(R.drawable.s4);
					
					break;

				case MotionEvent.ACTION_UP:
					isTouching = false;
					break;

				}
				return true;
			}
		});

		final String url = i.getStringExtra("url");
		if (!TextUtils.isEmpty(url))
			PicassoUtils.with(this).load(url).into(mImageView);
		
		mHandler.postDelayed(mPlay, 100);
		
		if (mImageCount == 2) {
				   	mIndicator.setVisibility(View.GONE);
			//		AccelerometerManager.stopListening();
			//		Vibration();
		} else if (mImageCount > 2) {
					// Picasso.with(View3D.this).load(new File(mImagePath.get(0)))
					// .resize(500, 500).centerCrop().into(mImageView);
					// mImageView.setImageBitmap(mImage.get(Camera3D.CENTER));
			//		lazyLoadImage(mImageView, 0);
					mIndicator.setImageResource(R.drawable.s3);
					mFrame.setVisibility(View.VISIBLE);
			//		if (AccelerometerManager.isSupported(CONTEXT)) {
			//			mBtnAccel.setOnClickListener(View3DPublic.this);
			//			AccelerometerManager.startListening(CONTEXT, View3DPublic.this);
			//		}
				}
		updateUi(true);
		
	}
		
	


	
	private void updateUi(Boolean status) {
		mContentLayout.setVisibility(status ? View.VISIBLE : View.GONE);
		mSpinner.setVisibility(status ? View.GONE : View.VISIBLE);
	}

	
	protected void onResume() {
		super.onResume();

		mIsLand = (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE);
		if ((getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
				|| (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)) {

			final DisplayMetrics displaymetrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
			final int height = displaymetrics.heightPixels;
			mWidth = displaymetrics.widthPixels;
			if (mWidth > height)
				mWidth = height;
			// mPrefs =
			// getSharedPreferences(Camera3D.MY_PREFS,Context.MODE_PRIVATE);
			mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
			if (mPrefs != null) {
				mSpeed = mPrefs.getInt("speed", DEFAULT_DURATION);
			}

			CONTEXT = this;
//			mFrame.setVisibility(View.GONE);
//			mBtnAccel.setVisibility(View.GONE);

			// Display display = ((WindowManager)
			// getSystemService(Context.WINDOW_SERVICE))
			// .getDefaultDisplay();
			// mWidth = display.getWidth();
			// int height = display.getHeight();

//			updateUi(false);

/*			Thread t = new Thread() {
				public void run() {

					File f;
					int count = 0;
					boolean done = true;
					String newPath;
					do {
						newPath = mFilepath.replace("_T",
								"_" + Utils.addZero(count, 2));
						f = new File(newPath);
						if (f.exists()) {
							if (count < BUFFER_COUNT) {
								mImage.put(count, new SoftReference<Bitmap>(
										decodeFile(newPath, mWidth)));
								// mImage.add(decodeFile(newPath, mWidth));
								// Log.i("Buffer added: " + count);
							}
							// else
							// mImage.add(null);
							mImagePath.add(newPath);
							// mNewPath.add(newPath);
							done = true;
							count++;
						} else
							done = false;
					} while (done);
					mImageCount = count;
					mHalf = Math.round(mImageCount / 2);
					mHandler.postDelayed(mDialogDismiss, 100);
				}
			};
			t.start(); */
		}

	};

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// setContentView(R.layout.myLayout);
	}

	
/*
	// Create runnable for posting
	final Runnable mResponsive = new Runnable() {
		public void run() {
			if (mImageCount < 2) {
		//		Toast.makeText(View3DPublic.this, getText(R.string.toast_no_images),
		//				Toast.LENGTH_LONG).show();
		//		finish();
		//		return;
		   } else if (mImageCount == 2) {
		//		mIndicator.setImageResource(R.drawable.ivib);
			   	mIndicator.setVisibility(View.GONE);
		//		AccelerometerManager.stopListening();
		//		Vibration();
			} else {
				// Picasso.with(View3D.this).load(new File(mImagePath.get(0)))
				// .resize(500, 500).centerCrop().into(mImageView);
				// mImageView.setImageBitmap(mImage.get(Camera3D.CENTER));
		//		lazyLoadImage(mImageView, 0);
				mIndicator.setImageResource(R.drawable.s3);
				mFrame.setVisibility(View.VISIBLE);
		//		if (AccelerometerManager.isSupported(CONTEXT)) {
		//			mBtnAccel.setOnClickListener(View3DPublic.this);
		//			AccelerometerManager.startListening(CONTEXT, View3DPublic.this);
		//		}
			}
			updateUi(true);
		}
	};
*/
/*
	public Bitmap decodeFile(String filename, int maxSize) {
		File file = new File(filename);
		return decodeFile(file, maxSize);
	}

	public Bitmap decodeFile(File file, int maxSize) {
		try {
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(file), null, o);
			int scale = 1;
			if (o.outHeight > maxSize || o.outWidth > maxSize) {
				scale = (int) Math.pow(
						2,
						(int) Math.round(Math.log(maxSize
								/ (double) Math.max(o.outHeight, o.outWidth))
								/ Math.log(0.5)));
			}

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(file), null,
					o2);
		} catch (FileNotFoundException e) {
			Log.e(e.toString());
		} catch (Exception e) {
			Log.e(e.toString());
		}
		return null;
	}

	@Override
	public void onClick(View v) {
		mBtnAccel.setVisibility(View.GONE);
		if (AccelerometerManager.isSupported(CONTEXT)) {
			AccelerometerManager.startListening(CONTEXT, this);
		}
	}
*/
	final Handler mHandler = new Handler();
	
/*	
	private int mSwitchImage = 0;
	final Runnable mVibrate = new Runnable() {
		public void run() {
			switch (mSwitchImage) {
			case (0):
				// Picasso.with(View3D.this).load(new File(mImagePath.get(1)))
				// .resize(500, 500).centerCrop().into(mImageView);
				mImageView.setImageBitmap(mImage.get(1).get());
				mSwitchImage = 1;
				break;
			case (1):
				// Picasso.with(View3D.this).load(new File(mImagePath.get(0)))
				// .resize(500, 500).centerCrop().into(mImageView);
				mImageView.setImageBitmap(mImage.get(0).get());
				mSwitchImage = 0;
				break;
			}
			mHandler.postDelayed(mVibrate, mSpeed);
		}
	};

	// private long mStartTime = 0L;

	private void Vibration() {

		Thread t = new Thread() {
			public void run() {
				mHandler.post(mVibrate);
			}
		};
		t.start();
	}
*/
	

	private int mMSTime;



	private void SetPref() {
		if (mPrefs != null) {
			SharedPreferences.Editor ed = mPrefs.edit();
			ed.putInt("speed", mSpeed);
			ed.commit();
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this); // Add this method.
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		try {
			if (mDialog != null && mDialog.isShowing())
				mDialog.dismiss();
		} catch (Exception e) {
			// nothing
		}
		mHandler.removeCallbacks(mPlay);
	}

	@Override
	public void onStop() {
		super.onStop();
//		mHandler.removeCallbacks(mVibrate);
//		mHandler.removeCallbacks(mResponsive);
//		mHandler.removeCallbacks(mCheckImageLoaded);
//		mHandler.removeCallbacks(mFlingEffect);
		mHandler.removeCallbacks(mPlay);
		EasyTracker.getInstance().activityStop(this); // Add this method.
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		SetPref();
//		if (AccelerometerManager.isListening()) {
//			AccelerometerManager.stopListening();
//		}
//		for (int x = 0; x < mImageCount; x++) {
//			SoftReference<Bitmap> reference = mImage.get(x);
//			if (reference != null) {
//				if (reference.get() != null)
//					reference.get().recycle();
//				mImage.remove(x);
				// Log.i("Forw Buffer recycled: " + y);
//			}
//		}
		mImage.clear();
		// mImageCount = mImage.get(key)
		// for (int x = 0; x < mImageCount; x++) {
		// if (mImage.get(x) != null)
		// mImage.get(x).recycle();
		// }
		// mImage.clear();
		if (mDialog != null && mDialog.isShowing()) {
			mDialog.cancel();
		}
	}

/*
	private ImageView mCheckImageView;
	private int mCheckBitmapPos;

	private void lazyLoadImage(ImageView iv, int pos) {
		mHandler.removeCallbacks(mCheckImageLoaded);
		SoftReference<Bitmap> reference = mImage.get(pos);
		if (reference != null && reference.get() != null) {
			try {
				iv.setImageBitmap(reference.get());
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				mCheckImageView = iv;
				mCheckBitmapPos = pos;
				mHandler.postDelayed(mCheckImageLoaded, 50);
			}
			return;
		} else {
			// Log.i("No Image found: " + pos);
			mImage.put(
					pos,
					new SoftReference<Bitmap>(decodeFile(mImagePath.get(pos),
							mWidth)));
			SoftReference<Bitmap> ref = mImage.get(pos);
			if (ref != null)
				if (ref.get() != null)
					iv.setImageBitmap(ref.get());
		}
	}

	private Runnable mCheckImageLoaded = new Runnable() {

		@Override
		public void run() {
			lazyLoadImage(mCheckImageView, mCheckBitmapPos);
		}
	}; */

/*	
	private class preloadImage extends AsyncTask<Integer, Void, Void> {

		protected Void doInBackground(Integer... params) {
			int isForward = params[0];
			int pos = params[1];
			int forPos, backPos, savePos, backsavePos;
			forPos = pos + BUFFER_COUNT;
			backPos = pos - BUFFER_COUNT;
			savePos = forPos - pos;
			backsavePos = pos + backPos;
			if (isForward == 1) {
				for (int x = pos; x < forPos; x++) {
					if (x < mImageCount && x >= 0) {
						SoftReference<Bitmap> reference = mImage.get(x);
						if (reference == null) {
							mImage.put(
									x,
									new SoftReference<Bitmap>(decodeFile(
											mImagePath.get(x), mWidth)));
						}
					}
				}
				for (int y = backPos; y >= savePos; y--) {
					if (y < mImageCount && y >= 0) {
						SoftReference<Bitmap> reference = mImage.get(y);
						if (reference != null) {
							if (reference.get() != null)
								reference.get().recycle();
							mImage.remove(y);
						}
					}
				}
			} else {
				for (int x = pos; x >= backPos; x--) {
					if (x < mImageCount && x >= 0) {
						SoftReference<Bitmap> reference = mImage.get(x);
						if (reference == null) {
							mImage.put(
									x,
									new SoftReference<Bitmap>(decodeFile(
											mImagePath.get(x), mWidth)));
						}
					}
				}
				for (int y = forPos; y <= backsavePos; y++) {
					if (y < mImageCount && y >= 0) {
						SoftReference<Bitmap> reference = mImage.get(y);
						if (reference != null) {
							if (reference.get() != null)
								reference.get().recycle();
							mImage.remove(y);
						}
					}
				}
			}
			return null;
		}
	}
	*/
	
	
	Runnable mPlay = new Runnable() {
		@Override
		public void run() {
			mHandler.removeCallbacks(mPlay);
			mCurrentFrame++;
			final int currenFrame = getCurrentFrame();
			if (!TextUtils.isEmpty(getUrl(currenFrame))) {
					PicassoUtils.with(View3DPublic.this).load(getUrl(currenFrame))
							.into(mImageView);
					mFrame.setText((mCurrentFrame+1) + "");
					mHandler.postDelayed(mPlay, 100);
			}
		}
	};
	
	public void viewNextFrame(){
		mCurrentFrame++;
		setCurrentFrame(mCurrentFrame);
	}
	
	public void viewLastFrame(){
		mCurrentFrame--;
		setCurrentFrame(mCurrentFrame);
	}

	public void setCurrentFrame(int frame) {
		mHandler.removeCallbacks(mPlay);
		Log.i("--- frame: " + frame);
		mCurrentFrame = frame;
		mCurrentFrame = getCurrentFrame();
		try {
				PicassoUtils.with(this).load(getUrl(mCurrentFrame))
						.into(mImageView);
				mFrame.setText((mCurrentFrame+1) + "");
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}
	
	int getCurrentFrame() {
		int mFrame = (frame == 0) ? size : frame;
		if (mCurrentFrame >= mFrame) {
			mCurrentFrame = 0;
			return 0;
		}

		if (mCurrentFrame < 0) {
			mCurrentFrame = 0;
			return 0;
		}
		return mCurrentFrame;
	}
	
	public String getUrl(int pos) {
		return Camera3D.S3_BASE_URL + "/" + filename + "_" + Utils.addZero(pos, 2) + ".jpg";
	}

}
