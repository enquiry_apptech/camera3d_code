package com.siulun.Camera3D;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;

public class View3D extends Activity implements AccelerometerListener,
		OnClickListener {
	private ImageView mImageView, mIndicator,mUploadBtn;
	private Button mBtnSave, mBtnAccel;
	private TextView mFrame, mTextSpeed;
	// private SeekBar mSeekbar;
	private static Context CONTEXT;
	// private ArrayList<Bitmap> mImage = new ArrayList<Bitmap>();
	private static final SparseArray<SoftReference<Bitmap>> mImage = new SparseArray<SoftReference<Bitmap>>();
	private ArrayList<String> mImagePath = new ArrayList<String>();
	// private Bitmap[] mImage;
	private int mHalf, mImageCount, mFileIndex, mSpeed;
	// private boolean mSeekOk = false;
	private String mFilepath, mThumbFilepath;
	private boolean mIsLand;
	private int mCurrent = Camera3D.CENTER;

	private GestureDetector detector;
	private myGestureListener gListener;
	private SharedPreferences mPrefs;
	private ProgressDialog mDialog;
	private int mWidth = 480;
	private float mLast = 0, mLastX = 0;
	private String mExportName;
	private final static int BUFFER_COUNT = 30;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		Intent i = getIntent();
		mThumbFilepath = i.getStringExtra("filepath");
		mFilepath = mThumbFilepath.replace("/.thumbnails", "");
		mFileIndex = i.getIntExtra("fileindex", -1);
		mIsLand = (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE);
		// if (mIsLand) {
		// this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		// } else
		// this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		setContentView(R.layout.view3d);

		mContentLayout = (RelativeLayout) findViewById(R.id.content);
		mSpinner = (RelativeLayout) findViewById(R.id.dashboard_spinner);
		mTextSpeed = (TextView) findViewById(R.id.mSpeed);
		mFrame = (TextView) findViewById(R.id.mFrame);
		mBtnAccel = (Button) findViewById(R.id.mBtnAccel);
		mBtnSave = (Button) findViewById(R.id.mBtnSave);
		mImageView = (ImageView) findViewById(R.id.ImageView);
		mIndicator = (ImageView) findViewById(R.id.mIndicator);
		mUploadBtn = (ImageView) findViewById(R.id.uploadBtn);
		mUploadBtn.setVisibility(View.VISIBLE);
		mUploadBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				if (!Utils.getBoolean(View3D.this, "isLogin", false)) {
					Toast.makeText(getApplicationContext(),"Please login first...",Toast.LENGTH_LONG).show();
					Intent i = new Intent(View3D.this, Login.class);
					startActivity(i);
					return;
				}

				String file_path = (String) getIntent().getStringExtra("filepath");
	            Intent intent = new Intent(View3D.this, ImageInfoSubmit.class);
	            intent.putExtra("file_path", file_path);
	            startActivity(intent);            
			}
		} );
	}


	RelativeLayout mContentLayout;
	RelativeLayout mSpinner;
	private void updateUi(Boolean status) {
		mContentLayout.setVisibility(status ? View.VISIBLE : View.GONE);
		mSpinner.setVisibility(status ? View.GONE : View.VISIBLE);
	}

	
	protected void onResume() {
		super.onResume();

		mIsLand = (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE);
		if ((getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
				|| (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)) {

			final DisplayMetrics displaymetrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
			final int height = displaymetrics.heightPixels;
			mWidth = displaymetrics.widthPixels;
			if (mWidth > height)
				mWidth = height;
			// mPrefs =
			// getSharedPreferences(Camera3D.MY_PREFS,Context.MODE_PRIVATE);
			mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
			if (mPrefs != null) {
				mSpeed = mPrefs.getInt("speed", 100);
			}

			CONTEXT = this;
			mTextSpeed.setText("Speed: " + mSpeed + "ms");
			mTextSpeed.setVisibility(View.GONE);
			mFrame.setVisibility(View.GONE);

			mBtnAccel.setVisibility(View.GONE);
			mBtnSave.setVisibility(View.GONE);
			// mImage = new Bitmap[Camera3D.MAX_VIEW];

			// mIndicator.setAlpha(120);

			// Display display = ((WindowManager)
			// getSystemService(Context.WINDOW_SERVICE))
			// .getDefaultDisplay();
			// mWidth = display.getWidth();
			// int height = display.getHeight();

//			if (mDialog != null) {
//				if (!mDialog.isShowing())
//					mDialog = ProgressDialog.show(View3D.this, "",
//							getText(R.string.dialog_loading), true);
//			} else {
//				mDialog = ProgressDialog.show(View3D.this, "",
//						getText(R.string.dialog_loading), true);
//			}
			updateUi(false);

			Thread t = new Thread() {
				public void run() {

					File f;
					int count = 0;
					boolean done = true;
					String newPath;
					do {
						newPath = mFilepath.replace("_T",
								"_" + Utils.addZero(count, 2));
						f = new File(newPath);
						if (f.exists()) {
							if (count < BUFFER_COUNT) {
								mImage.put(count, new SoftReference<Bitmap>(
										decodeFile(newPath, mWidth)));
								// mImage.add(decodeFile(newPath, mWidth));
								// Log.i("Buffer added: " + count);
							}
							// else
							// mImage.add(null);
							mImagePath.add(newPath);
							// mNewPath.add(newPath);
							done = true;
							count++;
						} else
							done = false;
					} while (done);
					mImageCount = count;
					mHalf = Math.round(mImageCount / 2);
					mHandler.postDelayed(mDialogDismiss, 100);
				}
			};
			t.start();
			gListener = new myGestureListener();
			detector = new GestureDetector(View3D.this, gListener);
		}

	};

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// setContentView(R.layout.myLayout);
	}


	// Create runnable for posting
	final Runnable mDialogDismiss = new Runnable() {
		public void run() {
			if (mImageCount < 2) {
				Toast.makeText(View3D.this, getText(R.string.toast_no_images),
						Toast.LENGTH_LONG).show();
				finish();
				return;
			} else if (mImageCount == 2) {
				mTextSpeed.setVisibility(View.VISIBLE);
				mIndicator.setImageResource(R.drawable.ivib);
				AccelerometerManager.stopListening();
				Vibration();
			} else {
				// Picasso.with(View3D.this).load(new File(mImagePath.get(0)))
				// .resize(500, 500).centerCrop().into(mImageView);
				// mImageView.setImageBitmap(mImage.get(Camera3D.CENTER));
				lazyLoadImage(mImageView, 0);
				mIndicator.setImageResource(R.drawable.s3);
				mFrame.setVisibility(View.VISIBLE);
				if (AccelerometerManager.isSupported(CONTEXT)) {
					mBtnAccel.setOnClickListener(View3D.this);
					AccelerometerManager.startListening(CONTEXT, View3D.this);
				}
			}
			updateUi(true);
//			try {
//				if (mDialog != null && mDialog.isShowing())
//					mDialog.dismiss();
//			} catch (Exception e) {
//				// nothing
//			}
		}
	};

	// Create runnable for posting
	final Runnable mDialogDismissExport = new Runnable() {
		public void run() {
			mDialog.dismiss();
			Toast.makeText(
					View3D.this,
					getText(R.string.toast_export) + "sdcard\\Pictures\\"
							+ mExportName, Toast.LENGTH_LONG).show();
			mHandler.post(mVibrate);
		}
	};

	public Bitmap decodeFile(String filename, int maxSize) {
		File file = new File(filename);
		return decodeFile(file, maxSize);
	}

	public Bitmap decodeFile(File file, int maxSize) {
		try {
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(file), null, o);
			int scale = 1;
			if (o.outHeight > maxSize || o.outWidth > maxSize) {
				scale = (int) Math.pow(
						2,
						(int) Math.round(Math.log(maxSize
								/ (double) Math.max(o.outHeight, o.outWidth))
								/ Math.log(0.5)));
			}

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(file), null,
					o2);
		} catch (FileNotFoundException e) {
			Log.e(e.toString());
		} catch (Exception e) {
			Log.e(e.toString());
		}
		return null;
	}

	@Override
	public void onClick(View v) {
		mBtnAccel.setVisibility(View.GONE);
		if (AccelerometerManager.isSupported(CONTEXT)) {
			AccelerometerManager.startListening(CONTEXT, this);
		}
	}

	final Handler mHandler = new Handler();
	private int mSwitchImage = 0;
	final Runnable mVibrate = new Runnable() {
		public void run() {
			switch (mSwitchImage) {
			case (0):
				// Picasso.with(View3D.this).load(new File(mImagePath.get(1)))
				// .resize(500, 500).centerCrop().into(mImageView);
				try{
					mImageView.setImageBitmap(mImage.get(1).get());
				} catch (NullPointerException e){}
				mSwitchImage = 1;
				break;
			case (1):
				// Picasso.with(View3D.this).load(new File(mImagePath.get(0)))
				// .resize(500, 500).centerCrop().into(mImageView);
				mImageView.setImageBitmap(mImage.get(0).get());
				mSwitchImage = 0;
				break;
			}
			mHandler.postDelayed(mVibrate, mSpeed);
		}
	};

	// private long mStartTime = 0L;

	private void Vibration() {

		Thread t = new Thread() {
			public void run() {
				mHandler.post(mVibrate);
			}
		};
		t.start();
	}

	public static final int DIALOG_MENU_HELP = 0;
	public static final int DIALOG_MENU_ABOUT = 1;

	protected static final int MENU_EXPORT = Menu.FIRST;
	protected static final int MENU_DELETE = Menu.FIRST + 1;
	protected static final int MENU_HELP = Menu.FIRST + 2;
	protected static final int MENU_ABOUT = Menu.FIRST + 3;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, MENU_EXPORT, 0, getText(R.string.menu_export)).setIcon(
				android.R.drawable.ic_menu_save);
		menu.add(0, MENU_DELETE, 0, getText(R.string.menu_delete)).setIcon(
				android.R.drawable.ic_menu_delete);
		menu.add(0, MENU_HELP, 0, getText(R.string.dialog_help)).setIcon(
				android.R.drawable.ic_menu_help);
		menu.add(0, MENU_ABOUT, 0, getText(R.string.dialog_about)).setIcon(
				android.R.drawable.ic_menu_info_details);
		return super.onCreateOptionsMenu(menu);
	}

	private int mMSTime;
	// private RadioGroup mEncodeSize;
	private Spinner mEncodeSpinner;
	private TextView mMSText;
	private SeekBar mMS;

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		switch (item.getItemId()) {
		/*
		 * case MENU_SHARE: if (mImageCount == 2) {
		 * Gallery.doFileUpload(mFilepath.replace("_T", "_L"), false);
		 * Gallery.doFileUpload(mFilepath.replace("_T", "_R"), true); } else {
		 * Gallery.doFileUpload(mFilepath.replace("_T", "_LL"), false);
		 * Gallery.doFileUpload(mFilepath.replace("_T", "_L"), false);
		 * Gallery.doFileUpload(mFilepath.replace("_T", "_C"), false);
		 * Gallery.doFileUpload(mFilepath.replace("_T", "_R"), false);
		 * Gallery.doFileUpload(mFilepath.replace("_T", "_RR"), true); } return
		 * true;
		 */
		case MENU_EXPORT:
			if (mImageCount == 2)
				mHandler.removeCallbacks(mVibrate);
			final Dialog dialog = new Dialog(View3D.this);
			dialog.setContentView(R.layout.encode);
			dialog.setTitle(getText(R.string.dialog_export));
			dialog.setCancelable(true);
			// there are a lot of settings, for dialog, check them all out!
			mMS = (SeekBar) dialog.findViewById(R.id.mMS);
			// mEncodeSize = (RadioGroup) dialog.findViewById(R.id.mEncodeSize);
			mEncodeSpinner = (Spinner) dialog.findViewById(R.id.mEncodeSpinner);
			mMSText = (TextView) dialog.findViewById(R.id.mMSText);

			// set up button
			Button mBtnOK = (Button) dialog.findViewById(R.id.mBtnOK);
			mMS.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {
					switch (seekBar.getId()) {
					case (R.id.mMS):
						mMSTime = progress + 100;
						mMSText.setText(mMSTime + " ms");
						break;
					}
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {

				}

				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
				}
			});

			mBtnOK.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mDialog = ProgressDialog.show(View3D.this, "",
							getText(R.string.dialog_encoding), true);

					Thread t = new Thread() {
						public void run() {
							try {
								File sdCard = Environment
										.getExternalStorageDirectory();
								File f = new File(sdCard.getAbsolutePath()
										+ "/sd");
								String AbsPath;
								if (f.exists())
									AbsPath = sdCard.getAbsolutePath() + "/sd";
								else
									AbsPath = sdCard.getAbsolutePath();

								f = new File(AbsPath + "/Pictures");
								if (!f.exists()) {
									f.mkdir();
								}
								// int eid =
								// mEncodeSize.getCheckedRadioButtonId();
								int eid = mEncodeSpinner
										.getSelectedItemPosition();
								int ex = 400, ey = 300;
								switch (eid) {
								case 0:
									ex = 400;
									ey = 300;
									break;
								case 1:
									ex = 600;
									ey = 450;
									break;
								case 2:
									ex = 800;
									ey = 600;
									break;
								case 3:
									ex = 1024;
									ey = 768;
									break;
								case 4:
									ex = 1280;
									ey = 1024;
									break;
								case 5:
									ex = 1920;
									ey = 1080;
									break;
								}

								JavaGIFEncoder e = new JavaGIFEncoder();
								FileOutputStream bos = null;
								Bitmap scaled;

								// Check Landscape or Portrait
								f = new File(mFilepath.replace("_T", "_"
										+ Utils.addZero(0, 2)));
								if (f.exists()) {
									BitmapFactory.Options o = new BitmapFactory.Options();
									o.inJustDecodeBounds = true;
									BitmapFactory.decodeStream(
											new FileInputStream(f), null, o);
									if (o.outWidth < o.outHeight) {
										int temp = ex;
										ex = ey;
										ey = temp;
									}
								}
								File n = new File(mFilepath.replace(
										Camera3D.FOLDER_NAME, "Pictures")
										.replace("_T.jpg", ".gif"));
								bos = new FileOutputStream(n.getAbsolutePath());
								e.start(bos);
								e.setSize(ex, ey);
								e.setDelay(mMSTime); // 1 frame per sec
								e.setRepeat(0);

								int count = 0;
								boolean done = true;
								do {
									f = new File(mFilepath.replace("_T", "_"
											+ Utils.addZero(count, 2)));
									if (f.exists()) {
										scaled = Bitmap.createScaledBitmap(
												decodeFile(f, (ex > ey) ? ex
														: ey), ex, ey, true);
										e.addFrame(scaled);
										scaled.recycle();
										done = true;
										count++;
									} else
										done = false;
								} while (done);

								e.finish();
								bos.flush();
								bos.close();

								System.gc();
								mExportName = n.getName();
							} catch (FileNotFoundException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							mHandler.post(mDialogDismissExport);
						}
					};
					t.start();
					dialog.dismiss();
				}
			});
			dialog.show();
			return true;
			/*
			 * mDialog = ProgressDialog.show(View3D.this, "",
			 * getText(R.string.dialog_saving), true); Thread t = new Thread() {
			 * public void run() { Looper.prepare(); try { File sdCard =
			 * Environment.getExternalStorageDirectory(); File f = new File
			 * (sdCard.getAbsolutePath() + "/sd"); String AbsPath; if
			 * (f.exists()) AbsPath = sdCard.getAbsolutePath() + "/sd"; else
			 * AbsPath = sdCard.getAbsolutePath();
			 * 
			 * f = new File (AbsPath + "/Pictures"); if (!f.exists()) {
			 * f.mkdir(); } JavaGIFEncoder e = new JavaGIFEncoder();
			 * FileOutputStream bos = null; Bitmap scaled; File n = new
			 * File(mFilepath.replace(Camera3D.FOLDER_NAME,
			 * "Pictures").replace("_T.jpg", ".gif")); bos = new
			 * FileOutputStream(n.getAbsolutePath()); e.start(bos);
			 * e.setSize(400, 300); e.setDelay(100); // 1 frame per sec
			 * e.setRepeat(0); f = new File(mFilepath.replace("_T", "_" +
			 * Gallery.addZero( 0, 2))); scaled =
			 * Bitmap.createScaledBitmap(decodeFile(f,400), 400, 300, true);
			 * e.addFrame(scaled); scaled.recycle(); f = new
			 * File(mFilepath.replace("_T", "_" + Gallery.addZero( 1, 2)));
			 * scaled = Bitmap.createScaledBitmap(decodeFile(f,400), 400, 300,
			 * true); e.addFrame(scaled); scaled.recycle(); e.finish();
			 * bos.flush(); bos.close(); System.gc(); mExportName = n.getName();
			 * } catch (FileNotFoundException e1) { // TODO Auto-generated catch
			 * block e1.printStackTrace(); } catch (IOException e) { // TODO
			 * Auto-generated catch block e.printStackTrace(); }
			 * mHandler.post(mDialogDismissExport); Looper.loop(); } };
			 * t.start(); return true;
			 */
		case MENU_DELETE:
			new AlertDialog.Builder(this)
					.setTitle(getString(R.string.dialog_delete))
					.setMessage(getString(R.string.dialog_delete_confirm))
					.setPositiveButton(android.R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface d, int w) {
									Gallery.delete_photo(mThumbFilepath);
									Intent i = new Intent(getBaseContext(),
											Gallery.class);
									i.putExtra("fileindex", mFileIndex);
									setResult(RESULT_OK, i);
									finish();
								}
							}).setNegativeButton(android.R.string.cancel, null)
					.show();
			return true;
			/*
			 * case MENU_PREF: Intent settingsActivity = new
			 * Intent(getBaseContext(), Preferences.class);
			 * startActivity(settingsActivity); break;
			 */
		case MENU_HELP:
			showDialog(DIALOG_MENU_HELP);
			break;
		case MENU_ABOUT:
			showDialog(DIALOG_MENU_ABOUT);
			break;
		}
		return true;
	}

	// private boolean mServiceOnline;
	// private void startSwitcherService(){
	// Intent i = new Intent(EncodeService.ACTION_FOREGROUND);
	// i.setClass(getBaseContext(), EncodeService.class);
	// i.setFlags( Intent.FLAG_ACTIVITY_SINGLE_TOP );
	// i.putExtra("filepath", mFilepath);
	// mServiceOnline = true;
	// startService(i);
	// }
	//
	// private void stopSwitcherService(){
	// mServiceOnline = false;
	// stopService(new Intent(getBaseContext(),
	// EncodeService.class));
	// }

	@Override
	public Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_MENU_HELP:
			try {
				InputStream is = getAssets().open("help.txt");
				int size = is.available();
				byte[] buffer = new byte[size];
				is.read(buffer);
				is.close();
				// Convert the buffer into a string.
				String text = new String(buffer);
				return new AlertDialog.Builder(this)
						.setTitle(R.string.dialog_instructions)
						.setMessage(text)
						.setPositiveButton(R.string.OK,
								new DialogInterface.OnClickListener() {
									public void onClick(
											DialogInterface dialoginterface,
											int i) {
										dialoginterface.cancel();
									}
								}).show();
			} catch (IOException e) {
				// Should never happen!
				throw new RuntimeException(e);
			}
		case DIALOG_MENU_ABOUT:
			final TextView message = new TextView(this);
			SpannableString s = new SpannableString(
					this.getText(R.string.dialog_about_1));
			Linkify.addLinks(s, Linkify.EMAIL_ADDRESSES);
			message.setText(s);
			s = new SpannableString(this.getText(R.string.dialog_about_2));
			Linkify.addLinks(s, Linkify.WEB_URLS);
			message.append(s);
			s = new SpannableString(this.getText(R.string.dialog_about_3));
			message.append(s);
			message.setMovementMethod(LinkMovementMethod.getInstance());

			return new AlertDialog.Builder(this)
					.setTitle(R.string.dialog_about)
					// .setMessage(R.string.dialog_about_detail)
					.setView(message)
					.setPositiveButton(R.string.OK,
							new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface dialoginterface, int i) {
									dialoginterface.cancel();
								}
							}).show();
		}
		return null;
	}

	private void SetPref() {
		if (mPrefs != null) {
			SharedPreferences.Editor ed = mPrefs.edit();
			ed.putInt("speed", mSpeed);
			ed.commit();
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this); // Add this method.
	}

	@Override
	public void onPause() {
		super.onPause();
		try {
			if (mDialog != null && mDialog.isShowing())
				mDialog.dismiss();
		} catch (Exception e) {
			// nothing
		}
		mHandler.removeCallbacks(mVibrate);
		mHandler.removeCallbacks(mDialogDismiss);
		mHandler.removeCallbacks(mDialogDismissExport);
		mHandler.removeCallbacks(mCheckImageLoaded);
		mHandler.removeCallbacks(mFlingEffect);
	}
	
	@Override
	public void onStop() {
		super.onStop();
		mHandler.removeCallbacks(mVibrate);
		mHandler.removeCallbacks(mDialogDismiss);
		mHandler.removeCallbacks(mDialogDismissExport);
		mHandler.removeCallbacks(mCheckImageLoaded);
		mHandler.removeCallbacks(mFlingEffect);
		EasyTracker.getInstance().activityStop(this); // Add this method.
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		SetPref();
		if (AccelerometerManager.isListening()) {
			AccelerometerManager.stopListening();
		}
		for (int x = 0; x < mImageCount; x++) {
			SoftReference<Bitmap> reference = mImage.get(x);
			if (reference != null) {
				if (reference.get() != null)
					reference.get().recycle();
				mImage.remove(x);
				// Log.i("Forw Buffer recycled: " + y);
			}
		}
		mImage.clear();
		// mImageCount = mImage.get(key)
		// for (int x = 0; x < mImageCount; x++) {
		// if (mImage.get(x) != null)
		// mImage.get(x).recycle();
		// }
		// mImage.clear();
		if (mDialog != null && mDialog.isShowing()) {
			mDialog.cancel();
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getY() > 90) {
			if (detector.onTouchEvent(event)) {
				return detector.onTouchEvent(event);
			} else {
				return super.onTouchEvent(event);
			}
		} else {
			return false;
		}
	}

	private final static int TOUCH_DISTANCE = 10;

	public class myGestureListener implements GestureDetector.OnGestureListener {
		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			if (mImageCount == 2) {
				if (distanceX < 0 && mSpeed < 1000) {
					mSpeed += 20;
				} else if (distanceX > 0 && mSpeed > 40) {
					mSpeed -= 20;
				}
				mTextSpeed.setText("Speed: " + mSpeed + "ms");
				return false;
			}
			if (distanceX < -TOUCH_DISTANCE) {
				// Log.i("mCurrent: "+mCurrent);
				if (AccelerometerManager.isListening()) {
					AccelerometerManager.stopListening();
				}
				mBtnAccel.setVisibility(View.VISIBLE);
				if (mCurrent < mImageCount - 1) {
					mCurrent++;
					new preloadImage().execute(1, mCurrent);
					lazyLoadImage(mImageView, mCurrent);
					mFrame.setText((mCurrent + 1) + "");
				} else {
					mCurrent = 0;
				}
			} else if (distanceX > TOUCH_DISTANCE) {
				// Log.i("mCurrent: "+mCurrent);
				if (AccelerometerManager.isListening()) {
					AccelerometerManager.stopListening();
				}
				mBtnAccel.setVisibility(View.VISIBLE);
				if (mCurrent > 0) {
					mCurrent--;
				} else {
					mCurrent = mImageCount-1;
				}
				new preloadImage().execute(0, mCurrent);
				lazyLoadImage(mImageView, mCurrent);
				mFrame.setText((mCurrent + 1) + "");
			}
			if (mCurrent < mHalf - mHalf / 2)
				mIndicator.setImageResource(R.drawable.s1);
			else if (mCurrent < mHalf)
				mIndicator.setImageResource(R.drawable.s2);
			else if (mCurrent > mHalf + mHalf / 2)
				mIndicator.setImageResource(R.drawable.s5);
			else if (mCurrent > mHalf)
				mIndicator.setImageResource(R.drawable.s4);
			return false;
		}

		@Override
		public boolean onDown(MotionEvent arg0) {
			mHandler.removeCallbacks(mFlingEffect);
			return true;
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
//			if (mIsLand)
//				mFlingVelocity = velocityX;
//			else
//				mFlingVelocity = velocityY;
			mFlingVelocity = velocityX;
			mHandler.removeCallbacks(mFlingEffect);
			mHandler.postDelayed(mFlingEffect, 100);
			return false;
		}

		@Override
		public void onLongPress(MotionEvent e) {
			/*
			 * if (bm != null) {
			 * 
			 * }
			 */
		}

		@Override
		public void onShowPress(MotionEvent e) {
		}

		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			return false;
		}
	}
	
	private float mFlingVelocity;
	
	private final Runnable mFlingEffect = new Runnable() {
		@Override
		public void run() {
			mHandler.removeCallbacks(mFlingEffect);
			if (mFlingVelocity != 0) {
				if (mFlingVelocity < 0) {
					if (mCurrent > 0){
						mCurrent--;
						if (mCurrent < 0) {
							mFlingVelocity = 0;
							return;
						}
					} else {
						return;
					}
				} else {
					if (mCurrent < mImageCount - 1) {
						mCurrent++;
						if (mCurrent >= mImageCount) {
							mFlingVelocity = 0;
							return;
						}
					} else {
						return;
					}
				}
				new preloadImage().execute(1, mCurrent);
				lazyLoadImage(mImageView, mCurrent);
				Thread t = new Thread() {
					public void run() {
					}
				};
//				if (mCurrent < mHalf - mHalf / 2)
//					mIndicator.setImageResource(R.drawable.s1);
//				else if (mCurrent < mHalf)
//					mIndicator.setImageResource(R.drawable.s2);
//				else if (mCurrent > mHalf + mHalf / 2)
//					mIndicator.setImageResource(R.drawable.s5);
//				else if (mCurrent > mHalf)
//					mIndicator.setImageResource(R.drawable.s4);
				
				mFrame.setText((mCurrent + 1) + "");
				mFlingVelocity = mFlingVelocity * 0.90f;
				Log.i("Velocity: " + mFlingVelocity);
				final int maxFlingVelocity = ViewConfiguration.get(View3D.this).getScaledMaximumFlingVelocity();
				float ratio = 1.0f - ((mFlingVelocity>0?mFlingVelocity:-mFlingVelocity)/(float)maxFlingVelocity);
				Log.i("Ratio: " + ratio);
				if (ratio > 0.99)
					return;
				int delayMillis = (int) (ratio * 500 * 0.1f);
				if (delayMillis < 1) {
					mHandler.post(mFlingEffect);
				} else {
					mHandler.postDelayed(mFlingEffect, delayMillis);
				}
				Log.i("delayMillis: " + delayMillis);
			}
		}
	};

	@Override
	public void onAccelerationChanged(float x, float y, float z) {
		float level;
		if (mIsLand)
			level = y;
		else
			level = -x;
		if (mImage == null)
			return;
		if (mImage.size() <= 0)
			return;
		if (mImageCount == 2)
			return;
		if (level > 8)
			level = x;
		if (mLast > level && mLast - 0.1 < level)
			return;
		if (mLast < level && mLast + 0.1 > level)
			return;
		if (level < -5 || level > 5)
			return;
//		if (mLast < level && level > 1) {
//			if (mCurrent > 0) {
//				mCurrent--;
//				new preloadImage().execute(0, mCurrent);
//			}
//		} else if (mLast < level && level < -1) {
//			if (mCurrent > mHalf) {
//				mCurrent--;
//				new preloadImage().execute(0, mCurrent);
//			}
//		} else if (mLast > level && level > 1) {
//			if (mCurrent < mHalf) {
//				mCurrent++;
//				new preloadImage().execute(1, mCurrent);
//			}
//		} else if (mLast > level && level < -1) {
//			if (mCurrent < mImageCount - 1) {
//				mCurrent++;
//				new preloadImage().execute(1, mCurrent);
//			}
//		}
		if (mLast < level && level > 1) {
			if (mCurrent < mImageCount - 1) {
				mCurrent++;
				new preloadImage().execute(1, mCurrent);
			}
		} else if (mLast < level && level < -1) {
			if (mCurrent < mHalf) {
				mCurrent++;
				new preloadImage().execute(1, mCurrent);
			}
		} else if (mLast > level && level > 1) {
			if (mCurrent > mHalf) {
				mCurrent--;
				new preloadImage().execute(0, mCurrent);
			}
		} else if (mLast > level && level < -1) {
			if (mCurrent > 0) {
				mCurrent--;
				new preloadImage().execute(0, mCurrent);
			}
		}

		SoftReference<Bitmap> reference = mImage.get(mCurrent);
		if (reference != null) {
			if (mCurrent < mHalf - mHalf / 2)
				mIndicator.setImageResource(R.drawable.s1);
			else if (mCurrent < mHalf)
				mIndicator.setImageResource(R.drawable.s2);
			else if (mCurrent > mHalf + mHalf / 2)
				mIndicator.setImageResource(R.drawable.s5);
			else if (mCurrent > mHalf)
				mIndicator.setImageResource(R.drawable.s4);

			lazyLoadImage(mImageView, mCurrent);

			mFrame.setText((mCurrent + 1) + "");
		}
	}

	private ImageView mCheckImageView;
	private int mCheckBitmapPos;

	private void lazyLoadImage(ImageView iv, int pos) {
		mHandler.removeCallbacks(mCheckImageLoaded);
		SoftReference<Bitmap> reference = mImage.get(pos);
		if (reference != null && reference.get() != null) {
			try {
				iv.setImageBitmap(reference.get());
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				mCheckImageView = iv;
				mCheckBitmapPos = pos;
				mHandler.postDelayed(mCheckImageLoaded, 50);
			}
			return;
		} else {
			// Log.i("No Image found: " + pos);
			mImage.put(
					pos,
					new SoftReference<Bitmap>(decodeFile(mImagePath.get(pos),
							mWidth)));
			SoftReference<Bitmap> ref = mImage.get(pos);
			if (ref != null)
				if (ref.get() != null)
					iv.setImageBitmap(ref.get());
		}
	}

	private Runnable mCheckImageLoaded = new Runnable() {

		@Override
		public void run() {
			lazyLoadImage(mCheckImageView, mCheckBitmapPos);
		}
	};

	private class preloadImage extends AsyncTask<Integer, Void, Void> {
		/**
		 * The system calls this to perform work in a worker thread and delivers
		 * it the parameters given to AsyncTask.execute()
		 * 
		 * @return
		 */
		protected Void doInBackground(Integer... params) {
			int isForward = params[0];
			int pos = params[1];
			int forPos, backPos, savePos, backsavePos;
			forPos = pos + BUFFER_COUNT;
			backPos = pos - BUFFER_COUNT;
			savePos = forPos - pos;
			backsavePos = pos + backPos;
			if (isForward == 1) {
//				 Log.i("Current: " + pos + ", isForward: " + isForward
//				 + ", forPos: " + forPos + ", backPos: " + backPos);
				for (int x = pos; x < forPos; x++) {
					if (x < mImageCount && x >= 0) {
						SoftReference<Bitmap> reference = mImage.get(x);
						if (reference == null) {
							mImage.put(
									x,
									new SoftReference<Bitmap>(decodeFile(
											mImagePath.get(x), mWidth)));
//							 Log.i("Forw Buffer added: " + x);
						}
					}
				}
				for (int y = backPos; y >= savePos; y--) {
					if (y < mImageCount && y >= 0) {
						SoftReference<Bitmap> reference = mImage.get(y);
						if (reference != null) {
							if (reference.get() != null)
								reference.get().recycle();
							mImage.remove(y);
//							 Log.i("Forw Buffer recycled: " + y);
						}
					}
				}
			} else {
//				 Log.i("Current: " + pos + ", isForward: " + isForward
//				 + ", forPos: " + forPos + ", backPos: " + backPos);
				for (int x = pos; x >= backPos; x--) {
					if (x < mImageCount && x >= 0) {
						SoftReference<Bitmap> reference = mImage.get(x);
						if (reference == null) {
							mImage.put(
									x,
									new SoftReference<Bitmap>(decodeFile(
											mImagePath.get(x), mWidth)));
//							 Log.i("Back Buffer added: " + x);
						}
					}
				}
				for (int y = forPos; y <= backsavePos; y++) {
					if (y < mImageCount && y >= 0) {
						SoftReference<Bitmap> reference = mImage.get(y);
						if (reference != null) {
							if (reference.get() != null)
								reference.get().recycle();
							mImage.remove(y);
//							 Log.i("Back Buffer recycled: " + y);
						}
					}
				}
			}
			return null;
		}
	}

	// private void preloadImage(final boolean isForward, final int pos) {
	//
	// Thread t = new Thread() {
	// public void run() {
	//
	// int forPos, backPos;
	// forPos = pos + BUFFER_COUNT;
	// backPos = pos - BUFFER_COUNT;
	// if (isForward) {
	// Log.i("Current: " + pos + ", isForward: " + isForward
	// + ", forPos: " + forPos + ", backPos: " + backPos);
	// for (int x = pos; x < forPos; x++) {
	// if (x < mImageCount && x >= 0) {
	// if (mImage.get(x) == null) {
	// mImage.add(x,
	// decodeFile(mImagePath.get(x), mWidth));
	// Log.i("Buffer added: " + x);
	// }
	// }
	// }
	// for (int y = backPos; y >= 0; y--) {
	// if (y < mImageCount && y >= 0) {
	// if (mImage.get(y) != null) {
	// mImage.get(y).recycle();
	// mImage.add(y, null);
	// Log.i("Buffer recycled: " + y);
	// }
	// }
	// }
	// } else {
	// Log.i("Current: " + pos + ", isForward: " + isForward
	// + ", forPos: " + forPos + ", backPos: " + backPos);
	// for (int x = pos; x >= backPos; x--) {
	// if (x < mImageCount && x >= 0) {
	// if (mImage.get(x) == null) {
	// mImage.add(x,
	// decodeFile(mImagePath.get(x), mWidth));
	// Log.i("Buffer added: " + x);
	// }
	// }
	// }
	// for (int y = forPos; y <= mImageCount; y++) {
	// if (y < mImageCount && y >= 0) {
	// if (mImage.get(y) != null) {
	// mImage.get(y).recycle();
	// mImage.add(y, null);
	// Log.i("Buffer recycled: " + y);
	// }
	// }
	// }
	// }
	// }
	// };
	// t.start();
	//
	// }

	// @Override
	// public void onAccelerationChanged(float x, float y, float z) {
	// if (Log.LOGV)
	// Log.v("x="+String.valueOf(x)+", y"+String.valueOf(y)+", z"+String.valueOf(z));
	//
	// if (mImage == null)
	// return;
	// if (mImage.size() <= 0)
	// return;
	// if (mImageCount == 2)
	// return;
	// if (y > 8)
	// y = x;
	// if (mLast > y && mLast - 0.1 < y)
	// return;
	// if (mLast < y && mLast + 0.1 > y)
	// return;
	//
	// if (mLastX > x && mLastX - 0.1 < y)
	// return;
	// if (mLastX < x && mLastX + 0.1 > y)
	// return;
	//
	// if (y < -5 || y > 5)
	// return;
	// if (y >= -1 && y <= 1) {
	// mCurrent = mHalf;
	// } else if (mLast < y && y > 1) {
	// if (mCurrent < mImageCount - 1)
	// mCurrent++;
	// } else if (mLast < y && y < -1) {
	// if (mCurrent < mHalf)
	// mCurrent++;
	// } else if (mLast > y && y > 1) {
	// if (mCurrent > mHalf)
	// mCurrent--;
	// } else if (mLast > y && y < -1) {
	// if (mCurrent > 0)
	// mCurrent--;
	// }
	// else if (x >= -1 && x <= 1) {
	// mCurrent = mHalf;
	// } else if (mLastX < x && x > 1) {
	// if (mCurrent < mImageCount - 1)
	// mCurrent++;
	// } else if (mLastX < x && x < -1) {
	// if (mCurrent < mHalf)
	// mCurrent++;
	// } else if (mLastX > x && x > 1) {
	// if (mCurrent > mHalf)
	// mCurrent--;
	// } else if (mLastX > x && x < -1) {
	// if (mCurrent > 0)
	// mCurrent--;
	// }
	// if (mImage.get(mCurrent) != null) {
	// if (y >= -1 && y <= 1)
	// mIndicator.setImageResource(R.drawable.s3);
	//
	// else if (x >= -1 && x <= 1)
	// mIndicator.setImageResource(R.drawable.s3);
	//
	//
	// else if (mCurrent < mHalf - mHalf / 2)
	// mIndicator.setImageResource(R.drawable.s1);
	// else if (mCurrent < mHalf)
	// mIndicator.setImageResource(R.drawable.s2);
	// else if (mCurrent > mHalf + mHalf / 2)
	// mIndicator.setImageResource(R.drawable.s5);
	// else if (mCurrent > mHalf)
	// mIndicator.setImageResource(R.drawable.s4);
	//
	// mImageView.setImageBitmap(mImage.get(mCurrent));
	// mFrame.setText((mCurrent + 1) + "");
	// }
	// mLast = y;
	// mLastX = x;
	// }

	@Override
	public void onShake(float force) {
		// Toast.makeText(this, "Phone shaked : " + force, 1000).show();
	}
}
