package com.siulun.Camera3D;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FacebookApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class LoginWeb extends Activity {
	SharedPreferences settings;
	OAuthService service;
	Token requestToken;
	final Token EMPTY_TOKEN = null;

	Button home;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_web);

		
		initControls();

		final String PROTECTED_RESOURCE_URL = "https://graph.facebook.com/me";
		String apiKey = "474574509298139";
		String apiSecret = "bce76ca13f2d4f191fdbf3728e24e30d";
		final String callbackUrl = "http://www.apptech.com.hk/";
		service = new ServiceBuilder().provider(FacebookApi.class)
				.apiKey(apiKey).apiSecret(apiSecret).callback(callbackUrl)
				.build();

//		requestToken = service.getRequestToken();
		final String authURL = service.getAuthorizationUrl(EMPTY_TOKEN);

		final WebView webview = (WebView) findViewById(R.id.webView);

		// attach WebViewClient to intercept the callback url
		webview.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {

				// check for our custom callback protocol
				// otherwise use default behavior
				if (url.startsWith(callbackUrl)) {
					// authorization complete hide webview for now.
					webview.setVisibility(View.GONE);
					Uri uri = Uri.parse(url);
					String verifier = uri.getQueryParameter("code");

					new AsyncTask<String, Void, Void>() {
						@Override
						protected Void doInBackground(String... params) {
							final String verifier = params[0];
							Verifier v = new Verifier(verifier);
							
							// save this token for practical use.
							Token accessToken = service.getAccessToken(requestToken, v);
							
							OAuthRequest request = new OAuthRequest(Verb.GET,
									PROTECTED_RESOURCE_URL);
							service.signRequest(accessToken, request);
							Response response = request.send();
		
//							 xmlHandler xh = new xmlHandler(response.getBody());
		
					/*		System.out.println("Got it! Lets see what we found...");
							System.out.println();
							System.out.println(response.getCode());
							System.out.println(response.getBody());
		
							System.out.println();
							System.out
									.println("Thats it man! Go and build something awesome with Scribe! :)");
		*/
							// settings = getSharedPreferences("preferences", 0);
							// SharedPreferences.Editor editor = settings.edit();
							//
							// editor.putString("accessToken", accessToken.getToken());
							//
							// // The requestToken is saved for use later on to verify
							// the
							// // OAuth request.
							// // See onResume() below
							// editor.putString("requestToken",
							// requestToken.getToken());
							// editor.putString("requestSecret",
							// requestToken.getSecret());
							//
							// editor.putString("first-name",
							// xh.getValue("first-name"));
							// editor.putString("last-name", xh.getValue("last-name"));
							//
							// editor.commit();
							return null;
								}
		
								protected void onPostExecute(Void result) {
								};
		
							}.execute(verifier);

					return true;
				}
				return super.shouldOverrideUrlLoading(view, url);
			}
		});

		// send user to authorization page
		webview.loadUrl(authURL);
	}

	@Override
	protected void onResume() {
		super.onResume();

		Intent i = getIntent();

		if (i != null) {
			Uri uri = i.getData();
			if (uri != null) {
				String oauthVerifier = uri.getQueryParameter("oauth_verifier");

				Verifier verifier = new Verifier(oauthVerifier);

				requestToken = new Token(settings.getString("requestToken",
						null), settings.getString("requestSecret", null));

				Token accessToken = service.getAccessToken(requestToken,
						verifier);

				// // Save the access token.
				// SharedPreferences.Editor editor = settings.edit();
				// editor.remove("requestToken");
				// editor.remove("requestSecret");
				// editor.putString("accessToken", accessToken.getToken());
				// editor.putString("accessSecret", accessToken.getSecret());
				// editor.commit();
				//
				// // Start the film list activity.
				// final Intent intent = new Intent(this, ProConnect.class);
				// intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				// startActivity(intent);
			}
		}
	}

	private void initControls() {
		// home = (Button) findViewById(R.id.home);
		//
		// final Intent intent = new Intent(this, ProConnect.class);
		// intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		//
		// home.setOnClickListener(new Button.OnClickListener() {
		// public void onClick(View v) {
		// startActivity(intent);
		// }
		// });

	}

}
