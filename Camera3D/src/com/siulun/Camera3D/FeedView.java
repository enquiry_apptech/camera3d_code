package com.siulun.Camera3D;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.google.analytics.tracking.android.EasyTracker;
import com.siulun.Camera3D.adapter.FeedViewPrivateAdapter;
import com.siulun.Camera3D.adapter.FeedViewPublicAdapter;
import com.siulun.Camera3D.model.FeedSet;

public class FeedView extends SherlockFragmentActivity implements
		ActionBar.OnNavigationListener {
	ListView listview;
	RelativeLayout mSpinner;
	String mFeedType;
	// private GestureDetector detector;
	// private myGestureListener gListener;
	// private boolean mIsLand;
	FeedViewPrivateAdapter mAdapter;
	FeedViewPublicAdapter mPublicAdapter;
	Boolean mStartScrolling = false;
	float lastX, lastY, distanceX, distanceY;
	private final static int TOUCH_DISTANCE = 10;
	ArrayList<FeedSet> mSocialSet = new ArrayList<FeedSet>();
	private FragmentTabHost mTabHost;
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.feed_fragment);
		

		Context context = getSupportActionBar().getThemedContext();
		SpinnerAdapter mSpinnerAdapter = ArrayAdapter.createFromResource(
				context, R.array.select_type, R.layout.abs_spinner);

		// ActionBar bar = getSupportActionBar();
		// bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		// bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
		//
		// mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		// mTabHost.setup(this, getSupportFragmentManager(), R.id.container);
		//
		// mTabHost.addTab(mTabHost.newTabSpec("simple").setIndicator("Simple"),
		// FeedViewPublicFragment.class, null);
		// mTabHost.addTab(mTabHost.newTabSpec("contacts")
		// .setIndicator("Contacts"), FeedViewPrivateFragment.class, null);
		// mTabHost.addTab(mTabHost.newTabSpec("custom").setIndicator("Custom"),
		// GalleryFragment.class, null);

		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		// actionBar.setSelectedNavigationItem(m_last_board);
		actionBar.setListNavigationCallbacks(mSpinnerAdapter, FeedView.this);

		// actionBar.setListNavigationCallbacks(mSpinnerAdapter,
		// new ActionBar.OnNavigationListener() {
		//
		// @Override
		// public boolean onNavigationItemSelected(int itemPosition,
		// long itemId) {
		// // if (mFirstLoad) {
		// // mFirstLoad = false;
		// // return true;
		// // }
		// // if (isOnline(TopicListActivity.this)) {
		// // } else {
		// // }
		//
		// new AsyncTask<Void, Void, String>() {
		// @Override
		// protected String doInBackground(Void... params) {
		// try {
		// return sendToServer();
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// return null;
		// }
		//
		// protected void onPostExecute(String result) {
		// // Log.i(Camera3D.TAG, result);
		// JSONObject json;
		// ArrayList<FeedSet> socialSet = new ArrayList<FeedSet>();
		// try {
		// if (!TextUtils.isEmpty(result)) {
		// JSONArray array = (JSONArray) new JSONArray(
		// result);
		// int count = array.length();
		// for (int i = 0; i < count; i++) {
		// JSONObject row = array
		// .getJSONObject(i);
		// socialSet.add(new FeedSet(row
		// .getString("filename"), row
		// .getInt("frame"), row
		// .getString("title"), row
		// .getString("description"),
		// row.getInt("status")));
		// }
		// mSocialSet = socialSet;
		// }
		// mSpinner.setVisibility(View.GONE);
		// init();
		// // json = (JSONObject) new
		// // JSONTokener(result).nextValue();
		// // if
		// // (json.get("status").toString().equals("done"))
		// // {
		// // // mImageId = (String)
		// // json.get("userId");
		// // } else {
		// // Toast.makeText(getApplicationContext(),
		// // (String) json.get("msg"),
		// // Toast.LENGTH_LONG).show();
		// // finish();
		// // }
		// } catch (JSONException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// };
		//
		// }.execute();
		// return true;
		// }
		// });

		Intent intent = getIntent();
		mFeedType = intent.getStringExtra("feed_type");
		if (TextUtils.isEmpty(mFeedType)) {
			mFeedType = "private";
		}

		listview = (ListView) findViewById(R.id.listView);
		mSpinner = (RelativeLayout) findViewById(R.id.spinner);

		//didnt login, redirect to login page
		if (mFeedType.equals("private")
				&& TextUtils.isEmpty(Utils.getString(this, "userId"))) {
			Intent i;
			i = new Intent(getBaseContext(), Login.class);
			startActivity(i);
			return;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	// private void init() {
	// if (mFeedType.equals("private")) {
	// mAdapter = new FeedViewAdapter(this, mSocialSet);
	// listview.setAdapter(mAdapter);
	//
	// mSpinner.setVisibility(View.GONE);
	// listview.setVisibility(View.VISIBLE);
	// listview.setOnScrollListener(new OnScrollListener() {
	//
	// int mVisibileItemCount;
	//
	// @Override
	// public void onScrollStateChanged(AbsListView view, int scrollState) {
	// // TODO Auto-generated method stub
	// // Log.i(""+scrollState);
	// mStartScrolling = true;
	// if (scrollState == OnScrollListener.SCROLL_STATE_IDLE) {
	// if (listIsAtTop())
	// mAdapter.setCurrentFeed(0);
	// else if (mVisibileItemCount > 2) {
	// mAdapter.setCurrentFeed(view.getFirstVisiblePosition() + 1);
	// } else
	// mAdapter.setCurrentFeed(view.getLastVisiblePosition());
	// }
	// }
	//
	// @Override
	// public void onScroll(AbsListView view, int firstVisibleItem,
	// int visibleItemCount, int totalItemCount) {
	// mVisibileItemCount = visibleItemCount;
	// //
	// Log.i(firstVisibleItem+" : "+visibleItemCount+" : "+view.getLastVisiblePosition());
	// }
	// });
	// mAdapter.setCurrentFeed(0);
	// mHandler.postDelayed(mCheckScroll, 100);
	// } else {
	// mPublicAdapter = new PublicFeedViewAdapter(this, mSocialSet);
	// listview.setAdapter(mPublicAdapter);
	//
	// mSpinner.setVisibility(View.GONE);
	// listview.setVisibility(View.VISIBLE);
	// listview.setOnScrollListener(new OnScrollListener() {
	//
	// int mVisibileItemCount;
	//
	// @Override
	// public void onScrollStateChanged(AbsListView view, int scrollState) {
	// // TODO Auto-generated method stub
	// // Log.i(""+scrollState);
	// mStartScrolling = true;
	// if (scrollState == OnScrollListener.SCROLL_STATE_IDLE) {
	// if (listIsAtTop())
	// mPublicAdapter.setCurrentFeed(0);
	// else if (mVisibileItemCount > 2) {
	// mPublicAdapter.setCurrentFeed(view.getFirstVisiblePosition() + 1);
	// } else
	// mPublicAdapter.setCurrentFeed(view.getLastVisiblePosition());
	// }
	// }
	//
	// @Override
	// public void onScroll(AbsListView view, int firstVisibleItem,
	// int visibleItemCount, int totalItemCount) {
	// mVisibileItemCount = visibleItemCount;
	// //
	// Log.i(firstVisibleItem+" : "+visibleItemCount+" : "+view.getLastVisiblePosition());
	// }
	// });
	// mPublicAdapter.setCurrentFeed(0);
	// mHandler.postDelayed(mCheckScroll, 100);
	// }
	// }

	// private String sendToServer() throws IOException {
	// HttpClient httpclient = new DefaultHttpClient();
	// HttpPost httppost;
	// if (mFeedType.equals("private"))
	// httppost = new HttpPost(Camera3D.SERVER_GET_PRIVATE);
	// else
	// httppost = new HttpPost(Camera3D.SERVER_GET_PUBLIC);
	//
	// ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
	// 2);
	// nameValuePairs.add(new BasicNameValuePair("userId", Utils.getString(
	// FeedView.this, "userId")));
	//
	// try {
	// httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
	// HttpResponse response = httpclient.execute(httppost);
	// HttpEntity entity = response.getEntity();
	//
	// String str = Utils.convertStreamToString(entity.getContent());
	//
	// return str;
	// } catch (Exception e) {
	// Log.e("log_tag", "Error in http connection " + e.toString());
	// }
	// return null;
	// }

	Handler mHandler = new Handler();

	Runnable mCheckScroll = new Runnable() {
		@Override
		public void run() {
			mHandler.removeCallbacks(mCheckScroll);
			mHandler.postDelayed(mCheckScroll, 300);
			// Log.i("getTouchingStatus : " + mAdapter.getTouchingStatus());
			if (mFeedType.equals("private")) {
				if (mAdapter.getTouchingStatus()) {
					listview.setScrollContainer(false);
				} else {
					mHandler.postDelayed(mRestoreScroll, 600);
				}
			} else {
				if (mPublicAdapter.getTouchingStatus()) {
					listview.setScrollContainer(false);
				} else {
					mHandler.postDelayed(mRestoreScroll, 600);
				}
			}
		}
	};

	Runnable mRestoreScroll = new Runnable() {
		@Override
		public void run() {
			listview.setScrollContainer(true);
		}
	};

	

	// @Override
	// protected void onResume() {
	// super.onResume();
	// mIsLand = (getResources().getConfiguration().orientation ==
	// Configuration.ORIENTATION_LANDSCAPE);
	// gListener = new myGestureListener();
	// detector = new GestureDetector(this, gListener);
	// }

	// @Override
	// public boolean onTouchEvent(MotionEvent event) {
	// if (event.getY() > 90) {
	// if (detector.onTouchEvent(event)) {
	// return detector.onTouchEvent(event);
	// } else {
	// return super.onTouchEvent(event);
	// }
	// } else {
	// return false;
	// }
	// }

	// private final static int TOUCH_DISTANCE = 10;
	// final Handler mHandler = new Handler();
	// public class myGestureListener implements
	// GestureDetector.OnGestureListener {
	// @Override
	// public boolean onScroll(MotionEvent e1, MotionEvent e2,
	// float distanceX, float distanceY) {
	// Log.i("myGestureListener"+" : "+distanceX+" : "+distanceY);
	// if (distanceX < -TOUCH_DISTANCE) {
	// mAdapter.viewLastFrame();
	// } else if (distanceX > TOUCH_DISTANCE) {
	// mAdapter.viewNextFrame();
	// }
	// return true;
	// }
	//
	// @Override
	// public boolean onDown(MotionEvent arg0) {
	// Log.i("onDown");
	// // mHandler.removeCallbacks(mFlingEffect);
	// return true;
	// }
	//
	// @Override
	// public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
	// float velocityY) {
	// Log.i("onFling");
	// // if (mIsLand)
	// // mFlingVelocity = velocityX;
	// // else
	// // mFlingVelocity = velocityY;
	// // mHandler.removeCallbacks(mFlingEffect);
	// // mHandler.postDelayed(mFlingEffect, 100);
	// return false;
	// }
	//
	// @Override
	// public void onLongPress(MotionEvent e) {
	// /*
	// * if (bm != null) {
	// *
	// * }
	// */
	// }
	//
	// @Override
	// public void onShowPress(MotionEvent e) {
	// }
	//
	// @Override
	// public boolean onSingleTapUp(MotionEvent e) {
	// return false;
	// }
	// }
	// private float mFlingVelocity;

	// private final Runnable mFlingEffect = new Runnable() {
	// @Override
	// public void run() {
	// mHandler.removeCallbacks(mFlingEffect);
	// if (mFlingVelocity != 0) {
	// if (mFlingVelocity < 0) {
	// if (mCurrent < mImageCount - 1) {
	// mCurrent++;
	// if (mCurrent >= mImageCount) {
	// mFlingVelocity = 0;
	// return;
	// }
	// } else {
	// return;
	// }
	// } else {
	// if (mCurrent > 0){
	// mCurrent--;
	// if (mCurrent < 0) {
	// mFlingVelocity = 0;
	// return;
	// }
	// } else {
	// return;
	// }
	// }
	// lazyLoadImage(mImageView, mCurrent);
	//
	// mFlingVelocity = mFlingVelocity * 0.90f;
	// Log.i("Velocity: " + mFlingVelocity);
	// final int maxFlingVelocity =
	// ViewConfiguration.get(FeedView.this).getScaledMaximumFlingVelocity();
	// float ratio = 1.0f -
	// ((mFlingVelocity>0?mFlingVelocity:-mFlingVelocity)/(float)maxFlingVelocity);
	// Log.i("Ratio: " + ratio);
	// if (ratio > 0.99)
	// return;
	// int delayMillis = (int) (ratio * 500 * 0.1f);
	// if (delayMillis < 1) {
	// mHandler.post(mFlingEffect);
	// } else {
	// mHandler.postDelayed(mFlingEffect, delayMillis);
	// }
	// Log.i("delayMillis: " + delayMillis);
	// }
	// }
	// };
	// public static final int DIALOG_MENU_HELP = 0;
	// public static final int DIALOG_MENU_ABOUT = 1;
	//
	// protected static final int MENU_SWITCH = Menu.FIRST;
	//
	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// // menu.add(0, MENU_PREF, 0,
	// //
	// getText(R.string.set_preferences)).setIcon(android.R.drawable.ic_menu_preferences);
	// if (mFeedType.equals("private")) {
	// menu.add(0, MENU_SWITCH, 0, "Public Album").setShowAsAction(
	// MenuItem.SHOW_AS_ACTION_IF_ROOM);
	// } else {
	// menu.add(0, MENU_SWITCH, 0, "Private Album").setShowAsAction(
	// MenuItem.SHOW_AS_ACTION_IF_ROOM);
	// }
	//
	// return super.onCreateOptionsMenu(menu);
	// }
	//
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// super.onOptionsItemSelected(item);
	// switch (item.getItemId()) {
	// /*
	// * case MENU_PREF: Intent settingsActivity = new
	// * Intent(getBaseContext(), Preferences.class);
	// * startActivity(settingsActivity); break;
	// */
	// case MENU_SWITCH:
	// mHandler.removeCallbacks(mCheckScroll);
	// mHandler.removeCallbacks(mRestoreScroll);
	// if (mFeedType.equals("private")) {
	// mFeedType = "public";
	// Intent intent = new Intent(getBaseContext(), FeedView.class);
	// intent.putExtra("feed_type", mFeedType);
	// startActivity(intent);
	// finish();
	// } else {
	// if (TextUtils.isEmpty(Utils.getString(this, "userId"))) {
	// Intent i;
	// i = new Intent(getBaseContext(), Login.class);
	// startActivity(i);
	// return false;
	// }
	// mFeedType = "private";
	// Intent intent = new Intent(getBaseContext(), FeedView.class);
	// intent.putExtra("feed_type", mFeedType);
	// startActivity(intent);
	// finish();
	// }
	// break;
	// }
	// return true;
	// }

	private boolean listIsAtTop() {
		if (listview.getChildCount() == 0)
			return true;
		return listview.getChildAt(0).getTop() == 0;
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this); // Add this method.
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		Fragment fragment;
		switch (itemPosition) {
		case 0:
			fragment = new FeedViewPublicFragment();
			// Bundle args = new Bundle();
			// args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position +
			// 1);
			// fragment.setArguments(args);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, fragment).commit();
			break;

		case 1:
			fragment = new FeedViewPrivateFragment();
			// Bundle args = new Bundle();
			// args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position +
			// 1);
			// fragment.setArguments(args);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, fragment).commit();
			break;
		case 2:
			fragment = new GalleryFragment();
			// Bundle args = new Bundle();
			// args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position +
			// 1);
			// fragment.setArguments(args);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, fragment).commit();
			break;
		}
		return true;
	}

	public static class TabsAdapter extends FragmentPagerAdapter implements
			ActionBar.TabListener, ViewPager.OnPageChangeListener {
		private final Context mContext;
		private final ActionBar mActionBar;
		private final ViewPager mViewPager;
		private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();

		static final class TabInfo {
			private final Class<?> clss;
			private final Bundle args;

			TabInfo(Class<?> _class, Bundle _args) {
				clss = _class;
				args = _args;
			}
		}

		public TabsAdapter(SherlockFragmentActivity activity, ViewPager pager) {
			super(activity.getSupportFragmentManager());
			mContext = activity;
			mActionBar = activity.getSupportActionBar();
			mViewPager = pager;
			mViewPager.setAdapter(this);
			mViewPager.setOnPageChangeListener(this);
		}

		public void addTab(ActionBar.Tab tab, Class<?> clss, Bundle args) {
			TabInfo info = new TabInfo(clss, args);
			tab.setTag(info);
			tab.setTabListener(this);
			mTabs.add(info);
			mActionBar.addTab(tab);
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return mTabs.size();
		}

		@Override
		public Fragment getItem(int position) {
			TabInfo info = mTabs.get(position);
			return Fragment.instantiate(mContext, info.clss.getName(),
					info.args);
		}

		@Override
		public void onPageScrolled(int position, float positionOffset,
				int positionOffsetPixels) {
		}

		@Override
		public void onPageSelected(int position) {
			mActionBar.setSelectedNavigationItem(position);
		}

		@Override
		public void onPageScrollStateChanged(int state) {
		}

		@Override
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			Object tag = tab.getTag();
			for (int i = 0; i < mTabs.size(); i++) {
				if (mTabs.get(i) == tag) {
					mViewPager.setCurrentItem(i);
				}
			}
		}

		@Override
		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		}

		@Override
		public void onTabReselected(Tab tab, FragmentTransaction ft) {
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		mHandler.removeCallbacks(mCheckScroll);
		mHandler.removeCallbacks(mRestoreScroll);
		// mHandler.removeCallbacks(mFlingEffect);
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance().activityStop(this); // Add this method.
		mHandler.removeCallbacks(mCheckScroll);
		mHandler.removeCallbacks(mRestoreScroll);
		// mHandler.removeCallbacks(mFlingEffect);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mHandler.removeCallbacks(mCheckScroll);
		mHandler.removeCallbacks(mRestoreScroll);
	};

}