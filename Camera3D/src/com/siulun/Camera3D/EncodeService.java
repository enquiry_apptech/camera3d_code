/*package com.siulun.Camera3D;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

public class EncodeService extends Service
{
	public final static int NETWORK = 0;
	public final static int GPS = 1;
    static final String ACTION_FOREGROUND = "com.siulun.Camera3D.FOREGROUND";
    static final String ACTION_BACKGROUND = "com.siulun.Camera3D.BACKGROUND";
    static final String TAG = "Camera3D";
	//private Handler objHandler = new Handler(); 
	private Context mContext;
    private NotificationManager mNM;
    private String mFilepath;

	
    private static SwitcherService sInstance;

	public static SwitcherService getInstance(Context c) {
	    if (sInstance == null) {
	        sInstance = new SwitcherService(c.getApplicationContext());
	    }
	    return sInstance;
	}
    private SwitcherService(Context c) {
        mContext = c;
    }
    
    private static final Class[] mStartForegroundSignature = new Class[] {
        int.class, Notification.class};
    private static final Class[] mStopForegroundSignature = new Class[] {
        boolean.class};
    private Method mStartForeground;
    private Method mStopForeground;
    private Object[] mStartForegroundArgs = new Object[2];
    private Object[] mStopForegroundArgs = new Object[1];
        
    public class LocalBinder extends Binder {
    	EncodeService getService() {
            return EncodeService.this;
        }
    }

    *//**
     * This is a wrapper around the new startForeground method, using the older
     * APIs if it is not available.
     *//*
    void startForegroundCompat(int id, Notification notification) {
        // If we have the new startForeground API, then use it.
        if (mStartForeground != null) {
            mStartForegroundArgs[0] = Integer.valueOf(id);
            mStartForegroundArgs[1] = notification;
            try {
                mStartForeground.invoke(this, mStartForegroundArgs);
            } catch (InvocationTargetException e) {
                // Should not happen.
            	if (Log.LOGV) Log.e("Unable to invoke startForeground", e);
            } catch (IllegalAccessException e) {
                // Should not happen.
            	if (Log.LOGV) Log.e("Unable to invoke startForeground", e);
            }
            return;
        }

        // Fall back on the old API.
        setForeground(true);
        mNM.notify(id, notification);
        mHandler.post(encoding);
    }

    *//**
     * This is a wrapper around the new stopForeground method, using the older
     * APIs if it is not available.
     *//*
    void stopForegroundCompat(int id) {
        // If we have the new stopForeground API, then use it.
        if (mStopForeground != null) {
            mStopForegroundArgs[0] = Boolean.TRUE;
            try {
                mStopForeground.invoke(this, mStopForegroundArgs);
            } catch (InvocationTargetException e) {
                // Should not happen.
            	if (Log.LOGV) Log.e("Unable to invoke stopForeground", e);
            } catch (IllegalAccessException e) {
                // Should not happen.
            	if (Log.LOGV) Log.e("Unable to invoke stopForeground", e);
            }
            return;
        }

        // Fall back on the old API.  Note to cancel BEFORE changing the
        // foreground state, since we could be killed at that point.
        mNM.cancel(id);
        setForeground(false);
    }

	@Override 
	public void onCreate() 
	{ 
		//setWakeLock(true);
	    // TODO Auto-generated method stub 
        mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        try {
            mStartForeground = getClass().getMethod("startForeground",
                    mStartForegroundSignature);
            mStopForeground = getClass().getMethod("stopForeground",
                    mStopForegroundSignature);
        } catch (NoSuchMethodException e) {
            // Running on an older platform.
            mStartForeground = mStopForeground = null;
        }
        
        // Display a notification about us starting.  We put an icon in the status bar.
	    super.onCreate();        
	}
	
	private Handler mHandler = new Handler();
	private Runnable encoding = new Runnable() {
        public void run() {
        	if (Log.LOGV) Log.v("Start encoding..");
        	
    		try {				    			
	    	    JavaGIFEncoder e = new JavaGIFEncoder();
	    		FileOutputStream bos = null;
	    		Bitmap scaled;
	    		File n = new File(mFilepath.replace(Camera3D.FOLDER_NAME, "Pictures").replace("_T.jpg", ".gif"));					    		
				bos = new FileOutputStream(n.getAbsolutePath());
	    	    e.start(bos);
	    	    e.setSize(400, 300);
	    	    e.setDelay(100);   // 1 frame per sec
	    	    e.setRepeat(0);
    	        File f = new File(mFilepath.replace("_T", "_" + Gallery.addZero( 0, 2)));
	    		scaled = Bitmap.createScaledBitmap(decodeFile(f,400), 400, 300, true);
	    	    e.addFrame(scaled);
	    	    scaled.recycle();
    	        f = new File(mFilepath.replace("_T", "_" + Gallery.addZero( 1, 2)));            			
	    		scaled = Bitmap.createScaledBitmap(decodeFile(f,400), 400, 300, true);
	    	    e.addFrame(scaled);
	    	    scaled.recycle();
	    	    e.finish();
	    	    bos.flush();
	    	    bos.close();
	    	    System.gc();
        		Toast.makeText(EncodeService.this, getText(R.string.toast_export) + "sdcard\\Pictures" + n.getName(),
        				Toast.LENGTH_LONG).show();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            mHandler.post(mDialogDismissExport);
        }
    };

    // Create runnable for posting
    final Runnable mDialogDismissExport = new Runnable() {
        public void run() {
        	onDestroy();
        }
    };
    
	@Override 
	public void onStart(Intent i, int startId) 
	{ 
	    // TODO Auto-generated method stub
        handleCommand(i);
	    super.onStart(i, startId);
	}

	@Override 
	public IBinder onBind(Intent intent) 
	{ 
	  // TODO Auto-generated method stub 
	  return null; 
	} 

    @Override
    public int onStartCommand(Intent i, int flags, int startId) {
        Log.i("Received start id " + startId + ": " + i);
        
        handleCommand(i);
        return START_STICKY;
    }
    
	@Override 
	public void onDestroy() 
	{ 
		// TODO Auto-generated method stub 
        mNM.cancel(R.string.local_service_started);
        
        Toast.makeText(this, R.string.local_service_stopped, Toast.LENGTH_SHORT).show();
        stopForegroundCompat(R.string.foreground_service_stopped);
        mHandler.removeCallbacks(encoding);
		super.onDestroy();
	} 

    void handleCommand(Intent intent) {
        mFilepath = intent.getStringExtra("filepath").replace("/.thumbnails", "");
        if (ACTION_FOREGROUND.equals(intent.getAction())) {
            // In this sample, we'll use the same text for the ticker and the expanded notification
            CharSequence text = getText(R.string.foreground_service_started) + mFilepath;

            // Set the icon, scrolling text and timestamp
            Notification notification = new Notification(R.drawable.icon, text,
                    System.currentTimeMillis());

            // The PendingIntent to launch our activity if the user selects this notification
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                    new Intent(this, Camera3D.class), 0);

            // Set the info for the views that show in the notification panel.
            notification.setLatestEventInfo(this, getText(R.string.local_service_label),
                           text, contentIntent);

            startForegroundCompat(R.string.foreground_service_started, notification);

        } else if (ACTION_BACKGROUND.equals(intent.getAction())) {
            stopForegroundCompat(R.string.foreground_service_started);
        }
    }
    
    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new LocalBinder();
    private static int[] leaving = null;

	public Bitmap decodeFile(String filename, int maxSize){
        File file = new File(filename);
	    return decodeFile(file, maxSize);
	}
	
	public Bitmap decodeFile(File file, int maxSize){
	    try {
	        //Decode image size
	        BitmapFactory.Options o = new BitmapFactory.Options();
	        o.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(new FileInputStream(file), null, o);
	        int scale = 1;
	        if (o.outHeight > maxSize || o.outWidth > maxSize) {
	            scale = (int) Math.pow(2, (int) Math.round(Math.log(maxSize / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
	        }
	        
	        //Decode with inSampleSize
	        BitmapFactory.Options o2 = new BitmapFactory.Options();
	        o2.inSampleSize=scale;
	        return BitmapFactory.decodeStream(new FileInputStream(file), null, o2);
	    } catch (FileNotFoundException e) {
	    	Log.e(e.toString());
	    } catch (Exception e) {
	    	Log.e(e.toString());
	    }
	    return null;
	}
}*/