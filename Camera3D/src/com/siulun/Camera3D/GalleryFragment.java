package com.siulun.Camera3D;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.google.analytics.tracking.android.EasyTracker;
import com.squareup.picasso.Picasso;

public class GalleryFragment extends SherlockFragment {
	private ArrayList<String> mFilesThumb;
	// private List<Bitmap> mBitmap;
	private ArrayList<Boolean> mIsLand;
	// private Bitmap [] mBitmap;
	private GridView g;
	private BaseAdapter mBaseAdapter;
	private int mColWidth;
	private static final int DIALOG_MENU_ABOUT = 0;
	private static final int DIALOG_ENCODE = 1;
	private ProgressDialog mDialog;
	private String mExportName;
	final int RESULT_OK = -1;
	private SharedPreferences mPrefs;
	ImageView cancelBtn;
	RelativeLayout banner;
	
	public static GalleryFragment newInstance(int num) {
		GalleryFragment f = new GalleryFragment();

		// Supply num input as an argument.
		Bundle args = new Bundle();
		args.putInt("num", num);
		f.setArguments(args);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.gallery, container, false);

//		if (Camera3D.DISPLAY_AD) {
//			AdView adView = (AdView) getActivity().findViewById(R.id.adView);
//			adView.loadAd(new AdRequest());
//		}	
		AdView adView = (AdView) v.findViewById(R.id.adView);
		adView.loadAd(new AdRequest());
		
		mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		banner = (RelativeLayout) v.findViewById(R.id.layout_banner);
		if (!mPrefs.getBoolean("showTapHold", true))
			banner.setVisibility(View.GONE);
		cancelBtn = (ImageView) v.findViewById(R.id.cancelBtn);
		cancelBtn.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				banner.setVisibility(View.GONE);
				SharedPreferences.Editor editor = mPrefs.edit();
				editor.putBoolean("showTapHold", false);
				editor.commit();

			}});
		final DisplayMetrics displaymetrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		final int height = displaymetrics.heightPixels;
		int width = displaymetrics.widthPixels;
		g = (GridView) v.findViewById(R.id.myGrid);
		if (width > height) {
			mColWidth = (int) width / 4 - 10;
			// width = height;
		} else
			mColWidth = (int) width / 3 - 10;

		g.setColumnWidth(mColWidth);

		return v;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mFilesThumb = Utils.getDir(Utils.getAbsPath() + "/"
				+ Camera3D.FOLDER_NAME_THUMB);
		mIsLand = new ArrayList<Boolean>();
		if (Utils.getBoolean(getActivity(), "isthumbrecreated", true)) {
			if (mFilesThumb != null) {

				if (mDialog != null) {
					if (!mDialog.isShowing())
						mDialog = ProgressDialog.show(getActivity(), "",
								getText(R.string.dialog_loading), true);
				} else {
					mDialog = ProgressDialog.show(getActivity(), "",
							getText(R.string.dialog_loading), true);
				}

				int len = mFilesThumb.size();
				for (int pos = 0; pos < len; pos++) {

					try {
						String uri = mFilesThumb.get(pos).replace("_T", "_01");
						uri = uri.replace("/.thumbnails", "");
						final File n = new File(uri);
						FileOutputStream bos = new FileOutputStream(
								mFilesThumb.get(pos));
						final Bitmap scaled = decodeFile(n, 200);
						scaled.compress(Bitmap.CompressFormat.JPEG, 80, bos);
						bos.flush();
						bos.close();
						scaled.recycle();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			Utils.getBoolean(getActivity(), "isthumbrecreated", true);
			mHandler.postDelayed(mDialogDismiss, 100);
		}

		if (mFilesThumb != null) {
			int len = mFilesThumb.size();
			// mBitmap = new Bitmap[len];
			for (int pos = 0; pos < len; pos++) {
				// for (int pos = len-1; pos >= 0; pos--)
				FileInputStream inputStream;
				try {
					inputStream = new FileInputStream(
							(String) mFilesThumb.get(pos));
					BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
					bitmapOptions.inJustDecodeBounds = true;
					BitmapFactory
							.decodeStream(inputStream, null, bitmapOptions);
					int imageWidth = bitmapOptions.outWidth;
					int imageHeight = bitmapOptions.outHeight;
					inputStream.close();
					mIsLand.add(imageWidth > imageHeight);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					mIsLand.add(true);
				} catch (IOException e) {
					e.printStackTrace();
					mIsLand.add(true);
				}
				// mBitmap.add(decodeFile((String) mFilesThumb.get(pos),
				// mColWidth));
			}
			// mBitmap[pos] = decodeFile((String)mFilesThumb.get(pos),
			// mColWidth);

			mBaseAdapter = new ImageAdapter(getActivity());
			g.setAdapter(mBaseAdapter);
			g.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView parent, View v,
						final int position, long id) {
					ViewPhoto(position);
				}
			});
			g.setOnCreateContextMenuListener(this);
		} else
			Toast.makeText(getActivity(), getText(R.string.toast_no_images),
					Toast.LENGTH_SHORT).show();
	}

//	@Override
//	public void onResume() {
//		super.onResume();
//
//		mFilesThumb = Utils.getDir(Utils.getAbsPath() + "/"
//				+ Camera3D.FOLDER_NAME_THUMB);
//		mIsLand = new ArrayList<Boolean>();
//		if (Utils.getBoolean(getActivity(), "isthumbrecreated", true)) {
//			if (mFilesThumb != null) {
//
//				if (mDialog != null) {
//					if (!mDialog.isShowing())
//						mDialog = ProgressDialog.show(getActivity(), "",
//								getText(R.string.dialog_loading), true);
//				} else {
//					mDialog = ProgressDialog.show(getActivity(), "",
//							getText(R.string.dialog_loading), true);
//				}
//
//				int len = mFilesThumb.size();
//				for (int pos = 0; pos < len; pos++) {
//
//					try {
//						String uri = mFilesThumb.get(pos).replace("_T", "_01");
//						uri = uri.replace("/.thumbnails", "");
//						final File n = new File(uri);
//						FileOutputStream bos = new FileOutputStream(
//								mFilesThumb.get(pos));
//						final Bitmap scaled = decodeFile(n, 200);
//						scaled.compress(Bitmap.CompressFormat.JPEG, 80, bos);
//						bos.flush();
//						bos.close();
//						scaled.recycle();
//					} catch (FileNotFoundException e) {
//						e.printStackTrace();
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//			}
//			Utils.getBoolean(getActivity(), "isthumbrecreated", true);
//			mHandler.postDelayed(mDialogDismiss, 100);
//		}
//
//		if (mFilesThumb != null) {
//			int len = mFilesThumb.size();
//			// mBitmap = new Bitmap[len];
//			for (int pos = 0; pos < len; pos++) {
//				// for (int pos = len-1; pos >= 0; pos--)
//				FileInputStream inputStream;
//				try {
//					inputStream = new FileInputStream(
//							(String) mFilesThumb.get(pos));
//					BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
//					bitmapOptions.inJustDecodeBounds = true;
//					BitmapFactory
//							.decodeStream(inputStream, null, bitmapOptions);
//					int imageWidth = bitmapOptions.outWidth;
//					int imageHeight = bitmapOptions.outHeight;
//					inputStream.close();
//					mIsLand.add(imageWidth > imageHeight);
//				} catch (FileNotFoundException e) {
//					e.printStackTrace();
//					mIsLand.add(true);
//				} catch (IOException e) {
//					e.printStackTrace();
//					mIsLand.add(true);
//				}
//				// mBitmap.add(decodeFile((String) mFilesThumb.get(pos),
//				// mColWidth));
//			}
//			// mBitmap[pos] = decodeFile((String)mFilesThumb.get(pos),
//			// mColWidth);
//
//			mBaseAdapter = new ImageAdapter(getActivity());
//			g.setAdapter(mBaseAdapter);
//			g.setOnItemClickListener(new OnItemClickListener() {
//				public void onItemClick(AdapterView parent, View v,
//						final int position, long id) {
//					ViewPhoto(position);
//				}
//			});
//			g.setOnCreateContextMenuListener(getActivity());
//		} else
//			Toast.makeText(getActivity(), getText(R.string.toast_no_images),
//					Toast.LENGTH_SHORT).show();
//	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// setContentView(R.layout.myLayout);
	}

	// Create runnable for posting
	final Runnable mDialogDismiss = new Runnable() {
		public void run() {
			try {
				if (mDialog != null && mDialog.isShowing())
					mDialog.dismiss();
			} catch (Exception e) {
				// nothing
			}
		}
	};

	public Bitmap decodeFile(String filename, int maxSize) {
		File file = new File(filename);
		return decodeFile(file, maxSize);
	}

	public Bitmap decodeFile(File file, int maxSize) {
		try {
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(file), null, o);
			int scale = 1;
			if (o.outHeight > maxSize || o.outWidth > maxSize) {
				scale = (int) Math.pow(
						2,
						(int) Math.round(Math.log(maxSize
								/ (double) Math.max(o.outHeight, o.outWidth))
								/ Math.log(0.5)));
			}

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(file), null,
					o2);
		} catch (FileNotFoundException e) {
			// Log.e(e.toString());
			return null;
		}
	}

	public void onCreateContextMenu(ContextMenu menu, View view,
			ContextMenuInfo menuInfo) {
		// Inflate the menu from xml.
		getActivity().getMenuInflater().inflate(R.menu.gallery_menu, menu);
		final AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
		File file = new File((String) mFilesThumb.get(info.position));
		menu.setHeaderTitle(file.getName().replace("_T", ""));
	}

	private int mMSTime;
	private int mId;
	// private RadioGroup mEncodeSize;
	private Spinner mEncodeSpinner;
	private TextView mMSText;
	private SeekBar mMS;
	
	@Override
	public boolean onContextItemSelected(final MenuItem item) {
		final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		mId = (int) info.id;
		switch (item.getItemId()) {
		
		case R.id.menu_upload:
			if (!Utils.getBoolean(getActivity(), "isLogin", false)) {
				Toast.makeText(getActivity(),"Please login first...",Toast.LENGTH_LONG).show();
				Intent i = new Intent(getActivity(), Login.class);
				startActivity(i);
				return false;
			}

			String file_path = (String) mFilesThumb.get(mId);
//			mFilesThumb.remove(mId);

			
//            Intent intent = new Intent(getActivity(), UploadService.class);
//            intent.putStringArrayListExtra(UploadService.ARG_FILE_PATH, files);
//            startService(intent);

            Intent intent = new Intent(getActivity(), ImageInfoSubmit.class);
            intent.putExtra("file_path", file_path);
            startActivity(intent);            
            
			return true;
		case R.id.menu_delete:
			// Confirm that the alarm will be deleted.
			new AlertDialog.Builder(getActivity())
					.setTitle(getString(R.string.dialog_delete))
					.setMessage(getString(R.string.dialog_delete_confirm))
					.setPositiveButton(android.R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface d, int w) {
									delete_photo(mId);
								}
							}).setNegativeButton(android.R.string.cancel, null)
					.show();
			return true;

		case R.id.menu_view:
			ViewPhoto(mId);
			return true;

		case R.id.menu_export:
			final Dialog dialog = new Dialog(getActivity());
			dialog.setContentView(R.layout.encode);
			// dialog.setTitle(null);
			dialog.setTitle(getText(R.string.dialog_export));
			dialog.setCancelable(true);
			// there are a lot of settings, for dialog, check them all out!
			mMS = (SeekBar) dialog.findViewById(R.id.mMS);
			// mEncodeSize = (RadioGroup) dialog.findViewById(R.id.mEncodeSize);
			mEncodeSpinner = (Spinner) dialog.findViewById(R.id.mEncodeSpinner);
			mMSText = (TextView) dialog.findViewById(R.id.mMSText);

			// set up button
			Button mBtnOK = (Button) dialog.findViewById(R.id.mBtnOK);
			mMS.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {
					switch (seekBar.getId()) {
					case (R.id.mMS):
						mMSTime = progress + 100;
						mMSText.setText(mMSTime + " ms");
						break;
					}
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {

				}

				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
				}
			});

			mBtnOK.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mFilepath = (String) mFilesThumb.get(mId).replace(
							"/.thumbnails", "");
					mDialog = ProgressDialog.show(getActivity(), "",
							getText(R.string.dialog_encoding), true);

					Thread t = new Thread() {
						public void run() {
							try {
								File sdCard = Environment
										.getExternalStorageDirectory();
								File f = new File(sdCard.getAbsolutePath()
										+ "/sd");
								String AbsPath;
								if (f.exists())
									AbsPath = sdCard.getAbsolutePath() + "/sd";
								else
									AbsPath = sdCard.getAbsolutePath();

								f = new File(AbsPath + "/Pictures");
								if (!f.exists()) {
									f.mkdir();
								}
								// int eid =
								// mEncodeSize.getCheckedRadioButtonId();
								int eid = mEncodeSpinner
										.getSelectedItemPosition();
								int ex = 400, ey = 300;
								switch (eid) {
								case 0:
									ex = 400;
									ey = 300;
									break;
								case 1:
									ex = 600;
									ey = 450;
									break;
								case 2:
									ex = 800;
									ey = 600;
									break;
								case 3:
									ex = 1024;
									ey = 768;
									break;
								case 4:
									ex = 1280;
									ey = 1024;
									break;
								case 5:
									ex = 1920;
									ey = 1080;
									break;
								}

								JavaGIFEncoder e = new JavaGIFEncoder();
								FileOutputStream bos = null;
								Bitmap scaled;

								// Check Landscape or Portrait
								f = new File(mFilepath.replace("_T", "_"
										+ Utils.addZero(0, 2)));
								if (f.exists()) {
									BitmapFactory.Options o = new BitmapFactory.Options();
									o.inJustDecodeBounds = true;
									BitmapFactory.decodeStream(
											new FileInputStream(f), null, o);
									if (o.outWidth < o.outHeight) {
										int temp = ex;
										ex = ey;
										ey = temp;
									}
								}

								File n = new File(mFilepath.replace(
										Camera3D.FOLDER_NAME, "Pictures")
										.replace("_T.jpg", ".gif"));
								bos = new FileOutputStream(n.getAbsolutePath());
								e.start(bos);
								e.setSize(ex, ey);
								e.setDelay(mMSTime); // 1 frame per sec
								e.setRepeat(0);

								boolean done = true;
								int count = 0;
								do {
									f = new File(mFilepath.replace("_T", "_"
											+ Utils.addZero(count, 2)));
									if (f.exists()) {
										scaled = Bitmap.createScaledBitmap(
												decodeFile(f, (ex > ey) ? ex
														: ey), ex, ey, true);
										e.addFrame(scaled);
										scaled.recycle();
										done = true;
										count++;
									} else
										done = false;
								} while (done);

								e.finish();
								bos.flush();
								bos.close();

								System.gc();
								mExportName = n.getName();
							} catch (FileNotFoundException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							mHandler.post(mDialogDismissExport);
						}
					};
					t.start();
					dialog.dismiss();
				}
			});
			dialog.show();
			return true;

		default:
			break;
		}
		return super.onContextItemSelected(item);
	}

	private String mFilepath;
	private Handler mHandler = new Handler();
	// Create runnable for posting
	final Runnable mDialogDismissExport = new Runnable() {
		public void run() {
			if (mDialog.isShowing())
				mDialog.dismiss();
			Toast.makeText(
					getActivity(),
					getText(R.string.toast_export) + "sdcard\\Pictures\\"
							+ mExportName, Toast.LENGTH_LONG).show();
		}
	};

	private void ViewPhoto(int position) {
//		mFilesThumb.remove(mId);

		
//        Intent intent = new Intent(getActivity(), UploadService.class);
//        intent.putStringArrayListExtra(UploadService.ARG_FILE_PATH, files);
//        startService(intent);

        
		
		Intent i = new Intent(getActivity(), View3D.class);
		i.putExtra("filepath", (String) mFilesThumb.get(position));
		i.putExtra("fileindex", position);
		i.putExtra("isLand", mIsLand.get(position));
		startActivityForResult(i, Camera3D.ACTIVITY_DELETE);
	}



	private boolean delete_photo(int pos) {

		String file_path = (String) mFilesThumb.get(pos);
		mFilesThumb.remove(pos);
		BitmapUtils.delete_file(file_path);

		file_path = file_path.replace("/.thumbnails", "");
		/*
		 * File f = new File(file_path.replace("_T", "_L")); if (f.exists()) {
		 * BitmapUtils.delete_file(file_path.replace("_T", "_LL"));
		 * BitmapUtils.delete_file(file_path.replace("_T", "_L"));
		 * BitmapUtils.delete_file(file_path.replace("_T", "_C"));
		 * BitmapUtils.delete_file(file_path.replace("_T", "_R"));
		 * BitmapUtils.delete_file(file_path.replace("_T", "_RR")); } else { }
		 */
		boolean done = false;
		int count = 0;
		do {
			done = BitmapUtils.delete_file(file_path.replace("_T",
					"_" + Utils.addZero(count, 2)));
			count++;
		} while (done);
		// mBitmap.get(pos).recycle();
		// mBitmap.remove(pos);
		mBaseAdapter.notifyDataSetChanged();
		return true;

	}

	public static boolean delete_photo(String file_path) {

		BitmapUtils.delete_file(file_path);

		file_path = file_path.replace("/.thumbnails", "");
		/*
		 * File f = new File(file_path.replace("_T", "_L")); if (f.exists()) {
		 * BitmapUtils.delete_file(file_path.replace("_T", "_LL"));
		 * BitmapUtils.delete_file(file_path.replace("_T", "_L"));
		 * BitmapUtils.delete_file(file_path.replace("_T", "_C"));
		 * BitmapUtils.delete_file(file_path.replace("_T", "_R"));
		 * BitmapUtils.delete_file(file_path.replace("_T", "_RR")); } else { }
		 */
		boolean done = false;
		int count = 0;
		do {
			done = BitmapUtils.delete_file(file_path.replace("_T",
					"_" + Utils.addZero(count, 2)));
			count++;
		} while (done);
		return true;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent i) {
	//	super.onActivityResult(requestCode, resultCode, i);
		if (resultCode == RESULT_OK) {
			if (requestCode == Camera3D.ACTIVITY_DELETE) {
				int pos = i.getIntExtra("fileindex", -1);
				if (pos != -1 && mFilesThumb.size() > 0) {
					mFilesThumb.remove(pos);
					// mBitmap.get(pos).recycle();
					// mBitmap.remove(pos);
					mBaseAdapter.notifyDataSetChanged();
				}
			}
		}
	}
	/*
	public void removeThumb(int pos) {
		if (pos != -1 && mFilesThumb.size() > 0) {
			mFilesThumb.remove(pos);
			// mBitmap.get(pos).recycle();
			// mBitmap.remove(pos);
			mBaseAdapter.notifyDataSetChanged();
		}
	}
*/
	@Override
	public void onStart() {
		super.onStart();
	//	EasyTracker.getInstance().activityStart(getActivity()); // Add getActivity() method.
	//	IntentFilter f = new IntentFilter();
	//	f.addAction(UploadService.UPLOAD_STATE_CHANGED_ACTION);
	//	registerReceiver(uploadStateReceiver, f);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		mHandler.removeCallbacks(mDialogDismiss);
		mHandler.removeCallbacks(mDialogDismissExport);
		// mHandler.removeCallbacks(mFlingEffect);
	}

	@Override
	public void onStop() {
//		unregisterReceiver(uploadStateReceiver);
		super.onStop();
		mHandler.removeCallbacks(mDialogDismiss);
		mHandler.removeCallbacks(mDialogDismissExport);
//		EasyTracker.getInstance().activityStop(getActivity()); // Add getActivity() method.
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
		mHandler.removeCallbacks(mDialogDismissExport);
		
	}


	public class ImageAdapter extends BaseAdapter {
		Bitmap myBitmap;

		public ImageAdapter(Context c) {
			mContext = c;
		}

		public int getCount() {
			return mFilesThumb.size();
		}

		public String getItem(int position) {
			return mFilesThumb.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ImageView imageView;
			if (convertView == null) {
				imageView = new ImageView(mContext);
				// imageView.setLayoutParams(new
				// GridView.LayoutParams(mColWidth, mColWidth));
				imageView.setLayoutParams(new GridView.LayoutParams(mColWidth,
						mColWidth));
				imageView.setAdjustViewBounds(false);
				imageView.setScaleType(ScaleType.CENTER_CROP);
				imageView.setPadding(4, 4, 4, 4);
			} else {
				imageView = (ImageView) convertView;
			}

			// imageView.setImageBitmap(mBitmap.get(position));
			Picasso.with(getActivity()).load(new File(getItem(position)))
					.resize(mColWidth, mColWidth).centerCrop().into(imageView);
			return imageView;
		}

		private Context mContext;
	}

}
