package com.siulun.Camera3D;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.WindowManager;

public class Utils {

	/**
	 * Check if OS version has built-in external cache dir method.
	 * 
	 * @return
	 */
	public static boolean hasExternalCacheDir() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
	}

	/**
	 * Check if OS version has built-in external cache dir method.
	 * 
	 * @return
	 */
	public static boolean hasJPEGFormat() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
	}

	/**
	 * Check if OS version has built-in external cache dir method.
	 * 
	 * @return
	 */
	public static boolean hasCameraFacing() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
	}

	/**
	 * Get the external app cache directory.
	 * 
	 * @param context
	 *            The context to use
	 * @return The external cache dir
	 */
	@SuppressLint("NewApi")
	public static File getExternalCacheDir(Context context) {
		if (hasExternalCacheDir()) {
			if (context.getExternalCacheDir() != null)
				return context.getExternalCacheDir();
		}

		// Before Froyo we need to construct the external cache dir ourselves
		final String cacheDir = "/Android/data/" + context.getPackageName()
				+ "/cache/";
		return new File(Environment.getExternalStorageDirectory().getPath()
				+ cacheDir);
	}
	
	public static String getString(final Context context, final String key) {
		SharedPreferences sp;
		try {
			sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		} catch(NullPointerException e) {
			return "";
		}
		return sp.getString(key, "");
	}

	public static void setString(final Context context, final String key,
			final String value) {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... voids) {
				SharedPreferences sp = PreferenceManager
						.getDefaultSharedPreferences(context);
				sp.edit().putString(key, value).commit();
				return null;
			}
		}.execute();
	}
	
	public static int getInt(final Context context, final String key) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sp.getInt(key, -1);
	}

	public static void setInt(final Context context, final String key,
			final int value) {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... voids) {
				SharedPreferences sp = PreferenceManager
						.getDefaultSharedPreferences(context);
				sp.edit().putInt(key, value).commit();
				return null;
			}
		}.execute();
	}
	
	public static Boolean getBoolean(final Context context, final String key,
			final boolean default_value) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sp.getBoolean(key, default_value);
	}

	public static void setBoolean(final Context context, final String key,
			final Boolean value) {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... voids) {
				SharedPreferences sp = PreferenceManager
						.getDefaultSharedPreferences(context);
				sp.edit().putBoolean(key, value).commit();
				return null;
			}
		}.execute();
	}

	public static ArrayList<String> getDir(String dirPath) {
		ArrayList<String> item = null;
		item = new ArrayList<String>();

		File f = new File(dirPath);
		File[] files = f.listFiles();
		if (files == null)
			return null;
		for (File file : files) {
			if (!file.isDirectory() && file.getName().indexOf("_T") > 0)
				item.add(file.getPath());
		}
		Collections.sort(item);
		Collections.reverse(item);
		return item;
	}

	public static String getAbsPath() {

		File sdCard = Environment.getExternalStorageDirectory();
		File f = new File(sdCard.getAbsolutePath() + "/sd");
		if (f.exists())
			return sdCard.getAbsolutePath() + "/sd";
		else
			return sdCard.getAbsolutePath();
	}
	
	public static String addZero(int pos, int digi) {
		String spos = "000" + pos;
		if (spos.length() > digi)
			spos = spos.substring(spos.length() - digi, spos.length());
		return spos;
	}
	
	public static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is),
				8 * 1024);
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return sb.toString();
	}
	
	public static int getRotation(Context context) {
		return ((WindowManager) context.getSystemService(context.WINDOW_SERVICE))
				.getDefaultDisplay().getRotation();
	}
}
