package com.siulun.Camera3D;

import android.graphics.Bitmap;

import com.squareup.picasso.Transformation;

final class CropSquareTransformation implements Transformation {
	
	int mWidth, mHeight;
	
	CropSquareTransformation(int w, int h) {
		mWidth = w;
		mHeight = h;
	}
	
	@Override
	public Bitmap transform(Bitmap source) {
//		int size = Math.min(source.getWidth(), source.getHeight());
		int width = mWidth, height = mHeight;

//		float scale;
//		if ((mWidth > mHeight && source.getHeight() > source.getWidth()) || (mWidth < mHeight && source.getHeight() < source.getWidth())) {
//			scale = (float) source.getWidth() / (float) width;
//			width = source.getWidth();
//			height = (int )((float) height * scale);
//		} else if ((mWidth > mHeight && source.getHeight() < source.getWidth()) || (mWidth < mHeight && source.getHeight() > source.getWidth())){
//			scale = (float) source.getHeight() / (float) height;
//			height = source.getHeight();
//			width = (int )((float) width * scale);
//		} else {
//			scale = (float) source.getHeight() / (float) height;
//			height = source.getHeight();
//			width = (int )((float) width * scale);
//		}
		int x = (source.getWidth() - width) / 2;
		int y = (source.getHeight() - height) / 2;
		
//		if (x <= 0 || y <= 0) {
//
//			width = mWidth;
//			height = mHeight;
//			if (height > source.getHeight()) {
//				scale = (float) source.getHeight() / (float) height;
//				height = source.getHeight();
//				width = (int )((float) width * scale);
//
//				if (width > source.getWidth()) {
//					scale = (float) source.getWidth() / (float) width;
//					width = source.getWidth();
//					height = (int )((float) height * scale);
//				}
//			}
//			x = (source.getWidth() - width) / 2;
//			y = (source.getHeight() - height) / 2;
//		}

		if (x > 0 && y > 0) {
			Bitmap squaredBitmap = Bitmap
					.createBitmap(source, x, y, mWidth, mHeight);
			if (squaredBitmap != source) {
				source.recycle();
			}
			return squaredBitmap;
		} else
			return source;
	}
	
	public void setSize(int w, int h) {
		mWidth = w;
		mHeight = h;
	}

	@Override
	public String key() {
		return "square()";
	}
}
