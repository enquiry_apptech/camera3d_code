package com.siulun.Camera3D;

import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.actionbarsherlock.app.SherlockFragment;
import com.siulun.Camera3D.adapter.FeedViewPrivateAdapter;
import com.siulun.Camera3D.adapter.FeedViewSampleAdapter;
import com.siulun.Camera3D.model.FeedSet;

public class FeedViewSampleFragment extends SherlockFragment {
	ListView listview;
	RelativeLayout mSpinner;
	String mFeedType;
	// private GestureDetector detector;
	// private myGestureListener gListener;
	// private boolean mIsLand;

	FeedViewSampleAdapter mAdapter;
	Boolean mStartScrolling = false;
	float lastX, lastY, distanceX, distanceY;
	private final static int TOUCH_DISTANCE = 10;
	ArrayList<FeedSet> mSocialSet = new ArrayList<FeedSet>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// if (mFeedType.equals("private")
		// && TextUtils.isEmpty(Utils.getString(getActivity(), "userId"))) {
		// Intent i;
		// i = new Intent(getActivity(), Login.class);
		// startActivity(i);
		// }
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);

		final View v = inflater.inflate(R.layout.feed, container, false);

		listview = (ListView) v.findViewById(R.id.listView);
		mSpinner = (RelativeLayout) v.findViewById(R.id.spinner);
		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);

		mSocialSet.add(new FeedSet("I3D_20130822_021643", 77, "Wedding bands", "", 0));
		mSocialSet.add(new FeedSet("I3D_20130822_010425", 82, "Pendant", "", 0));
		mSocialSet.add(new FeedSet("I3D_20130822_004020", 50, "Diamond ring", "", 0));

		mSpinner.setVisibility(View.GONE);
		init();
	}

	public void init() {
		mAdapter = new FeedViewSampleAdapter(getActivity(), mSocialSet);
		listview.setAdapter(mAdapter);

		mSpinner.setVisibility(View.GONE);
		listview.setVisibility(View.VISIBLE);
		listview.setOnScrollListener(new OnScrollListener() {

			int mVisibileItemCount;

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				// Log.i(""+scrollState);
				mStartScrolling = true;
				if (scrollState == OnScrollListener.SCROLL_STATE_IDLE) {
					if (listIsAtTop())
						mAdapter.setCurrentFeed(0);
					else if (mVisibileItemCount > 2) {
						mAdapter.setCurrentFeed(view.getFirstVisiblePosition() + 1);
					} else
						mAdapter.setCurrentFeed(view.getLastVisiblePosition());
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				mVisibileItemCount = visibleItemCount;
				// Log.i(firstVisibleItem+" : "+visibleItemCount+" : "+view.getLastVisiblePosition());
			}
		});
		mAdapter.setCurrentFeed(0);
		mHandler.postDelayed(mCheckScroll, 100);
	}

	Handler mHandler = new Handler();

	Runnable mCheckScroll = new Runnable() {
		@Override
		public void run() {
			mHandler.removeCallbacks(mCheckScroll);
			mHandler.postDelayed(mCheckScroll, 300);
			// Log.i("getTouchingStatus : " + mAdapter.getTouchingStatus());
			// if (mFeedType.equals("private")) {
			// if (mAdapter.getTouchingStatus()) {
			// listview.setScrollContainer(false);
			// } else {
			// mHandler.postDelayed(mRestoreScroll, 600);
			// }
			// } else {
			if (mAdapter.getTouchingStatus()) {
				listview.setScrollContainer(false);
			} else {
				mHandler.postDelayed(mRestoreScroll, 600);
			}
			// }
		}
	};

	Runnable mRestoreScroll = new Runnable() {
		@Override
		public void run() {
			listview.setScrollContainer(true);
		}
	};

	// @Override
	// protected void onResume() {
	// super.onResume();
	// mIsLand = (getResources().getConfiguration().orientation ==
	// Configuration.ORIENTATION_LANDSCAPE);
	// gListener = new myGestureListener();
	// detector = new GestureDetector(this, gListener);
	// }

	// @Override
	// public boolean onTouchEvent(MotionEvent event) {
	// if (event.getY() > 90) {
	// if (detector.onTouchEvent(event)) {
	// return detector.onTouchEvent(event);
	// } else {
	// return super.onTouchEvent(event);
	// }
	// } else {
	// return false;
	// }
	// }

	// private final static int TOUCH_DISTANCE = 10;
	// final Handler mHandler = new Handler();
	// public class myGestureListener implements
	// GestureDetector.OnGestureListener {
	// @Override
	// public boolean onScroll(MotionEvent e1, MotionEvent e2,
	// float distanceX, float distanceY) {
	// Log.i("myGestureListener"+" : "+distanceX+" : "+distanceY);
	// if (distanceX < -TOUCH_DISTANCE) {
	// mAdapter.viewLastFrame();
	// } else if (distanceX > TOUCH_DISTANCE) {
	// mAdapter.viewNextFrame();
	// }
	// return true;
	// }
	//
	// @Override
	// public boolean onDown(MotionEvent arg0) {
	// Log.i("onDown");
	// // mHandler.removeCallbacks(mFlingEffect);
	// return true;
	// }
	//
	// @Override
	// public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
	// float velocityY) {
	// Log.i("onFling");
	// // if (mIsLand)
	// // mFlingVelocity = velocityX;
	// // else
	// // mFlingVelocity = velocityY;
	// // mHandler.removeCallbacks(mFlingEffect);
	// // mHandler.postDelayed(mFlingEffect, 100);
	// return false;
	// }
	//
	// @Override
	// public void onLongPress(MotionEvent e) {
	// /*
	// * if (bm != null) {
	// *
	// * }
	// */
	// }
	//
	// @Override
	// public void onShowPress(MotionEvent e) {
	// }
	//
	// @Override
	// public boolean onSingleTapUp(MotionEvent e) {
	// return false;
	// }
	// }
	// private float mFlingVelocity;

	// private final Runnable mFlingEffect = new Runnable() {
	// @Override
	// public void run() {
	// mHandler.removeCallbacks(mFlingEffect);
	// if (mFlingVelocity != 0) {
	// if (mFlingVelocity < 0) {
	// if (mCurrent < mImageCount - 1) {
	// mCurrent++;
	// if (mCurrent >= mImageCount) {
	// mFlingVelocity = 0;
	// return;
	// }
	// } else {
	// return;
	// }
	// } else {
	// if (mCurrent > 0){
	// mCurrent--;
	// if (mCurrent < 0) {
	// mFlingVelocity = 0;
	// return;
	// }
	// } else {
	// return;
	// }
	// }
	// lazyLoadImage(mImageView, mCurrent);
	//
	// mFlingVelocity = mFlingVelocity * 0.90f;
	// Log.i("Velocity: " + mFlingVelocity);
	// final int maxFlingVelocity =
	// ViewConfiguration.get(FeedView.this).getScaledMaximumFlingVelocity();
	// float ratio = 1.0f -
	// ((mFlingVelocity>0?mFlingVelocity:-mFlingVelocity)/(float)maxFlingVelocity);
	// Log.i("Ratio: " + ratio);
	// if (ratio > 0.99)
	// return;
	// int delayMillis = (int) (ratio * 500 * 0.1f);
	// if (delayMillis < 1) {
	// mHandler.post(mFlingEffect);
	// } else {
	// mHandler.postDelayed(mFlingEffect, delayMillis);
	// }
	// Log.i("delayMillis: " + delayMillis);
	// }
	// }
	// };
	// public static final int DIALOG_MENU_HELP = 0;
	// public static final int DIALOG_MENU_ABOUT = 1;
	//
	// protected static final int MENU_SWITCH = Menu.FIRST;
	//
	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// // menu.add(0, MENU_PREF, 0,
	// //
	// getText(R.string.set_preferences)).setIcon(android.R.drawable.ic_menu_preferences);
	// if (mFeedType.equals("private")) {
	// menu.add(0, MENU_SWITCH, 0, "Public Album").setShowAsAction(
	// MenuItem.SHOW_AS_ACTION_IF_ROOM);
	// } else {
	// menu.add(0, MENU_SWITCH, 0, "Private Album").setShowAsAction(
	// MenuItem.SHOW_AS_ACTION_IF_ROOM);
	// }
	//
	// return super.onCreateOptionsMenu(menu);
	// }
	//
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// super.onOptionsItemSelected(item);
	// switch (item.getItemId()) {
	// /*
	// * case MENU_PREF: Intent settingsActivity = new
	// * Intent(getBaseContext(), Preferences.class);
	// * startActivity(settingsActivity); break;
	// */
	// case MENU_SWITCH:
	// mHandler.removeCallbacks(mCheckScroll);
	// mHandler.removeCallbacks(mRestoreScroll);
	// if (mFeedType.equals("private")) {
	// mFeedType = "public";
	// Intent intent = new Intent(getBaseContext(),
	// FeedViewPublicFragment.class);
	// intent.putExtra("feed_type", mFeedType);
	// startActivity(intent);
	// finish();
	// } else {
	// if (TextUtils.isEmpty(Utils.getString(this, "userId"))) {
	// Intent i;
	// i = new Intent(getBaseContext(), Login.class);
	// startActivity(i);
	// return false;
	// }
	// mFeedType = "private";
	// Intent intent = new Intent(getBaseContext(),
	// FeedViewPublicFragment.class);
	// intent.putExtra("feed_type", mFeedType);
	// startActivity(intent);
	// finish();
	// }
	// break;
	// }
	// return true;
	// }

	private boolean listIsAtTop() {
		if (listview.getChildCount() == 0)
			return true;
		return listview.getChildAt(0).getTop() == 0;
	}

	@Override
	public void onStop() {
		super.onStop();
		mHandler.removeCallbacks(mCheckScroll);
		mHandler.removeCallbacks(mRestoreScroll);
		// mHandler.removeCallbacks(mFlingEffect);
	}
}
