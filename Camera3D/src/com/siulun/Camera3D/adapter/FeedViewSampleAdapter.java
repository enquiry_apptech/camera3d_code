package com.siulun.Camera3D.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.siulun.Camera3D.Log;
import com.siulun.Camera3D.R;
import com.siulun.Camera3D.model.FeedSet;
import com.squareup.picasso.Picasso;

public class FeedViewSampleAdapter extends BaseAdapter {
	
	private final static int DEFAULT_DURATION = 100;
	
	Context mContext;
	LayoutInflater mInflater;
	ArrayList<FeedSet> mFeedSet = new ArrayList<FeedSet>();
	SparseArray<ImageView> mImageView = new SparseArray<ImageView>();
	int mCurrentFeed = -1, mCurrentFrame = 0;
	private final static int TOUCH_DISTANCE = 5;
	public boolean isTouching = false;

	public FeedViewSampleAdapter(Context context, ArrayList<FeedSet> socialSet) {
		mContext = context;
		mInflater = LayoutInflater.from(context);
		mFeedSet = socialSet;
//		URL.setURLStreamHandlerFactory(new OkHttpClient());
	}

	public void setCurrentFeed(int pos) {
		if (pos != mCurrentFeed) {
//			Log.i("--- setCurrentFeed: " + pos);
			mCurrentFeed = pos;
			mCurrentFrame = 0;
			mHandler.removeCallbacks(mPlay);
			mHandler.postDelayed(mPlay, 50);
		}
	}

	public void viewNextFrame(){
		mCurrentFrame++;
		setCurrentFrame(mCurrentFrame);
	}
	
	public void viewLastFrame(){
		mCurrentFrame--;
		setCurrentFrame(mCurrentFrame);
	}

	public void setCurrentFrame(int frame) {
		mHandler.removeCallbacks(mPlay);
//		Log.i("--- frame: " + frame);
		mCurrentFrame = frame;
		mCurrentFrame = getCurrentFrame(mCurrentFeed);
//		Log.i("--- setCurrentFrame: " + mCurrentFrame);

		FeedSet fs = getItem(mCurrentFeed);
		if (mImageView.get(mCurrentFeed) != null) {
			String url = getItem(mCurrentFeed).getFilePath(0);
			Picasso.with(mContext).load(mContext.getResources().getIdentifier(url, "drawable", mContext.getPackageName())).into(mImageView.get(mCurrentFeed));
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mFeedSet.size();
	}

	@Override
	public FeedSet getItem(int pos) {
		// TODO Auto-generated method stub
//		Log.i("pos : " + pos);
		return mFeedSet.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		// TODO Auto-generated method stub
		return 0;
	}

	float lastX, lastY, distanceX, distanceY;
	
	@Override
	public View getView(final int pos, View view, final ViewGroup parent) {
		if (view != null) {
			holder = (ViewHolder) view.getTag();
		} else {
			view = mInflater.inflate(R.layout.feed_item, parent, false);
			holder = new ViewHolder();
			holder.imageView = (ImageView) view.findViewById(R.id.imageView);
			holder.imageHolder = (FrameLayout) view.findViewById(R.id.imageHolder);
			holder.title = (TextView) view.findViewById(R.id.title);
			holder.desc = (TextView) view.findViewById(R.id.desc);
			holder.desc.setVisibility(View.GONE);
			holder.editButton = (Button) view.findViewById(R.id.edit);
			holder.editButton.setVisibility(View.GONE);
//			holder.commentButton = (Button) view.findViewById(R.id.comment);
			view.setTag(holder);
		}

		holder.title.setText(getItem(pos).getTitle());
		mImageView.put(pos, holder.imageView);
		holder.imageView.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				distanceX = event.getX() - lastX;
				distanceY = event.getY() - lastY;
				lastX = event.getX();
				lastY = event.getY();
//				if (distanceX < 0) distanceX = -distanceX;
//				if (distanceY < 0) distanceY = -distanceY;
//				Log.i("lastY" + lastY);
//				Log.i("lastX" + lastX);
//				Log.i("distanceX : " + distanceX);
//				Log.i("distanceY" + distanceY);
				
				int action = event.getAction();
//				Log.i("getX" + event.getX());
//				Log.i("getY" + event.getY());
				switch (action) {
				case MotionEvent.ACTION_DOWN:
//					Log.i("ACTION_DOWN");
					isTouching = true;
					break;

				case MotionEvent.ACTION_MOVE:
//					Log.i("ACTION_MOVE");
					if (distanceX < -TOUCH_DISTANCE) {
//						listview.setScrollContainer(false);
						Log.i("viewLastFrame");
						viewLastFrame();
					} else if (distanceX > TOUCH_DISTANCE) {
//						listview.setScrollContainer(false);
						Log.i("viewNextFrame");
						viewNextFrame();
					} 
//					else
//						listview.setScrollContainer(true);
					break;

				case MotionEvent.ACTION_UP:
//					Log.i("ACTION_UP");
//					mHandler.postDelayed(new Runnable() {
//						
//						@Override
//						public void run() {
//							listview.setScrollContainer(true);
//						}
//					}, 100);
					isTouching = false;
					break;
				}
				return true;
			}
		});

		String url = getItem(pos).getFilePath(0);	    
		if (!TextUtils.isEmpty(url))
			Picasso.with(mContext).load(mContext.getResources().getIdentifier(url, "drawable", mContext.getPackageName())).into(holder.imageView);

		// Log.i(Camera3D.TAG, getItem(pos).getImageUrl(0));
		return view;
	}

	ViewHolder holder;
	static class ViewHolder {
		TextView title;
		TextView desc;
		ImageView imageView;
		FrameLayout imageHolder;
		Button editButton;
//		Button commentButton;
	}
	
	public boolean getTouchingStatus() {
		return isTouching;
	}	                                   

	Handler mHandler = new Handler();
	Runnable mPlay = new Runnable() {
		@Override
		public void run() {
			// Log.i(Camera3D.TAG, "mCurrentFeed: "+mCurrentFeed +
			// " - mCurrentFrame: "+mCurrentFrame);
			mHandler.removeCallbacks(mPlay);
			FeedSet fs = getItem(mCurrentFeed);
			mCurrentFrame++;
			final int currenFrame = getCurrentFrame(mCurrentFeed);
			// Log.i(Camera3D.TAG, "getUrlcurrenFrame: "+
			// fs.getUrl(currenFrame));
			if (!TextUtils.isEmpty(fs.getFilePath(currenFrame))) {
				if (mImageView.get(mCurrentFeed) != null) {
					String url = fs.getFilePath(currenFrame);	    
					if (!TextUtils.isEmpty(url))
						Picasso.with(mContext)
								.load(mContext.getResources().getIdentifier(
										url, "drawable",
										mContext.getPackageName()))
								.into(mImageView.get(mCurrentFeed));
					//
//					PicassoUtils.with(mContext).load(fs.getUrl(currenFrame))
//							.into(mImageView.get(mCurrentFeed));
				}
				
				final int duration = fs.getFrame(mCurrentFeed).getDuration();
				if (duration > 0)
					mHandler.postDelayed(mPlay, duration);
				else
					mHandler.postDelayed(mPlay, DEFAULT_DURATION);
			}
		}
	};

	int getCurrentFrame(int currentFeed) {
		FeedSet fs = mFeedSet.get(currentFeed);
		int frame = (fs.frame() == 0) ? fs.size() : fs.frame();
		if (mCurrentFrame >= frame) {
			mCurrentFrame = 0;
			return 0;
		}

		if (mCurrentFrame < 0) {
			mCurrentFrame = 0;
			return 0;
		}
		return mCurrentFrame;
	}

	final private Runnable runSlideShow = new Runnable() {

		@Override
		public void run() {

		}
	};
}
