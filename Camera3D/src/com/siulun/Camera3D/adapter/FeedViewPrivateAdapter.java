package com.siulun.Camera3D.adapter;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apptechhk.Utils.PicassoUtils;
import com.siulun.Camera3D.Camera3D;
import com.siulun.Camera3D.Comment;
import com.siulun.Camera3D.FeedViewPrivateFragment;
import com.siulun.Camera3D.FeedViewPublicFragment;
import com.siulun.Camera3D.ImageInfoEdit;
import com.siulun.Camera3D.Log;
import com.siulun.Camera3D.MainActivity;
import com.siulun.Camera3D.R;
import com.siulun.Camera3D.Utils;
import com.siulun.Camera3D.adapter.FeedViewPublicAdapter.ViewHolder;
import com.siulun.Camera3D.model.FeedSet;
import com.squareup.picasso.RequestBuilder;

public class FeedViewPrivateAdapter extends BaseAdapter {
	
	private final static int DEFAULT_DURATION = 100;
	
	Context mContext;
	LayoutInflater mInflater;
	FeedViewPrivateFragment mFragment;
	ArrayList<FeedSet> mFeedSet = new ArrayList<FeedSet>();
	SparseArray<ImageView> mImageView = new SparseArray<ImageView>();
	int mCurrentFeed = -1, mCurrentFrame = 0;
	private final static int TOUCH_DISTANCE = 5;
	public boolean isTouching = false;
	private final static int TYPE_PRIVATE = 0;
	private final static int TYPE_PUBLIC = 1;
	PrivateListviewInterface listInterface;
	
	public interface PrivateListviewInterface{
		public void updateListview(FeedSet item);
	}

	public FeedViewPrivateAdapter(Context context, ArrayList<FeedSet> socialSet,PrivateListviewInterface listInterface, FeedViewPrivateFragment fragment) {
		mContext = context;
		mInflater = LayoutInflater.from(context);
		mFragment = fragment;
		mFeedSet = socialSet;
		this.listInterface = listInterface;
//		URL.setURLStreamHandlerFactory(new OkHttpClient());
	}

	public void setCurrentFeed(int pos) {
		if (pos != mCurrentFeed) {
			mCurrentFeed = pos;
			mCurrentFrame = 0;
			mHandler.removeCallbacks(mPlay);
			mHandler.postDelayed(mPlay, 50);
		}
	}

	public void viewNextFrame(){
		mCurrentFrame++;
		setCurrentFrame(mCurrentFrame);
	}
	
	public void viewLastFrame(){
		mCurrentFrame--;
		setCurrentFrame(mCurrentFrame);
	}

	public void setCurrentFrame(int frame) {
		mHandler.removeCallbacks(mPlay);
		Log.i("--- frame: " + frame);
		mCurrentFrame = frame;
		mCurrentFrame = getCurrentFrame(mCurrentFeed);

		try {
		FeedSet fs = getItem(mCurrentFeed);
		if (mImageView.get(mCurrentFeed) != null)
			PicassoUtils.with(mContext).load(fs.getUrl(mCurrentFrame))
					.into(mImageView.get(mCurrentFeed));
		} catch (NullPointerException e) {
		}
	}

	@Override
	public int getCount() {
		return mFeedSet.size();
	}

	@Override
	public FeedSet getItem(int pos) {
//		Log.i("pos : " + pos);
		return mFeedSet.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return 0;
	}
	
	@Override
	public int getViewTypeCount() {
	    return 2;
	}
	
	 @Override
     public int getItemViewType(int position) {
         return getItem(position).getStatus()<10 ? TYPE_PRIVATE : TYPE_PUBLIC;
     }

	float lastX, lastY, distanceX, distanceY;
	
	@Override
	public View getView(final int pos, View view, final ViewGroup parent) {
		if (mFeedSet.size()==0)
			return view;
		if (view != null) {
			holder = (ViewHolder) view.getTag();
		} else if(getItemViewType(pos) == TYPE_PRIVATE){
			view = mInflater.inflate(R.layout.feed_item, parent, false);
			holder = new ViewHolder();
			holder.imgID = (TextView) view.findViewById(R.id.imgID);
			holder.imageView = (ImageView) view.findViewById(R.id.imageView);
			holder.imageHolder = (FrameLayout) view.findViewById(R.id.imageHolder);
			holder.title = (TextView) view.findViewById(R.id.title);
			holder.desc = (TextView) view.findViewById(R.id.desc);
			holder.publishButton = (Button) view.findViewById(R.id.edit);
			holder.editBtn = (ImageView) view.findViewById(R.id.editInfoBtn);
			holder.shareBtn = (ImageView) view.findViewById(R.id.shareBtn);
			
			holder.publishButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {

					new AsyncTask<Void, Void, String>() {
		        			@Override
		        			protected String doInBackground(Void... params) {
		        				try {
		        					return sendToServer(pos);
		        				} catch (IOException e) {
		        					e.printStackTrace();
		        				}
		        				return null;
		        			}

		        			protected void onPostExecute(String result) {
		        				JSONObject json;
		        				try {
		        					if (!TextUtils.isEmpty(result)) {
		        						json = (JSONObject) new JSONTokener(result).nextValue();
		        						if (json.get("status").toString().equals("done")) {
		        							Toast.makeText(mContext, "Published!", Toast.LENGTH_LONG).show();
		        					//		holder.publishButton.setOnClickListener(null);
		        							holder.publishButton.setVisibility(View.GONE);
		        					//		holder.editBtn.setOnClickListener(null);
		        							holder.editBtn.setVisibility(View.GONE);
		        						} else 
		        							Toast.makeText(mContext, "Sorry, fail to publish.", Toast.LENGTH_LONG).show();
		        					} else 
		        						Toast.makeText(mContext, "Sorry, fail to publish.", Toast.LENGTH_LONG).show();
		        				} catch (JSONException e) {
		        					e.printStackTrace();
		        				}
		        			};
		        		}.execute();
		        	} //onclick
			});
			holder.publishButton.setVisibility(View.VISIBLE);
			
			holder.editBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(mContext, ImageInfoEdit.class);
		            intent.putExtra("image_id", getItem(pos).getImageId());
		            intent.putExtra("title", getItem(pos).getTitle());
		            intent.putExtra("desc", getItem(pos).getDesc());
		            intent.putExtra("size", getItem(pos).getSize());
		            intent.putExtra("pos", pos);
		            mContext.startActivity(intent);  
				}
			});
			
			final View viewtemp = view;
			holder.shareBtn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					String[] s = {"Email", "Facebook"};
					new AlertDialog.Builder(mContext)
					.setTitle("Share")
					.setItems(s, new DialogInterface.OnClickListener() {
				               public void onClick(DialogInterface dialog, int pos) {
				            	   if (pos==0){
				            		   Intent email = new Intent(Intent.ACTION_SEND);
										email.putExtra(Intent.EXTRA_SUBJECT, "Camera3D Photo URL -- "+((ViewHolder)viewtemp.getTag()).title.getText());//holder.title.getText().toString());
										email.putExtra(Intent.EXTRA_TEXT, "Dear Camera3D user, \n\nThis is the URL to your photo:"
												+"http://camera3d.apptech.com.hk/public_view.php?photo="+((ViewHolder)viewtemp.getTag()).imgID.getText()//getItem(pos).getFilename()
												+"\n Feel free to share it on web! Thanks for using Camera3D :) \n\nBest Regards,\nApptech HK");
										email.setType("message/rfc822");
										mContext.startActivity(Intent.createChooser(email, "Choose an Email client :"));
				            	   }
				            	   else {
				            		   mFragment.onShareBtnClick("http://camera3d.apptech.com.hk/public_view.php?photo="+((ViewHolder)viewtemp.getTag()).imgID.getText()
				       						,""+((ViewHolder)viewtemp.getTag()).title.getText());
				            	   }
				           }
				    }).show();
				}
			});
			
			view.setTag(holder);
		} else {
			view = mInflater.inflate(R.layout.feed_item_public, parent, false);
			holder = new ViewHolder();
			holder.imgID = (TextView) view.findViewById(R.id.imgID);
			holder.imageView = (ImageView) view.findViewById(R.id.imageView);
			holder.imageHolder = (FrameLayout) view.findViewById(R.id.imageHolder);
			holder.title = (TextView) view.findViewById(R.id.title);
			holder.desc = (TextView) view.findViewById(R.id.desc);
			holder.likeBtn = (TextView) view.findViewById(R.id.likeBtn);
			holder.commentBtn = (TextView) view.findViewById(R.id.commentBtn);
			holder.likeCount = (TextView) view.findViewById(R.id.likeCount);
			holder.commentCount = (TextView) view.findViewById(R.id.commentCount);
			holder.deleteBtn = (ImageView) view.findViewById(R.id.deleteBtn);
			holder.shareBtn = (ImageView) view.findViewById(R.id.shareBtn);
			view.setTag(holder);
			
			final View viewtemp = view;
			if (!getItem(pos).getUserLike()) {
				holder.likeBtn.setText("Like");
				holder.likeBtn.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if (TextUtils.isEmpty(Utils.getString(mContext, "userId"))) {
							Toast.makeText(mContext, "Please login to \"like\" :)", Toast.LENGTH_LONG).show();
						} else {
						new AsyncTask<String, Void, String>() {
							@Override
							protected String doInBackground(String... params) {
								String image_id = params[0];
								HttpClient httpclient = new DefaultHttpClient();
								HttpPost httppost = new HttpPost(Camera3D.SERVER_SET_LIKE);

								ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
										2);
								nameValuePairs.add(new BasicNameValuePair("userId", Utils.getString(mContext, "userId")));
								nameValuePairs.add(new BasicNameValuePair("image_id", image_id));
								String str = "";
								try {
									httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
									HttpResponse response = httpclient.execute(httppost);
									HttpEntity entity = response.getEntity();

									str = Utils.convertStreamToString(entity.getContent());
								} catch (Exception e) {
									e.printStackTrace();
								}
								return str;
							}

							protected void onPostExecute(String result) {
//								Log.i(Camera3D.TAG, result);
								JSONObject json;
								try {
									json = (JSONObject) new JSONTokener(result).nextValue();
									if (json.get("status").toString().equals("done")) {
										Toast.makeText(mContext, mContext.getResources().getString(R.string.str_liked)+"!", Toast.LENGTH_LONG).show();
										int likes = getItem(pos).getLike()+1;
										((ViewHolder)viewtemp.getTag()).likeCount.setText(likes+" "+mContext.getResources().getString(R.string.str_like));
										((ViewHolder)viewtemp.getTag()).likeBtn.setText(mContext.getResources().getString(R.string.str_liked));
									} else {
										Toast.makeText(mContext, (String) json.get("msg"), Toast.LENGTH_LONG).show();
									}
									
									
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							};

						}.execute(getItem(pos).getImageId());
					} //else
					}//onclick
				});
				} else {
					holder.likeBtn.setText(mContext.getResources().getString(R.string.str_liked));
					holder.likeBtn.setOnClickListener(null);
				}

				holder.commentBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent i = new Intent(mContext, Comment.class);
						i.putExtra("image_id", getItem(pos).getImageId());
						mContext.startActivity(i);
					}
					
				});
				
				holder.commentCount.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent i = new Intent(mContext, Comment.class);
						i.putExtra("image_id", getItem(pos).getImageId());
						mContext.startActivity(i);
					}
					
				});
				holder.likeCount.setText(getItem(pos).getLike()+" "+mContext.getResources().getString(R.string.str_like));
				holder.commentCount.setText(getItem(pos).getComment()+" "+mContext.getResources().getString(R.string.str_comment));
				holder.shareBtn.setVisibility(View.VISIBLE);
				
				holder.shareBtn.setOnClickListener(new OnClickListener(){
					@Override
					public void onClick(View v) {
						String[] s = {"Email", "Facebook"};
						new AlertDialog.Builder(mContext)
						.setTitle("Share")
						.setItems(s, new DialogInterface.OnClickListener() {
					               public void onClick(DialogInterface dialog, int pos) {
					            	   if (pos==0){
					            		   Intent email = new Intent(Intent.ACTION_SEND);
											email.putExtra(Intent.EXTRA_SUBJECT, "Camera3D Photo URL -- "+((ViewHolder)viewtemp.getTag()).title.getText());//holder.title.getText().toString());
											email.putExtra(Intent.EXTRA_TEXT, "Dear Camera3D user, \n\nThis is the URL to your photo:"
													+"http://camera3d.apptech.com.hk/public_view.php?photo="+((ViewHolder)viewtemp.getTag()).imgID.getText()//getItem(pos).getFilename()
													+"\n Feel free to share it on web! Thanks for using Camera3D :) \n\nBest Regards,\nApptech HK");
											email.setType("message/rfc822");
											mContext.startActivity(Intent.createChooser(email, "Choose an Email client :"));
					            	   }
					            	   else {
					            		   mFragment.onShareBtnClick("http://camera3d.apptech.com.hk/public_view.php?photo="+((ViewHolder)viewtemp.getTag()).imgID.getText()
					       						,""+((ViewHolder)viewtemp.getTag()).title.getText());
					            	   }
					           }
					    }).show();
					}
				});
				
				holder.deleteBtn.setVisibility(View.VISIBLE);
				holder.deleteBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {


						new AlertDialog.Builder(mContext)
				        .setIcon(android.R.drawable.ic_dialog_alert)
				        .setTitle("Delete Photo \""+getItem(pos).getTitle()+"\"")
				        .setMessage("Are you sure you want to delete the photo? Photo will not be viewable by public after delete.")
				        .setPositiveButton("OK", new DialogInterface.OnClickListener()
					    {
					        @Override
					        public void onClick(DialogInterface dialog, int which) {
					        	new AsyncTask<Void, Void, String>() {
				        			@Override
				        			protected String doInBackground(Void... params) {
				        				try {
				        					return deletePhoto(pos);
				        				} catch (IOException e) {
				        					e.printStackTrace();
				        				}
				        				return null;
				        			}

				        			protected void onPostExecute(String result) {
//				        				Log.i(Camera3D.TAG, result);
				        				JSONObject json;
				        				try {
				        					if (!TextUtils.isEmpty(result)) {
				        						json = (JSONObject) new JSONTokener(result).nextValue();
				        						if (json.get("status").toString().equals("done")) {
				        							listInterface.updateListview(getItem(pos));
				        							Toast.makeText(mContext, "Deleted!", Toast.LENGTH_LONG).show();
				        							return;
				        						} else {
				        							Toast.makeText(mContext, "Sorry, fail to delete.", Toast.LENGTH_LONG).show();
				        							return;
				        						}
				        					} else {
				        						Toast.makeText(mContext, "Sorry, fail to delete.", Toast.LENGTH_LONG).show();
				        						return;
				        					}
				        				} catch (JSONException e) {
				        					e.printStackTrace();
				        				}
				        			};
				        		}.execute();	
								
					        }
					    })
					    .setNegativeButton("Cancel", null)
					    .show();
						
					}
				});
				view.setTag(holder);
		}


		holder.title.setText(getItem(pos).getTitle());
		holder.imgID.setText(getItem(pos).getFilename());
		if(!TextUtils.isEmpty(getItem(pos).getDesc())) {
			holder.desc.setText(getItem(pos).getDesc());
			holder.desc.setVisibility(View.VISIBLE);
		} else {
			holder.desc.setVisibility(View.GONE);
		}
		
		holder.imageView.setTag(getItem(pos).getFilename());
		mImageView.put(pos, holder.imageView);

/*
		holder.imageView.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				distanceX = event.getX() - lastX;
				distanceY = event.getY() - lastY;
				lastX = event.getX();
				lastY = event.getY();

				
				int action = event.getAction();

				switch (action) {
				case MotionEvent.ACTION_DOWN:
					isTouching = true;
					MainActivity.setSwipeable(false);
					break;

				case MotionEvent.ACTION_MOVE:
					if (distanceX < -TOUCH_DISTANCE) {
						Log.i("viewNextFrame");
						viewNextFrame();
					} else if (distanceX > TOUCH_DISTANCE) {
						Log.i("viewLastFrame");
						viewLastFrame();
					} 
					break;

				case MotionEvent.ACTION_UP:
					isTouching = false;
					MainActivity.setSwipeable(true);
					break;
					
				default:
					MainActivity.setSwipeable(true);
					break;	
				}
				return true;
			}
		});
*/
		final String url = getItem(pos).getUrl(0);
		holder.imageView.setImageBitmap(null);
		if (!TextUtils.isEmpty(url))
			PicassoUtils.with(mContext).load(url).into(holder.imageView);

	//	view.setTag(holder);
		
		return view;
	}

	ViewHolder holder;
	static class ViewHolder {
		TextView title, desc, imgID;
		ImageView imageView;
		FrameLayout imageHolder;
		Button publishButton;
		TextView likeCount, commentCount;
		TextView likeBtn, commentBtn;
		ImageView deleteBtn, editBtn, shareBtn;
	}
	
	public boolean getTouchingStatus() {
		return isTouching;
	}	                                   

	Handler mHandler = new Handler();
	Runnable mPlay = new Runnable() {
		@Override
		public void run() {
			// Log.i(Camera3D.TAG, "mCurrentFeed: "+mCurrentFeed +
			// " - mCurrentFrame: "+mCurrentFrame);
			mHandler.removeCallbacks(mPlay);
			FeedSet fs = getItem(mCurrentFeed);
			mCurrentFrame++;
			final int currenFrame = getCurrentFrame(mCurrentFeed);
			// Log.i(Camera3D.TAG, "getUrlcurrenFrame: "+
			// fs.getUrl(currenFrame));
			if (!TextUtils.isEmpty(fs.getUrl(currenFrame))) {
				if (mImageView.get(mCurrentFeed) != null) {
					String url = fs.getUrl(currenFrame);
					String filename = fs.getFilename();
					RequestBuilder rb = PicassoUtils.with(mContext).load(url);
					if (mImageView.get(mCurrentFeed).getTag().toString() == filename)
							rb.into(mImageView.get(mCurrentFeed));
				}
				final int duration = fs.getFrame(mCurrentFeed).getDuration();
				if (duration > 0)
					mHandler.postDelayed(mPlay, duration);
				else
					mHandler.postDelayed(mPlay, DEFAULT_DURATION);
			}
		}
	};

	int getCurrentFrame(int currentFeed) {
		FeedSet fs = mFeedSet.get(currentFeed);
		int frame = (fs.frame() == 0) ? fs.size() : fs.frame();
		if (mCurrentFrame >= frame) {
			mCurrentFrame = 0;
			return 0;
		}

		if (mCurrentFrame < 0) {
			mCurrentFrame = 0;
			return 0;
		}
		return mCurrentFrame;
	}

	private String sendToServer(int pos) throws IOException {
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(Camera3D.SERVER_SET_IMAGE_PUBLIC);

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
				2);
		nameValuePairs.add(new BasicNameValuePair("userId", Utils.getString(mContext, "userId")));
		nameValuePairs.add(new BasicNameValuePair("title", "" + getItem(pos).getTitle()));
		nameValuePairs.add(new BasicNameValuePair("desc", getItem(pos).getDesc()));
		nameValuePairs.add(new BasicNameValuePair("image_id", getItem(pos).getImageId()));
		nameValuePairs.add(new BasicNameValuePair("size", ""+getItem(pos).getSize()));
		
		try {
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();

			String str = Utils.convertStreamToString(entity.getContent());

			return str;
		} catch (Exception e) {
			android.util.Log.e("log_tag", "Error in http connection " + e.toString());
		}
		return null;
	}
	
	private String deletePhoto(int pos) throws IOException {
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(Camera3D.SERVER_DELETE_PHOTO);

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
				2);
		nameValuePairs.add(new BasicNameValuePair("userId", Utils.getString(mContext, "userId")));
		nameValuePairs.add(new BasicNameValuePair("image_id", getItem(pos).getImageId()));
		
		try {
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();

			String str = Utils.convertStreamToString(entity.getContent());

			return str;
		} catch (Exception e) {
			android.util.Log.e("log_tag", "Error in http connection " + e.toString());
		}
		return null;
	}
	
	final private Runnable runSlideShow = new Runnable() {

		@Override
		public void run() {

		}
	};
	
}
