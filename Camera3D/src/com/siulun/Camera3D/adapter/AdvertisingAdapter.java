package com.siulun.Camera3D.adapter;

import android.app.Activity;
import android.database.DataSetObserver;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdSize;
import com.google.ads.AdView;

 
/**
 * List adapter decorator that inserts adverts into the list.
 */
public class AdvertisingAdapter extends BaseAdapter implements AdListener
{
    private static final String ADMOB_PUBLISHER_ID = "a14cf4d23e7ea88";
 
    private final FragmentActivity activity;
    private final FeedViewPublicAdapter delegate;
    private final int k = 3;
 
    public AdvertisingAdapter(FragmentActivity activity, FeedViewPublicAdapter delegate)
    {
        this.activity = activity;
        this.delegate = delegate;
        delegate.registerDataSetObserver(new DataSetObserver()
        {
            @Override
            public void onChanged()
            {
                notifyDataSetChanged();
            }
 
            @Override
            public void onInvalidated()
            {
                notifyDataSetInvalidated();
            }
        });
    }
 
    @Override
    public int getCount()
    {
    	int count = delegate.getCount() + delegate.getCount()/(k-1);
        return delegate.getCount() + delegate.getCount()/(k-1);
    }
 
    @Override
    public Object getItem(int i)
    {
    	if ((i%k) == 0) {
            return null;
        }
    	int count = i - (int) Math.ceil(i / k) - 1;
        return delegate.getItem(i - (int) Math.ceil(i / k) - 1);
    }
 
    public long getItemId(int i)
    {
        return i;
    }
 
    public View getView(int position, View convertView, ViewGroup parent)
    {
    	if ((position % k) == 0) {
    	      if (convertView instanceof AdView) {
    	        // Don��t instantiate new AdView, reuse old one
    	        return convertView;
    	      } else {
    	        // Create a new AdView
    	        AdView adView = new AdView(activity, AdSize.BANNER,
    	        		ADMOB_PUBLISHER_ID);

    	        // Convert the default layout parameters so that they play nice with
    	        // ListView.
    	        float density = activity.getResources().getDisplayMetrics().density;
    	        int height = Math.round(AdSize.BANNER.getHeight() * density);
    	        AbsListView.LayoutParams params = new AbsListView.LayoutParams(
    	            AbsListView.LayoutParams.FILL_PARENT,
    	            height);
    	        adView.setLayoutParams(params);

    	        adView.loadAd(new AdRequest());
    	        return adView;
    	      }
    	    } else {
    	      // Offload displaying other items to the delegate
    	      return delegate.getView(position - (int) Math.ceil(position / k) - 1,
    	          convertView, parent);
    	    }
    }
 
    @Override
    public int getViewTypeCount()
    {
        return 2;
    }
 
    @Override
    public int getItemViewType(int position)
    {
        return (position%k) == 0 ? 0  : 1;
    }
 
    @Override
    public boolean areAllItemsEnabled()
    {
        return false;
    }
 
    @Override
    public boolean isEnabled(int position)
    {
        return  (position%k) != 0 ;
    }

	@Override
	public void onDismissScreen(Ad arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFailedToReceiveAd(Ad arg0, ErrorCode arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLeaveApplication(Ad arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPresentScreen(Ad arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceiveAd(Ad arg0) {
		// TODO Auto-generated method stub
		
	}
}