package com.siulun.Camera3D.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.siulun.Camera3D.R;
import com.siulun.Camera3D.model.CommentItem;

public class CommentViewAdapter extends BaseAdapter {
	
	Context mContext;
	LayoutInflater mInflater;
	ArrayList<CommentItem> mCommentItem = new ArrayList<CommentItem>();

	public CommentViewAdapter(Context context, ArrayList<CommentItem> commentItem) {
		mContext = context;
		mInflater = LayoutInflater.from(context);
		mCommentItem = commentItem;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mCommentItem.size();
	}

	@Override
	public CommentItem getItem(int pos) {
		// TODO Auto-generated method stub
//		Log.i("pos : " + pos);
		return mCommentItem.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int pos, View view, final ViewGroup parent) {
		if (view != null) {
			holder = (ViewHolder) view.getTag();
		} else {
			view = mInflater.inflate(R.layout.comment_item, parent, false);
			holder = new ViewHolder();
			holder.display_name = (TextView) view.findViewById(R.id.display_name);
			holder.comment = (TextView) view.findViewById(R.id.comment);
			holder.date_time = (TextView) view.findViewById(R.id.date_time);
			view.setTag(holder);
		}

		if(!TextUtils.isEmpty(getItem(pos).getDisplayName()))
			holder.display_name.setText(getItem(pos).getDisplayName());
		
		if(!TextUtils.isEmpty(getItem(pos).getComment()))
			holder.comment.setText(getItem(pos).getComment());
		
		if(!TextUtils.isEmpty(getItem(pos).getCreateDate()))
			holder.date_time.setText(getItem(pos).getCreateDate());

		return view;
	}

	ViewHolder holder;
	static class ViewHolder {
		TextView display_name;
		TextView comment;
		TextView date_time;
	}
}
