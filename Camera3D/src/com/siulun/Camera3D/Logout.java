package com.siulun.Camera3D;

import java.io.IOException;
import java.io.InputStream;

import com.google.ads.AdRequest;
import com.google.ads.AdView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.OnClick;
import butterknife.Views;

public class Logout extends Activity {
	
	public static final int DIALOG_MENU_HELP = 0;
	public static final int DIALOG_MENU_ABOUT = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.logout);
//	    Views.inject(this);
		AdView adView = (AdView) this.findViewById(R.id.adView);
		adView.loadAd(new AdRequest());
		Button mLogoutButton = (Button) findViewById(R.id.btn_logout);
		mLogoutButton.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			logout();
		}
		});	
		
		Button mTipsButton = (Button) findViewById(R.id.tips);
		mTipsButton.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			showDialog(DIALOG_MENU_HELP);
		}
		});	
		Button mAboutButton = (Button) findViewById(R.id.about);
		mAboutButton.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			showDialog(DIALOG_MENU_ABOUT);
		}
		});	
	}


	public void logout() {
		if (!TextUtils.isEmpty(Utils.getString(Logout.this, "userId"))){
			Utils.setString(Logout.this, "userId", "");
			Toast.makeText(getApplicationContext(), "Logout Success!", Toast.LENGTH_LONG).show();
			setResult(RESULT_OK);
			finish();
		}		
	}
	
	@Override
	public Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_MENU_ABOUT:
			final TextView message = new TextView(this);
			SpannableString s = new SpannableString(
					this.getText(R.string.dialog_about_1));
			Linkify.addLinks(s, Linkify.EMAIL_ADDRESSES);
			message.setText(s);
			message.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
			s = new SpannableString(this.getText(R.string.dialog_about_2));
			Linkify.addLinks(s, Linkify.WEB_URLS);
			message.append(s);
			s = new SpannableString(this.getText(R.string.dialog_about_3));
			message.append(s);
			message.setMovementMethod(LinkMovementMethod.getInstance());
			message.setPadding(20, 20, 20, 20);
			return new AlertDialog.Builder(this)
					.setTitle(R.string.dialog_about)
					// .setMessage(R.string.dialog_about_detail)
					.setView(message)
					.setPositiveButton(R.string.OK,
							new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface dialoginterface, int i) {
									dialoginterface.cancel();
								}
							}).show();
		case DIALOG_MENU_HELP:
			try {
				InputStream is = getAssets().open("help.txt");
				int size = is.available();
				byte[] buffer = new byte[size];
				is.read(buffer);
				is.close();
				// Convert the buffer into a string.
				String text = new String(buffer);
				return new AlertDialog.Builder(this)
						.setTitle(R.string.dialog_instructions)
						.setMessage(text)
						.setPositiveButton(R.string.OK,
								new DialogInterface.OnClickListener() {
									public void onClick(
											DialogInterface dialoginterface,
											int i) {
										dialoginterface.cancel();
									}
								}).show();
			} catch (IOException e) {
				// Should never happen!
				throw new RuntimeException(e);
			}
		}
		return null;
	}

}
