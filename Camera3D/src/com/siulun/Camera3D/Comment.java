package com.siulun.Camera3D; 

import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.OnClick;
import butterknife.Views;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.internal.widget.IcsAdapterView;
import com.actionbarsherlock.internal.widget.IcsAdapterView.OnItemSelectedListener;
import com.apptechhk.Utils.PicassoUtils;
import com.siulun.Camera3D.adapter.CommentViewAdapter;
import com.siulun.Camera3D.model.CommentItem;

public class Comment extends SherlockActivity implements OnItemSelectedListener,OnItemClickListener{
//	ScrollView scrollView;
	ListView listview;
	RelativeLayout mSpinner;
	CommentViewAdapter mAdapter;
	EditText mCommentText;
	TextView mTitle,mDesc;
	ArrayList<CommentItem> mCommentItem = new ArrayList<CommentItem>();
	String mImageId,filename,title,desc;
	Button mCommentButton;
	ImageView imageView;
	FrameLayout imageHolder;
	int mCurrentFeed = -1, mCurrentFrame = 0, frame, size;
	private final static int TOUCH_DISTANCE = 5;
	public boolean isTouching = false;
	float lastX, lastY, distanceX, distanceY;
	private final static int DEFAULT_DURATION = 100;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.comment);
	//    Views.inject(this);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		Intent intent = getIntent();
		filename = intent.getStringExtra("filename");
		mImageId = intent.getStringExtra("image_id");
		frame = intent.getIntExtra("frame", 0);
		size = intent.getIntExtra("size", 0);
		title = intent.getStringExtra("title");
		desc = intent.getStringExtra("desc");
		View editTextView = getLayoutInflater().inflate(R.layout.comment_edittext_view,listview, false);
		View submitBtnView = getLayoutInflater().inflate(R.layout.comment_submitbtn_view, listview, false);
		
	//	scrollView = (ScrollView) findViewById(R.id.scrollView);
		mTitle = (TextView) findViewById(R.id.title);
		mDesc = (TextView) editTextView.findViewById(R.id.desc);
		listview = (ListView) findViewById(R.id.listView);
		
    	listview.addHeaderView(editTextView);
    	
   // 	submitBtnView.findViewById(R.id.submit);
    	listview.addHeaderView(submitBtnView);
		
		mSpinner = (RelativeLayout) findViewById(R.id.spinner);
		imageView = (ImageView) editTextView.findViewById(R.id.imageView);
		imageHolder = (FrameLayout) editTextView.findViewById(R.id.imageHolder);
		mCommentText = (EditText) editTextView.findViewById(R.id.commentText);
		mCommentButton = (Button) submitBtnView.findViewById(R.id.cm_submit);
/*	
		scrollView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
            	scrollView.requestDisallowInterceptTouchEvent(false);
                return false;
            }
        });
		listview.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of
                // child view
                scrollView.requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
		
	*/	
		
		mTitle.setText(title);
		if(!TextUtils.isEmpty(desc)) {
			mDesc.setText(desc);
			mDesc.setVisibility(View.VISIBLE);
		} else {
			mDesc.setVisibility(View.GONE);
		}
		mCommentButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if ( TextUtils.isEmpty(mCommentText.getText().toString())) {
					Toast.makeText(getApplicationContext(), "Comment must not empty...", Toast.LENGTH_LONG).show();
					return;
				}

		        mSpinner.setVisibility(View.VISIBLE);
		        
				new AsyncTask<String, Void, String>() {
					@Override
					protected String doInBackground(String... params) {
						String image_id = params[0];
						HttpClient httpclient = new DefaultHttpClient();
						HttpPost httppost = new HttpPost(Camera3D.SERVER_SET_COMMENT);

						ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
								2);
						nameValuePairs.add(new BasicNameValuePair("userId", Utils
								.getString(Comment.this, "userId")));
						nameValuePairs
						.add(new BasicNameValuePair("image_id", image_id));
						nameValuePairs
						.add(new BasicNameValuePair("comment", mCommentText.getText().toString()));

						String str = "";
						try {
							httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
									"UTF-8"));
							HttpResponse response = httpclient.execute(httppost);
							HttpEntity entity = response.getEntity();

							str = Utils.convertStreamToString(entity.getContent());
						} catch (Exception e) {
							e.printStackTrace();
						}
						return str;
					}

					protected void onPostExecute(String result) {
//						Log.i(Camera3D.TAG, result);
						JSONObject json;
						try {
							if (!TextUtils.isEmpty(result)) {
								json = (JSONObject) new JSONTokener(result).nextValue();
								if (json.get("status").toString().equals("done")) {
									Toast.makeText(getApplicationContext(), "OOooooH!!!", Toast.LENGTH_LONG).show();
									finish();
								} else {
									Toast.makeText(getApplicationContext(), (String) json.get("msg"), Toast.LENGTH_LONG).show();
								}
							} else {
								Toast.makeText(getApplicationContext(), "Sorry, No Response...", Toast.LENGTH_LONG).show();
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					};

				}.execute(mImageId);
				
				
				
				
			}
		}); //comment button onclick listener
		
		
		imageView.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				distanceX = event.getX() - lastX;
				distanceY = event.getY() - lastY;
				lastX = event.getX();
				lastY = event.getY();

				int action = event.getAction();

				switch (action) {
				case MotionEvent.ACTION_DOWN:
					isTouching = true;
					break;

				case MotionEvent.ACTION_MOVE:
					if (distanceX < -TOUCH_DISTANCE) {
//						listview.setScrollContainer(false);
						Log.i("viewNextFrame");
						viewNextFrame();
					} else if (distanceX > TOUCH_DISTANCE) {
//						listview.setScrollContainer(false);
						Log.i("viewLastFrame");
						viewLastFrame();
					} 
//					else
//						listview.setScrollContainer(true);
					break;

				case MotionEvent.ACTION_UP:
//					mHandler.postDelayed(new Runnable() {
//						
//						@Override
//						public void run() {
//							listview.setScrollContainer(true);
//						}
//					}, 100);
					isTouching = false;
					break;
				
				}
				return true;
			}
		});

		final String url = intent.getStringExtra("url");
		if (!TextUtils.isEmpty(url))
			PicassoUtils.with(this).load(url).into(imageView);
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		new AsyncTask<String, Void, String>() {
			@Override
			protected String doInBackground(String... params) {
				String image_id = params[0];
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(Camera3D.SERVER_GET_COMMENT);

				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						2);
				nameValuePairs.add(new BasicNameValuePair("userId", Utils
						.getString(Comment.this, "userId")));
				nameValuePairs
				.add(new BasicNameValuePair("image_id", image_id));

				String str = "";
				try {
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
							"UTF-8"));
					HttpResponse response = httpclient.execute(httppost);
					HttpEntity entity = response.getEntity();

					str = Utils.convertStreamToString(entity.getContent());
				} catch (Exception e) {
					e.printStackTrace();
				}
				return str;
			}

			protected void onPostExecute(String result) {
				ArrayList<CommentItem> commentItem = new ArrayList<CommentItem>();
				try {
					if (!TextUtils.isEmpty(result)) {
						JSONArray array = (JSONArray) new JSONArray(result);
						int count = array.length();
						for (int i = 0; i < count; i++) {
							JSONObject row = array.getJSONObject(i);
							commentItem.add(new CommentItem(row
									.getString("display_name"), "", row
									.getString("comment"), row
									.getString("create_date")));
						}
						mCommentItem = commentItem;
					}
					mSpinner.setVisibility(View.GONE);
					init();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			};

		}.execute(mImageId);
	}

	private void init() {
		mAdapter = new CommentViewAdapter(this, mCommentItem);
		listview.setAdapter(mAdapter);

		mSpinner.setVisibility(View.GONE);
		listview.setVisibility(View.VISIBLE);
	}
	
	Handler mHandler = new Handler();
	Runnable mPlay = new Runnable() {
		@Override
		public void run() {
			// Log.i(Camera3D.TAG, "mCurrentFeed: "+mCurrentFeed +
			// " - mCurrentFrame: "+mCurrentFrame);
			mHandler.removeCallbacks(mPlay);
			mCurrentFrame++;
			final int currenFrame = getCurrentFrame();
			// Log.i(Camera3D.TAG, "getUrlcurrenFrame: "+
			// fs.getUrl(currenFrame));
			if (!TextUtils.isEmpty(getUrl(currenFrame))) {
					PicassoUtils.with(Comment.this).load(getUrl(currenFrame))
							.into(imageView);
					mHandler.postDelayed(mPlay, DEFAULT_DURATION);
			}
		}
	};
	
	public void viewNextFrame(){
		mCurrentFrame++;
		setCurrentFrame(mCurrentFrame);
	}
	
	public void viewLastFrame(){
		mCurrentFrame--;
		setCurrentFrame(mCurrentFrame);
	}

	public void setCurrentFrame(int frame) {
		mHandler.removeCallbacks(mPlay);
		Log.i("--- frame: " + frame);
		mCurrentFrame = frame;
		mCurrentFrame = getCurrentFrame();
		Log.i("--- setCurrentFrame: " + mCurrentFrame);

		try {
				PicassoUtils.with(this).load(getUrl(mCurrentFrame))
						.into(imageView);
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}
	
	int getCurrentFrame() {
		int mFrame = (frame == 0) ? size : frame;
		if (mCurrentFrame >= mFrame) {
			mCurrentFrame = 0;
			return 0;
		}

		if (mCurrentFrame < 0) {
			mCurrentFrame = 0;
			return 0;
		}
		return mCurrentFrame;
	}
	
	public String getUrl(int pos) {
		return Camera3D.S3_BASE_URL + "/" + filename + "_" + Utils.addZero(pos, 2) + ".jpg";
	}

	@Override
	public void onItemSelected(IcsAdapterView<?> parent, View view,
			int position, long id) {
		    if (position == 1)
		    {
		        // listView.setItemsCanFocus(true);

		        // Use afterDescendants, because I don't want the ListView to steal focus
		        listview.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
		        mCommentText.requestFocus();
		    }
		    else
		    {
		        if (!listview.isFocused())
		        {
		            // listView.setItemsCanFocus(false);

		            // Use beforeDescendants so that the EditText doesn't re-take focus
		            listview.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
		            listview.requestFocus();
		        }
		    }
	}

	@Override
	public void onNothingSelected(IcsAdapterView<?> parent) {
		// This happens when you start scrolling, so we need to prevent it from staying
	    // in the afterDescendants mode if the EditText was focused 
	    listview.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);		
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}
	
}
