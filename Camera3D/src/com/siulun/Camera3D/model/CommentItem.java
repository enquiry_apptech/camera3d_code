package com.siulun.Camera3D.model;

public class CommentItem {
	
	String display_name, avator, comment, create_date;
	
	public CommentItem (String display_name, String avator, String comment, String create_date) {
		this.display_name = display_name;
		this.avator = avator;
		this.comment = comment;
		this.create_date = create_date;
	}
	
	public String getDisplayName() {
		return this.display_name;
	}
	
	public String getComment() {
		return this.comment;
	}
	
	public String getCreateDate() {
		return this.create_date;
	}
}
