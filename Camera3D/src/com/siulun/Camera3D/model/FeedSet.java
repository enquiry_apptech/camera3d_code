package com.siulun.Camera3D.model;

import java.util.ArrayList;
import java.util.Date;

import com.siulun.Camera3D.Camera3D;
import com.siulun.Camera3D.Utils;

public class FeedSet {
	String imageId, userId, categoryId, tags;
	int status;
	String fileName, title, description;
	int thumbnailPos = 0;
	int size, frame, width, height, like, comment, popluar;
	Date uploadDate;
	boolean userLike;
	ArrayList<FeedFrame> list = new ArrayList<FeedFrame>();
	
	public FeedSet (String fileName, int size, String title, String description, int status) {
		this.imageId = fileName;
		this.fileName = fileName;
		this.title = title;
		this.description = description;
		this.status = status;
		createItems(size);
	}
	
	public FeedSet (String fileName, int size, String title, String description, int status, int like, int comment,boolean userLike) {
		this.imageId = fileName;
		this.fileName = fileName;
		this.title = title;
		this.description = description;
		this.status = status;
		this.like = like;
		this.comment = comment;
		this.userLike = userLike;
		createItems(size);
	}
	
	public void createItems(int size) {
		for (int x = 0; x < size; x++)
			list.add(new FeedFrame());
	}

	public FeedFrame getFrame(int pos) {
		try {
			return this.list.get(pos);
		} catch (IndexOutOfBoundsException e)
		{			
			return this.list.get(0);
		}
	}
	public int getComment() {
		return this.comment;
	}
	
	public int getLike() {
		return this.like;
	}

	public int getStatus() {
		return this.status;
	}
	
	public int getFrame() {
		return this.frame;
	}
	
	public int getSize() {
		return this.size;
	}
	
	
	public String getImageId() {
		return this.imageId;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public String getDesc() {
		return this.description;
	}
	
	public String getFilename (){
		return fileName;
	}
	
	public String getFilePath(int pos) {
		return fileName + "_" + Utils.addZero(pos, 2) + ".jpg";
	}
	
	public String getUrl(int pos) {
		return Camera3D.S3_BASE_URL + "/" + fileName + "_" + Utils.addZero(pos, 2) + ".jpg";
	}
	
	public String getImageUrl() {
		return Camera3D.S3_BASE_URL + "/" + fileName + "_" + Utils.addZero(this.thumbnailPos, 2) + ".jpg";
	}
	
	public boolean getUserLike(){
		return this.userLike;
	}
	
	public int size() {
		return this.list.size();
	}
	
	public int frame() {
		return this.frame;
	}
	
	public void overrideDuration(int duration) {
		for(FeedFrame item : this.list) {
			item.setDuration(duration);
		}
	}
	
	public void setDuration(int pos, int duration) {
		this.list.get(pos).setDuration(duration);
	}
	
	public void setTitle (String title) {
		this.title = title;
	}
	
	public void setDesc (String desc) {
		this.description = desc;
	}
	
	public void setUserLike(boolean userlike){
		this.userLike = userlike;
	}
	
//	public reorder
}
