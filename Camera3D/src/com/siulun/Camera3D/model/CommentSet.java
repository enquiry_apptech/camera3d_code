package com.siulun.Camera3D.model;

import java.util.ArrayList;

public class CommentSet {
		
	ArrayList<CommentItem> list = new ArrayList<CommentItem>();	
	String displayName, avator, comment, create_date;

	public CommentSet () {
	}
	
	public void add(CommentItem item) {
		list.add(item);
	}
	
	public int size() {
		return this.list.size();
	}
}
