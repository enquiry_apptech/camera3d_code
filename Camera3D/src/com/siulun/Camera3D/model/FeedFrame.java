package com.siulun.Camera3D.model;

public class FeedFrame {
	int pos; // in ms
	int duration; // in ms
	
	public void setPos(int pos) {
		this.pos = pos;
	}

	public int getPos() {
		return this.pos;
	}
	
	public void setDuration(int d) {
		this.duration = d;
	}

	public int getDuration() {
		return this.duration;
	}
}
