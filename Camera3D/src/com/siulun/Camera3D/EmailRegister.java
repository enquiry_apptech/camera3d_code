package com.siulun.Camera3D;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EmailRegister extends Activity {
	
	boolean mRegister = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.register);

		final EditText firstName = (EditText) findViewById(R.id.firstName);
		final EditText lastName = (EditText) findViewById(R.id.lastName);
		final EditText email = (EditText) findViewById(R.id.email);
		final EditText password = (EditText) findViewById(R.id.password);
		final Button submit = (Button) findViewById(R.id.submit);
		final Button register = (Button) findViewById(R.id.register);
		register.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (!mRegister) {
					firstName.setVisibility(View.VISIBLE);
					lastName.setVisibility(View.VISIBLE);
					register.setText(R.string.str_login);
					mRegister = true;
				} else {
					firstName.setVisibility(View.GONE);
					lastName.setVisibility(View.GONE);
					firstName.setText("");
					lastName.setText("");
					register.setText(R.string.str_register);
					mRegister = false;
				}
			}
		});
		submit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (mRegister) {
					if (TextUtils.isEmpty(firstName.getText().toString())) {
						Toast.makeText(getApplicationContext(),"First name is empty",Toast.LENGTH_LONG).show();
						return;
					}
	
					if (TextUtils.isEmpty(lastName.getText().toString())) {
						Toast.makeText(getApplicationContext(),"Last name is empty",Toast.LENGTH_LONG).show();
						return;
					}
				}

				if (TextUtils.isEmpty(email.getText().toString())) {
					Toast.makeText(getApplicationContext(),"Email is empty",Toast.LENGTH_LONG).show();
					return;
				}

				if (TextUtils.isEmpty(password.getText().toString())) {
					Toast.makeText(getApplicationContext(),"Password is empty",Toast.LENGTH_LONG).show();
					return;
				}
				
				submit.setEnabled(false);

				new AsyncTask<Uri, Void, String>() {
					@Override
					protected String doInBackground(Uri... params) {
//						final Uri uri = params[0];

						return perpareDataToServer(0, firstName.getText()
								.toString(), lastName.getText().toString(),
								email.getText().toString(), password.getText()
										.toString());
					}

					protected void onPostExecute(String result) {
						JSONObject json;
						try {
							json = (JSONObject) new JSONTokener(result)
									.nextValue();
							if (json.get("status").toString().equals("done")) {
								Utils.setBoolean(EmailRegister.this, "isLogin",
										true);
								Utils.setString(EmailRegister.this, "userId",
										(String) json.get("id"));
								Toast.makeText(getApplicationContext(),
										"Login Success!", Toast.LENGTH_LONG)
										.show();
								Intent returnIntent = new Intent();
								returnIntent.putExtra(Login.LOGIN_OK, true);
								setResult(RESULT_OK,returnIntent);    
								finish();
							} else {
								submit.setEnabled(true);
								Toast.makeText(getApplicationContext(),
										(String) json.get("msg"),
										Toast.LENGTH_LONG).show();
							}
						} catch (NullPointerException e) {
							e.printStackTrace();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					};

				}.execute();

			}
		});
	}

	public String perpareDataToServer(int provider, String firstName,
			String lastName, String email, String password) {
		try {

			Utils.setInt(EmailRegister.this, "provider", provider);
			Utils.setString(EmailRegister.this, "provider_uid", "0");
			Utils.setString(EmailRegister.this, "display_name", firstName + " "
					+ lastName);
			Utils.setString(EmailRegister.this, "first_name", firstName);
			Utils.setString(EmailRegister.this, "last_name", lastName);
			Utils.setString(EmailRegister.this, "email", email);
			Utils.setString(EmailRegister.this, "access_token", "");

			return Login.sendToServer(Camera3D.SERVER_REGISTER_USER, provider,
					"", "", "", firstName + " " + lastName, email, firstName,
					lastName, password);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
