package com.siulun.Camera3D;

import java.lang.ref.SoftReference;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.SparseArray;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;

public class Preview3D extends Activity implements AccelerometerListener,
		OnClickListener {
	private ImageItem mImageItem;
	private ImageView mImageView, mIndicator;
	private Button mBtnSave, mBtnAccel;
	private TextView mFrame, mTextSpeed;
	private static Context CONTEXT;

	private int mSpeed;
    private static final SparseArray<SoftReference<Bitmap>> mImage =
        new SparseArray<SoftReference<Bitmap>>();
	private ArrayList<String> mImagePath = new ArrayList<String>();
	private int mHalf, mImageCount;
	private int mCurrent = Camera3D.CENTER;

	private GestureDetector detector;
	private myGestureListener gListener;
	private CharSequence dateStr;
	private SharedPreferences mPrefs;
	private ProgressDialog mDialog;
	private float mLast = 0, mLastX = 0;
	private boolean mIsLand;
	private final static int BUFFER_COUNT = 10;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		Intent i = getIntent();
		mIsLand = i.getBooleanExtra("isLand", true);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		if (mIsLand) {
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		} else
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.view3d);

		mContentLayout = (RelativeLayout) findViewById(R.id.content);
		mSpinner = (RelativeLayout) findViewById(R.id.dashboard_spinner);
		mTextSpeed = (TextView) findViewById(R.id.mSpeed);
		mFrame = (TextView) findViewById(R.id.mFrame);
		mImageView = (ImageView) findViewById(R.id.ImageView);
		mIndicator = (ImageView) findViewById(R.id.mIndicator);
		mBtnAccel = (Button) findViewById(R.id.mBtnAccel);
		mBtnSave = (Button) findViewById(R.id.mBtnSave);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if ((getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
				|| (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)) {

			mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
			if (mPrefs != null) {
				mSpeed = mPrefs.getInt("speed", 100);
			}

			CONTEXT = this;

			mTextSpeed.setText("Speed: " + mSpeed + "ms");
			mTextSpeed.setVisibility(View.GONE);
			mFrame.setVisibility(View.GONE);

			mImageItem = ImageItem.getInstance(this);
			mImageItem.setHasCache(true);
			// mIndicator.setAlpha(120);
			mBtnAccel.setVisibility(View.GONE);
			mImageCount = mImageItem.getImageCount();
			mHalf = Math.round(mImageCount / 2);
			mBtnSave.setOnClickListener(new Button.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					mBtnSave.setEnabled(false);
					mHandler.removeCallbacks(vibrate);
					mDialog = ProgressDialog.show(Preview3D.this, "",
							getText(R.string.dialog_saving), true);
					if (mImageItem.get(Camera3D.LEFT) != null) {

						if (!Environment.MEDIA_MOUNTED.equals(Environment
								.getExternalStorageState())) {

							Toast.makeText(Preview3D.this,
									getText(R.string.toast_ext_storage),
									Toast.LENGTH_LONG).show();
						} else {
							Thread t = new Thread() {
								public void run() {
									dateStr = mImageItem.SaveAllImage();
									mHandler.post(mDialogDismiss);
								}
							};
							t.start();
						}
					}
				}
			});

			if (mImageCount == 2) {
				mIndicator.setImageResource(R.drawable.ivib);
				AccelerometerManager.stopListening();
//				mImage.add(mImageItem.get(0));
//				mImage.add(mImageItem.get(1));
				mImage.put(0, new SoftReference<Bitmap>(mImageItem.get(0)));
				mImage.put(1, new SoftReference<Bitmap>(mImageItem.get(1)));
				mTextSpeed.setVisibility(View.VISIBLE);
				Vibration();
				updateUi(true);
			} else {
				mFrame.setVisibility(View.VISIBLE);
				mIndicator.setImageResource(R.drawable.s3);
				updateUi(false);
//				mDialog = ProgressDialog.show(Preview3D.this, "",
//						getText(R.string.dialog_loading), true);
				Thread t = new Thread() {
					public void run() {
						for (int count = 0; count < mImageCount; count++) {
//							mImage.add(mImageItem.get(x));
							if (count < BUFFER_COUNT) {
								mImage.put(count, new SoftReference<Bitmap>(mImageItem.get(count)));
//								mImage.add(decodeFile(newPath, mWidth));
//								Log.i("Buffer added: " + count);
							} 
//								else
//								mImage.add(null);
//							mImagePath.add(newPath);
						}
						mHandler.post(mDialogDismissForLoading);
					}
				};
				t.start();
			}
			gListener = new myGestureListener();
			detector = new GestureDetector(Preview3D.this, gListener);
		}
	}

	// Create runnable for posting
	final Runnable mDialogDismissForLoading = new Runnable() {
		public void run() {
			mTextSpeed.setVisibility(View.GONE);

			lazyLoadImage(mImageView, 0);
//			mImageView.setImageBitmap(mImage.get(Camera3D.LEFT));
			mIndicator.setImageResource(R.drawable.s1);
			if (AccelerometerManager.isSupported(CONTEXT)) {
				mBtnAccel.setOnClickListener(Preview3D.this);
				AccelerometerManager.startListening(CONTEXT, Preview3D.this);
			}
//			mDialog.dismiss();
			updateUi(true);
		}
	};

	RelativeLayout mContentLayout;
	RelativeLayout mSpinner;
	private void updateUi(Boolean status) {
		mContentLayout.setVisibility(status ? View.VISIBLE : View.GONE);
		mSpinner.setVisibility(status ? View.GONE : View.VISIBLE);
	}

	// Create runnable for posting
	final Runnable mDialogDismiss = new Runnable() {
		public void run() {
			if (dateStr == null) {
				Toast.makeText(Preview3D.this,
						Camera3D.FILE_PREFIX + dateStr + ".jpg save fail!",
						Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(Preview3D.this,
						Camera3D.FILE_PREFIX + dateStr + ".jpg saved!",
						Toast.LENGTH_LONG).show();
				// mTakeCount++;
			}
			mImageItem.setHasCache(false);
			SetPref();
			mBtnSave.setEnabled(true);
			mDialog.dismiss();
			finish();
		}
	};

	@Override
	public void onClick(View v) {
		mBtnAccel.setVisibility(View.GONE);
		if (AccelerometerManager.isSupported(CONTEXT)) {
			AccelerometerManager.startListening(CONTEXT, this);
		}
	}

	private final Handler mHandler = new Handler();
	private int mSwitchImage = 0;
	final Runnable vibrate = new Runnable() {
		public void run() {
			switch (mSwitchImage) {
			case (0):
				mImageView.setImageBitmap(mImage.get(1).get());
				mSwitchImage = 1;
				break;
			case (1):
				mImageView.setImageBitmap(mImage.get(0).get());
				mSwitchImage = 0;
				break;
			}
			mHandler.postDelayed(vibrate, mSpeed);
		}
	};

	private void Vibration() {
		Thread t = new Thread() {
			public void run() {
				mHandler.post(vibrate);
			}
		};
		t.start();
	}

	/*
	 * private String str_format(int cnt) { int len = (cnt + "").length();
	 * switch (len) { case (1): return "000" + cnt; case (2): return "00" + cnt;
	 * case (3): return "0" + cnt; default: return "" + cnt; } }
	 */

	private void SetPref() {
		if (mPrefs != null) {
			SharedPreferences.Editor ed = mPrefs.edit();
			ed.putInt("speed", mSpeed);
			ed.commit();
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this); // Add this method.
	}

	@Override
	public void onPause() {
		super.onPause();
		mHandler.removeCallbacks(vibrate);
		mHandler.removeCallbacks(mDialogDismiss);
		mHandler.removeCallbacks(mDialogDismissForLoading);
	}
	
	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance().activityStop(this); // Add this method.
		mHandler.removeCallbacks(vibrate);
		mHandler.removeCallbacks(mDialogDismiss);
		mHandler.removeCallbacks(mDialogDismissForLoading);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mHandler.removeCallbacks(vibrate);
		mHandler.removeCallbacks(mDialogDismiss);
		mHandler.removeCallbacks(mDialogDismissForLoading);
		if (AccelerometerManager.isListening()) {
			AccelerometerManager.stopListening();
		}
		for (int x=0; x < mImageCount; x++) {
	        SoftReference<Bitmap> reference = mImage.get(x);
			if (reference != null) {
				if (reference.get() != null)
					reference.get().recycle();
				mImage.remove(x);
//				Log.i("Forw Buffer recycled: " + y);
			}
		}
		mImage.clear();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getY() > 90) {
			if (detector.onTouchEvent(event)) {
				return detector.onTouchEvent(event);
			} else {
				return super.onTouchEvent(event);
			}
		} else {
			return false;
		}
	}

	private final static int TOUCH_DISTANCE = 10;

	public class myGestureListener implements GestureDetector.OnGestureListener {
		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			if (mImageCount == 2) {
				if (distanceX < 0 && mSpeed < 1000) {
					mSpeed += 20;
				} else if (distanceX > 0 && mSpeed > 40) {
					mSpeed -= 20;
				}
				mTextSpeed.setText("Speed: " + mSpeed + "ms");
				return false;
			}
			if (distanceX < -TOUCH_DISTANCE) {
				// Log.i("mCurrent: "+mCurrent);
				if (AccelerometerManager.isListening()) {
					AccelerometerManager.stopListening();
				}
				mBtnAccel.setVisibility(View.VISIBLE);
				if (mCurrent < mImageCount - 1) {
					mCurrent++;
					new preloadImage().execute(1, mCurrent);
					lazyLoadImage(mImageView, mCurrent);
					mFrame.setText((mCurrent + 1) + "");
				} else {
					mCurrent = 0;
				}
			} else if (distanceX > TOUCH_DISTANCE) {
				// Log.i("mCurrent: "+mCurrent);
				if (AccelerometerManager.isListening()) {
					AccelerometerManager.stopListening();
				}
				mBtnAccel.setVisibility(View.VISIBLE);
				if (mCurrent > 0) {
					mCurrent--;
				} else {
					mCurrent = mImageCount-1;
				}
				new preloadImage().execute(0, mCurrent);
				lazyLoadImage(mImageView, mCurrent);
				mFrame.setText((mCurrent + 1) + "");
			}
			if (mCurrent < mHalf - mHalf / 2)
				mIndicator.setImageResource(R.drawable.s1);
			else if (mCurrent < mHalf)
				mIndicator.setImageResource(R.drawable.s2);
			else if (mCurrent > mHalf + mHalf / 2)
				mIndicator.setImageResource(R.drawable.s5);
			else if (mCurrent > mHalf)
				mIndicator.setImageResource(R.drawable.s4);
			return false;
		}

		@Override
		public boolean onDown(MotionEvent arg0) {
			mHandler.removeCallbacks(mFlingEffect);
			return true;
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			if (mIsLand)
				mFlingVelocity = velocityX;
			else
				mFlingVelocity = velocityY;
			mHandler.removeCallbacks(mFlingEffect);
			mHandler.postDelayed(mFlingEffect, 100);
			return false;
		}

		@Override
		public void onLongPress(MotionEvent e) {
			/*
			 * if (bm != null) {
			 * 
			 * }
			 */
		}

		@Override
		public void onShowPress(MotionEvent e) {
		}

		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			return false;
		}
	}

	private float mFlingVelocity;
	
	private final Runnable mFlingEffect = new Runnable() {
		@Override
		public void run() {
			mHandler.removeCallbacks(mFlingEffect);
			if (mFlingVelocity != 0) {
				if (mFlingVelocity < 0) {
					if (mCurrent > 0){
						mCurrent--;
						if (mCurrent < 0) {
							mFlingVelocity = 0;
							return;
						}
					} else {
						return;
					}
				} else {
					if (mCurrent < mImageCount - 1) {
						mCurrent++;
						if (mCurrent >= mImageCount) {
							mFlingVelocity = 0;
							return;
						}
					} else {
						return;
					}
				}
				new preloadImage().execute(1, mCurrent);
				lazyLoadImage(mImageView, mCurrent);
				Thread t = new Thread() {
					public void run() {
					}
				};
//				if (mCurrent < mHalf - mHalf / 2)
//					mIndicator.setImageResource(R.drawable.s1);
//				else if (mCurrent < mHalf)
//					mIndicator.setImageResource(R.drawable.s2);
//				else if (mCurrent > mHalf + mHalf / 2)
//					mIndicator.setImageResource(R.drawable.s5);
//				else if (mCurrent > mHalf)
//					mIndicator.setImageResource(R.drawable.s4);
				
				mFrame.setText((mCurrent + 1) + "");
				mFlingVelocity = mFlingVelocity * 0.90f;
				Log.i("Velocity: " + mFlingVelocity);
				final int maxFlingVelocity = ViewConfiguration.get(Preview3D.this).getScaledMaximumFlingVelocity();
				float ratio = 1.0f - ((mFlingVelocity>0?mFlingVelocity:-mFlingVelocity)/(float)maxFlingVelocity);
				Log.i("Ratio: " + ratio);
				if (ratio > 0.99)
					return;
				int delayMillis = (int) (ratio * 500 * 0.1f);
				if (delayMillis < 1) {
					mHandler.post(mFlingEffect);
				} else {
					mHandler.postDelayed(mFlingEffect, delayMillis);
				}
				Log.i("delayMillis: " + delayMillis);
			}
		}
	};
	
	@Override
	public void onAccelerationChanged(float x, float y, float z) {
		float level;
		if (mIsLand)
			level = y;
		else
			level = -x;
		if (mImage == null)
			return;
		if (mImage.size() <= 0)
			return;
		if (mImageCount == 2)
			return;
		if (level > 8)
			level = x;
		if (mLast > level && mLast - 0.1 < level)
			return;
		if (mLast < level && mLast + 0.1 > level)
			return;
		if (level < -5 || level > 5)
			return;
//		if (mLast < level && level > 1) {
//			if (mCurrent > 0) {
//				mCurrent--;
//				new preloadImage().execute(0, mCurrent);
//			}
//		} else if (mLast < level && level < -1) {
//			if (mCurrent > mHalf) {
//				mCurrent--;
//				new preloadImage().execute(0, mCurrent);
//			}
//		} else if (mLast > level && level > 1) {
//			if (mCurrent < mHalf) {
//				mCurrent++;
//				new preloadImage().execute(1, mCurrent);
//			}
//		} else if (mLast > level && level < -1) {
//			if (mCurrent < mImageCount - 1) {
//				mCurrent++;
//				new preloadImage().execute(1, mCurrent);
//			}
//		}
		if (mLast < level && level > 1) {
			if (mCurrent < mImageCount - 1) {
				mCurrent++;
				new preloadImage().execute(1, mCurrent);
			}
		} else if (mLast < level && level < -1) {
			if (mCurrent < mHalf) {
				mCurrent++;
				new preloadImage().execute(1, mCurrent);
			}
		} else if (mLast > level && level > 1) {
			if (mCurrent > mHalf) {
				mCurrent--;
				new preloadImage().execute(0, mCurrent);
			}
		} else if (mLast > level && level < -1) {
			if (mCurrent > 0) {
				mCurrent--;
				new preloadImage().execute(0, mCurrent);
			}
		}

		SoftReference<Bitmap> reference = mImage.get(mCurrent);
		if (reference != null) {
			if (mCurrent < mHalf - mHalf / 2)
				mIndicator.setImageResource(R.drawable.s1);
			else if (mCurrent < mHalf)
				mIndicator.setImageResource(R.drawable.s2);
			else if (mCurrent > mHalf + mHalf / 2)
				mIndicator.setImageResource(R.drawable.s5);
			else if (mCurrent > mHalf)
				mIndicator.setImageResource(R.drawable.s4);

			lazyLoadImage(mImageView, mCurrent);

			mFrame.setText((mCurrent + 1) + "");
		}
	}
	
	private ImageView mCheckImageView;
	private int mCheckBitmapPos;
	
	private void lazyLoadImage(ImageView iv, int pos) {
		mHandler.removeCallbacks(mCheckImageLoaded);
        SoftReference<Bitmap> reference = mImage.get(pos);
		if (reference != null && reference.get() != null) {
			try {
				iv.setImageBitmap(reference.get());
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				mCheckImageView = iv;
				mCheckBitmapPos = pos;
				mHandler.postDelayed(mCheckImageLoaded, 50);
			}
			return;
		} else {
//			Log.i("No Image found: " + pos);
			mImage.put(pos, new SoftReference<Bitmap>(mImageItem.get(pos)));
			iv.setImageBitmap(mImage.get(pos).get());
		}		
	}
	
	private Runnable mCheckImageLoaded = new Runnable() {
		
		@Override
		public void run() {
			lazyLoadImage(mCheckImageView, mCheckBitmapPos);
		}
	};
	
	private class preloadImage extends AsyncTask<Integer, Void, Void> {
	    /** The system calls this to perform work in a worker thread and
	      * delivers it the parameters given to AsyncTask.execute() 
	     * @return */
	    protected Void doInBackground(Integer... params) {
	    	int isForward = params[0];
	    	int pos = params[1];
			int forPos, backPos, savePos, backsavePos;
			forPos = pos + BUFFER_COUNT;
			backPos = pos - BUFFER_COUNT;
			savePos = forPos - pos;
			backsavePos = pos + backPos;
			if (isForward == 1) {
//				Log.i("Current: " + pos + ", isForward: " + isForward
//						+ ", forPos: " + forPos + ", backPos: " + backPos);
				for (int x = pos; x < forPos; x++) {
					if (x < mImageCount && x >= 0) {
				        SoftReference<Bitmap> reference = mImage.get(x);
						if (reference == null) {
							mImage.put(x, new SoftReference<Bitmap>(mImageItem.get(x)));
//							Log.i("Forw Buffer added: " + x);
						}
					}
				}
				for (int y = backPos; y >= savePos; y--) {
					if (y < mImageCount && y >= 0) {
				        SoftReference<Bitmap> reference = mImage.get(y);
						if (reference != null) {
							if (reference.get() != null)
								reference.get().recycle();
							mImage.remove(y);
//							Log.i("Forw Buffer recycled: " + y);
						}
					}
				}
			} else {
//				Log.i("Current: " + pos + ", isForward: " + isForward
//						+ ", forPos: " + forPos + ", backPos: " + backPos);
				for (int x = pos; x >= backPos; x--) {
					if (x < mImageCount && x >= 0) {
				        SoftReference<Bitmap> reference = mImage.get(x);
						if (reference == null) {
							mImage.put(x, new SoftReference<Bitmap>(mImageItem.get(x)));
//							mImage.put(x, new SoftReference<Bitmap>(decodeFile(mImagePath.get(x), mWidth)));
//							Log.i("Back Buffer added: " + x);
						}
					}
				}
				for (int y = forPos; y <= backsavePos; y++) {
					if (y < mImageCount && y >= 0) {
				        SoftReference<Bitmap> reference = mImage.get(y);
						if (reference != null) {
							if (reference.get() != null)
								reference.get().recycle();
							mImage.remove(y);
//							Log.i("Back Buffer recycled: " + y);
						}
					}
				}
			}
			return null;
	    }
	}

	@Override
	public void onShake(float force) {
		// Toast.makeText(this, "Phone shaked : " + force, 1000).show();
	}
}
