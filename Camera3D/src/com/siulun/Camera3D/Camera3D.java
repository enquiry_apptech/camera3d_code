package com.siulun.Camera3D;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockActivity;
import com.google.analytics.tracking.android.EasyTracker;

public class Camera3D extends SherlockActivity {
//	private final static String welcomeScreenShownPref = "welcomeScreenShownPref_201303160";
	public final static String TAG = "Camera3D";
	public final static boolean DEBUG = false;
	public final static boolean DISPLAY_AD = true;
	public final static String BASE_URL = "http://api.apptech.com.hk/camera3D";
	public final static String IMAGE_URL = BASE_URL + "/images";
	public final static String SERVER_REGISTER_USER = BASE_URL+ "/"
			+ "set.php?mode=registerUser";
	public final static String SERVER_SET_IMAGEID = BASE_URL+ "/"
	+ "set.php?mode=getImageId";
	public final static String SERVER_SET_IMAGE_INFO = BASE_URL+ "/"
	+ "set.php?mode=setImageInfo";
	public final static String SERVER_GET_PRIVATE = BASE_URL+ "/"
	+ "get.php?mode=getPrivate";
	public final static String SERVER_GET_PUBLIC = BASE_URL+ "/"
	+ "get.php?mode=getPublic";
	public final static String SERVER_SET_IMAGE_PUBLIC = BASE_URL+ "/"
	+ "set.php?mode=setImagePublic";
	public final static String SERVER_SET_LIKE = BASE_URL+ "/"
	+ "set.php?mode=setLike";
	public final static String SERVER_SET_COMMENT = BASE_URL+ "/"
	+ "set.php?mode=setComment";
	public final static String SERVER_GET_COMMENT = BASE_URL+ "/"
	+ "get.php?mode=getComment";
	public final static String SERVER_GET_USER_LIKE = BASE_URL+ "/"
			+ "get.php?mode=getUserLike";
	public final static String SERVER_DELETE_PHOTO = BASE_URL+ "/"
			+ "set.php?mode=deletePhoto";
	public final static String SERVER_UPDATE_IMAGE_INFO = BASE_URL+ "/"
			+ "set.php?mode=updateImageInfo";
	
	public final static String S3_BASE_URL = "https://apptechhk-camera3d.s3.amazonaws.com";	

	public final static String ONIMAGECLICKPOS = "ONIMAGECLICKPOS";

	// public final static String MY_PREFS = "Camera3D";
	public final static String FILE_PREFIX = "I3D_";
	public static String FOLDER_NAME = ".Camera3D";
	public static String FOLDER_NAME_THUMB = ".Camera3D/.thumbnails";
	public final static int LEFTLEFT = 0;
	public final static int LEFT = 1;
	public final static int CENTER = 2;
	public final static int RIGHT = 3;
	public final static int RIGHTRIGHT = 4;
	public final static int MAX_VIEW = 5;

	public final static int MAX_3d_LENGTH = 1200;
	private final static int IMAGE_3D_WIDTH = 160;
	private ProgressDialog mDialog;

	public static final int ACTIVITY_DELETE = 0x1001;
	private SharedPreferences mPrefs;
	private final Handler mHandler = new Handler();

	private int mImageCount = 0, mImageWidth;
	ArrayList<ImageView> mImageView3D = new ArrayList<ImageView>();
	ArrayList<String> mThumbList;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		}

	@Override
	protected void onResume() {
		super.onResume();
	}
	

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance().activityStop(this); // Add this method.
	}

	@Override
	protected void onPause() {
		super.onPause();
	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}