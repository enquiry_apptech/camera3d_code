package com.siulun.Camera3D;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;
import butterknife.OnClick;
import butterknife.Views;

import com.actionbarsherlock.app.SherlockActivity;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.google.analytics.tracking.android.EasyTracker;
import com.siulun.Camera3D.model.FeedSet;

public class ImageInfoEdit extends SherlockActivity {

	private String mImageId;
	private int mSize;
	ArrayList<String> files = new ArrayList<String>();
	RelativeLayout mSpinner;
	EditText mTitle;
	EditText mDesc;
	Button mEditButton;
	EditListviewInterface listInterface;
//	EditText mTag1;
//	EditText mTag2;
//	EditText mTag3;
	
	public interface EditListviewInterface{
		public void updateTitleDesc(int pos,String title,String desc);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.image_info_edit);
	//    Views.inject(this);
		AdView adView = (AdView) this.findViewById(R.id.adView);
		adView.loadAd(new AdRequest());
		
		listInterface = MainActivity.feedViewPrivateFragment;
		
		mSpinner = (RelativeLayout) findViewById(R.id.dashboard_spinner);
		mTitle = (EditText) findViewById(R.id.title);
		mDesc = (EditText) findViewById(R.id.desc);
//		mTag1 = (EditText) findViewById(R.id.tag1);
//		mTag2 = (EditText) findViewById(R.id.tag2);
//		mTag3 = (EditText) findViewById(R.id.tag3);
		
		final Intent intent = getIntent();
		mTitle.setText(intent.getStringExtra("title"));
		mDesc.setText(intent.getStringExtra("desc"));
		mImageId = intent.getStringExtra("image_id");
		mSize = intent.getIntExtra("size", -1);
	

		mEditButton = (Button) findViewById(R.id.submit);
		mEditButton.setOnClickListener(new View.OnClickListener() {
			
		@Override
		public void onClick(View v) {
		if ( TextUtils.isEmpty(mTitle.getText().toString())) {
			Toast.makeText(getApplicationContext(), "Title must not empty...", Toast.LENGTH_LONG).show();
			return;
		}

        mSpinner.setVisibility(View.VISIBLE);
        
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				try {
					return sendToServer();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
			}

			protected void onPostExecute(String result) {
//				Log.i(Camera3D.TAG, result);
				JSONObject json;
				try {
					if (!TextUtils.isEmpty(result)) {
						json = (JSONObject) new JSONTokener(result).nextValue();
						if (json.get("status").toString().equals("done")) {
							Toast.makeText(getApplicationContext(), "Changes saved.", Toast.LENGTH_LONG).show();
							listInterface.updateTitleDesc(intent.getIntExtra("pos", 0), mTitle.getText().toString(), mDesc.getText().toString());
							finish();
						} else {
							Toast.makeText(getApplicationContext(), "No Response. Please try again later.", Toast.LENGTH_LONG).show();
						}
					} else {
						Toast.makeText(getApplicationContext(), "No Response. Please try again later.", Toast.LENGTH_LONG).show();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			};

		}.execute();
	}
	});	
	}
	
	private String sendToServer() throws IOException {
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(Camera3D.SERVER_UPDATE_IMAGE_INFO);

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
				2);
		nameValuePairs.add(new BasicNameValuePair("userId", Utils.getString(ImageInfoEdit.this, "userId")));
		nameValuePairs.add(new BasicNameValuePair("title", "" + mTitle.getText().toString()));
		nameValuePairs.add(new BasicNameValuePair("desc", mDesc.getText().toString()));
		nameValuePairs.add(new BasicNameValuePair("image_id", mImageId));
		
		try {
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();

			String str = Utils.convertStreamToString(entity.getContent());

			return str;
		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}
		return null;
	}


	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance().activityStop(this); // Add this method.
	}
//	private void updateUi(boolean status) {
//		mLoginContainer.setVisibility(status?View.GONE:View.VISIBLE);
//		mSpinner.setVisibility(status?View.VISIBLE:View.GONE);
//	}

}
