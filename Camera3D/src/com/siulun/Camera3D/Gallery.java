package com.siulun.Camera3D;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.google.analytics.tracking.android.EasyTracker;
import com.squareup.picasso.Picasso;

public class Gallery extends SherlockActivity {
	private ArrayList<String> mFilesThumb;
	// private List<Bitmap> mBitmap;
	private ArrayList<Boolean> mIsLand;
	// private Bitmap [] mBitmap;
	private GridView g;
	private BaseAdapter mBaseAdapter;
	private int mColWidth;
	private static final int DIALOG_MENU_ABOUT = 0;
	private static final int DIALOG_ENCODE = 1;
	private ProgressDialog mDialog;
	private String mExportName;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.gallery);

		// AdManager.setTestDevices( new String[] {
		// "BD6F0DFC6AF2E57660C66F6DC6246E4F" } );
		if (Camera3D.DISPLAY_AD) {
			AdView adView = (AdView) this.findViewById(R.id.adView);
			adView.loadAd(new AdRequest());
		}

		final DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		final int height = displaymetrics.heightPixels;
		int width = displaymetrics.widthPixels;
		// Display display = ((WindowManager)
		// getSystemService(Context.WINDOW_SERVICE))
		// .getDefaultDisplay();
		// int width = display.getWidth();
		// int height = display.getHeight();
		g = (GridView) findViewById(R.id.myGrid);
		if (width > height) {
			mColWidth = (int) width / 4 - 10;
			// width = height;
		} else
			mColWidth = (int) width / 3 - 10;

		g.setColumnWidth(mColWidth);
		// mBitmap = new ArrayList<Bitmap>();

		//for old version, where user click a photo to go to album, delay 100ms and then open the photo in view3D
		Intent i = getIntent();
		final int pos = i.getIntExtra(Camera3D.ONIMAGECLICKPOS, -1);
		if (pos != -1) {
			mHandler.postDelayed(new Runnable() {

				@Override
				public void run() {
					ViewPhoto(pos);
				}
			}, 100);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		mFilesThumb = Utils.getDir(Utils.getAbsPath() + "/"
				+ Camera3D.FOLDER_NAME_THUMB);
		mIsLand = new ArrayList<Boolean>();
		if (Utils.getBoolean(this, "isthumbrecreated", true)) {
			if (mFilesThumb != null) {

				if (mDialog != null) {
					if (!mDialog.isShowing())
						mDialog = ProgressDialog.show(Gallery.this, "",
								getText(R.string.dialog_loading), true);
				} else {
					mDialog = ProgressDialog.show(Gallery.this, "",
							getText(R.string.dialog_loading), true);
				}

				int len = mFilesThumb.size();
				for (int pos = 0; pos < len; pos++) {

					try {
						String uri = mFilesThumb.get(pos).replace("_T", "_01");
						uri = uri.replace("/.thumbnails", "");
						final File n = new File(uri);
						FileOutputStream bos = new FileOutputStream(
								mFilesThumb.get(pos));
						final Bitmap scaled = decodeFile(n, 200);
						scaled.compress(Bitmap.CompressFormat.JPEG, 80, bos);
						bos.flush();
						bos.close();
						scaled.recycle();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			Utils.getBoolean(this, "isthumbrecreated", true);
			mHandler.postDelayed(mDialogDismiss, 100);
		}

		if (mFilesThumb != null) {
			int len = mFilesThumb.size();
			// mBitmap = new Bitmap[len];
			for (int pos = 0; pos < len; pos++) {
				// for (int pos = len-1; pos >= 0; pos--)
				FileInputStream inputStream;
				try {
					inputStream = new FileInputStream(
							(String) mFilesThumb.get(pos));
					BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
					bitmapOptions.inJustDecodeBounds = true;
					BitmapFactory
							.decodeStream(inputStream, null, bitmapOptions);
					int imageWidth = bitmapOptions.outWidth;
					int imageHeight = bitmapOptions.outHeight;
					inputStream.close();
					mIsLand.add(imageWidth > imageHeight);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					mIsLand.add(true);
				} catch (IOException e) {
					e.printStackTrace();
					mIsLand.add(true);
				}
				// mBitmap.add(decodeFile((String) mFilesThumb.get(pos),
				// mColWidth));
			}
			// mBitmap[pos] = decodeFile((String)mFilesThumb.get(pos),
			// mColWidth);

			mBaseAdapter = new ImageAdapter(this);
			g.setAdapter(mBaseAdapter);
			g.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView parent, View v,
						final int position, long id) {
					ViewPhoto(position);
				}
			});
			g.setOnCreateContextMenuListener(this);
		} else
			Toast.makeText(this, getText(R.string.toast_no_images),
					Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// setContentView(R.layout.myLayout);
	}

	// Create runnable for posting
	final Runnable mDialogDismiss = new Runnable() {
		public void run() {
			try {
				if (mDialog != null && mDialog.isShowing())
					mDialog.dismiss();
			} catch (Exception e) {
				// nothing
			}
		}
	};

	public Bitmap decodeFile(String filename, int maxSize) {
		File file = new File(filename);
		return decodeFile(file, maxSize);
	}

	public Bitmap decodeFile(File file, int maxSize) {
		try {
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(file), null, o);
			int scale = 1;
			if (o.outHeight > maxSize || o.outWidth > maxSize) {
				scale = (int) Math.pow(
						2,
						(int) Math.round(Math.log(maxSize
								/ (double) Math.max(o.outHeight, o.outWidth))
								/ Math.log(0.5)));
			}

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(file), null,
					o2);
		} catch (FileNotFoundException e) {
			// Log.e(e.toString());
			return null;
		}
	}

	public void onCreateContextMenu(ContextMenu menu, View view,
			ContextMenuInfo menuInfo) {
		// Inflate the menu from xml.
		getMenuInflater().inflate(R.menu.gallery_menu, menu);
		final AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
		File file = new File((String) mFilesThumb.get(info.position));
		menu.setHeaderTitle(file.getName().replace("_T", ""));
	}

	private int mMSTime;
	private int mId;
	// private RadioGroup mEncodeSize;
	private Spinner mEncodeSpinner;
	private TextView mMSText;
	private SeekBar mMS;
	
	@Override
	public boolean onContextItemSelected(final MenuItem item) {
		final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		mId = (int) info.id;
		switch (item.getItemId()) {
		
		case R.id.menu_upload:
			if (!Utils.getBoolean(Gallery.this, "isLogin", false)) {
				Toast.makeText(getApplicationContext(),"Please login first...",Toast.LENGTH_LONG).show();
				Intent i = new Intent(Gallery.this, Login.class);
				startActivity(i);
				finish();
				return false;
			}

			String file_path = (String) mFilesThumb.get(mId);
//			mFilesThumb.remove(mId);

			
//            Intent intent = new Intent(this, UploadService.class);
//            intent.putStringArrayListExtra(UploadService.ARG_FILE_PATH, files);
//            startService(intent);

            Intent intent = new Intent(Gallery.this, ImageInfoSubmit.class);
            intent.putExtra("file_path", file_path);
            startActivity(intent);            
            
			return true;
		case R.id.menu_delete:
			// Confirm that the alarm will be deleted.
			new AlertDialog.Builder(this)
					.setTitle(getString(R.string.dialog_delete))
					.setMessage(getString(R.string.dialog_delete_confirm))
					.setPositiveButton(android.R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface d, int w) {
									delete_photo(mId);
								}
							}).setNegativeButton(android.R.string.cancel, null)
					.show();
			return true;

		case R.id.menu_view:
			ViewPhoto(mId);
			return true;

		case R.id.menu_export:
			final Dialog dialog = new Dialog(Gallery.this);
			dialog.setContentView(R.layout.encode);
			// dialog.setTitle(null);
			dialog.setTitle(getText(R.string.dialog_export));
			dialog.setCancelable(true);
			// there are a lot of settings, for dialog, check them all out!
			mMS = (SeekBar) dialog.findViewById(R.id.mMS);
			// mEncodeSize = (RadioGroup) dialog.findViewById(R.id.mEncodeSize);
			mEncodeSpinner = (Spinner) dialog.findViewById(R.id.mEncodeSpinner);
			mMSText = (TextView) dialog.findViewById(R.id.mMSText);

			// set up button
			Button mBtnOK = (Button) dialog.findViewById(R.id.mBtnOK);
			mMS.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {
					switch (seekBar.getId()) {
					case (R.id.mMS):
						mMSTime = progress + 100;
						mMSText.setText(mMSTime + " ms");
						break;
					}
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {

				}

				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
				}
			});

			mBtnOK.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mFilepath = (String) mFilesThumb.get(mId).replace(
							"/.thumbnails", "");
					mDialog = ProgressDialog.show(Gallery.this, "",
							getText(R.string.dialog_encoding), true);

					Thread t = new Thread() {
						public void run() {
							try {
								File sdCard = Environment
										.getExternalStorageDirectory();
								File f = new File(sdCard.getAbsolutePath()
										+ "/sd");
								String AbsPath;
								if (f.exists())
									AbsPath = sdCard.getAbsolutePath() + "/sd";
								else
									AbsPath = sdCard.getAbsolutePath();

								f = new File(AbsPath + "/Pictures");
								if (!f.exists()) {
									f.mkdir();
								}
								// int eid =
								// mEncodeSize.getCheckedRadioButtonId();
								int eid = mEncodeSpinner
										.getSelectedItemPosition();
								int ex = 400, ey = 300;
								switch (eid) {
								case 0:
									ex = 400;
									ey = 300;
									break;
								case 1:
									ex = 600;
									ey = 450;
									break;
								case 2:
									ex = 800;
									ey = 600;
									break;
								case 3:
									ex = 1024;
									ey = 768;
									break;
								case 4:
									ex = 1280;
									ey = 1024;
									break;
								case 5:
									ex = 1920;
									ey = 1080;
									break;
								}

								JavaGIFEncoder e = new JavaGIFEncoder();
								FileOutputStream bos = null;
								Bitmap scaled;

								// Check Landscape or Portrait
								f = new File(mFilepath.replace("_T", "_"
										+ Utils.addZero(0, 2)));
								if (f.exists()) {
									BitmapFactory.Options o = new BitmapFactory.Options();
									o.inJustDecodeBounds = true;
									BitmapFactory.decodeStream(
											new FileInputStream(f), null, o);
									if (o.outWidth < o.outHeight) {
										int temp = ex;
										ex = ey;
										ey = temp;
									}
								}

								File n = new File(mFilepath.replace(
										Camera3D.FOLDER_NAME, "Pictures")
										.replace("_T.jpg", ".gif"));
								bos = new FileOutputStream(n.getAbsolutePath());
								e.start(bos);
								e.setSize(ex, ey);
								e.setDelay(mMSTime); // 1 frame per sec
								e.setRepeat(0);

								boolean done = true;
								int count = 0;
								do {
									f = new File(mFilepath.replace("_T", "_"
											+ Utils.addZero(count, 2)));
									if (f.exists()) {
										scaled = Bitmap.createScaledBitmap(
												decodeFile(f, (ex > ey) ? ex
														: ey), ex, ey, true);
										e.addFrame(scaled);
										scaled.recycle();
										done = true;
										count++;
									} else
										done = false;
								} while (done);

								e.finish();
								bos.flush();
								bos.close();

								System.gc();
								mExportName = n.getName();
							} catch (FileNotFoundException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							mHandler.post(mDialogDismissExport);
						}
					};
					t.start();
					dialog.dismiss();
				}
			});
			dialog.show();
			return true;

		default:
			break;
		}
		return super.onContextItemSelected(item);
	}

	private String mFilepath;
	private Handler mHandler = new Handler();
	// Create runnable for posting
	final Runnable mDialogDismissExport = new Runnable() {
		public void run() {
			if (mDialog.isShowing())
				mDialog.dismiss();
			Toast.makeText(
					Gallery.this,
					getText(R.string.toast_export) + "sdcard\\Pictures\\"
							+ mExportName, Toast.LENGTH_LONG).show();
		}
	};

	private void ViewPhoto(int position) {
		Intent i = new Intent(Gallery.this, View3D.class);
		i.putExtra("filepath", (String) mFilesThumb.get(position));
		i.putExtra("fileindex", position);
		i.putExtra("isLand", mIsLand.get(position));
		startActivityForResult(i, Camera3D.ACTIVITY_DELETE);
	}

	/*
	 * private boolean delete_photo(int pos) {
	 * 
	 * String file_path = (String)mFilesThumb.get(pos); mFilesThumb.remove(pos);
	 * BitmapUtils.delete_file(file_path);
	 * 
	 * file_path = file_path.replace("/.thumbnails", "");
	 * BitmapUtils.delete_file(file_path.replace("_T", "_LL"));
	 * BitmapUtils.delete_file(file_path.replace("_T", "_L"));
	 * BitmapUtils.delete_file(file_path.replace("_T", "_C"));
	 * BitmapUtils.delete_file(file_path.replace("_T", "_R"));
	 * BitmapUtils.delete_file(file_path.replace("_T", "_RR"));
	 * mBitmap.get(pos).recycle(); mBitmap.remove(pos);
	 * mBaseAdapter.notifyDataSetChanged(); return true;
	 * 
	 * }
	 * 
	 * public static boolean delete_photo(String path) {
	 * 
	 * BitmapUtils.delete_file(path);
	 * 
	 * path = path.replace("/.thumbnails", "");
	 * BitmapUtils.delete_file(path.replace("_T", "_LL"));
	 * BitmapUtils.delete_file(path.replace("_T", "_L"));
	 * BitmapUtils.delete_file(path.replace("_T", "_C"));
	 * BitmapUtils.delete_file(path.replace("_T", "_R"));
	 * BitmapUtils.delete_file(path.replace("_T", "_RR")); return true; }
	 */

	private boolean delete_photo(int pos) {

		String file_path = (String) mFilesThumb.get(pos);
		mFilesThumb.remove(pos);
		BitmapUtils.delete_file(file_path);

		file_path = file_path.replace("/.thumbnails", "");
		/*
		 * File f = new File(file_path.replace("_T", "_L")); if (f.exists()) {
		 * BitmapUtils.delete_file(file_path.replace("_T", "_LL"));
		 * BitmapUtils.delete_file(file_path.replace("_T", "_L"));
		 * BitmapUtils.delete_file(file_path.replace("_T", "_C"));
		 * BitmapUtils.delete_file(file_path.replace("_T", "_R"));
		 * BitmapUtils.delete_file(file_path.replace("_T", "_RR")); } else { }
		 */
		boolean done = false;
		int count = 0;
		do {
			done = BitmapUtils.delete_file(file_path.replace("_T",
					"_" + Utils.addZero(count, 2)));
			count++;
		} while (done);
		// mBitmap.get(pos).recycle();
		// mBitmap.remove(pos);
		mBaseAdapter.notifyDataSetChanged();
		return true;

	}

	public static boolean delete_photo(String file_path) {

		BitmapUtils.delete_file(file_path);

		file_path = file_path.replace("/.thumbnails", "");
		/*
		 * File f = new File(file_path.replace("_T", "_L")); if (f.exists()) {
		 * BitmapUtils.delete_file(file_path.replace("_T", "_LL"));
		 * BitmapUtils.delete_file(file_path.replace("_T", "_L"));
		 * BitmapUtils.delete_file(file_path.replace("_T", "_C"));
		 * BitmapUtils.delete_file(file_path.replace("_T", "_R"));
		 * BitmapUtils.delete_file(file_path.replace("_T", "_RR")); } else { }
		 */
		boolean done = false;
		int count = 0;
		do {
			done = BitmapUtils.delete_file(file_path.replace("_T",
					"_" + Utils.addZero(count, 2)));
			count++;
		} while (done);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent i) {
		super.onActivityResult(requestCode, resultCode, i);
		if (resultCode == RESULT_OK) {
			if (requestCode == Camera3D.ACTIVITY_DELETE) {
				int pos = i.getIntExtra("fileindex", -1);
				if (pos != -1 && mFilesThumb.size() > 0) {
					mFilesThumb.remove(pos);
					// mBitmap.get(pos).recycle();
					// mBitmap.remove(pos);
					mBaseAdapter.notifyDataSetChanged();
				}
			}
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this); // Add this method.
		IntentFilter f = new IntentFilter();
		f.addAction(UploadService.UPLOAD_STATE_CHANGED_ACTION);
		registerReceiver(uploadStateReceiver, f);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		mHandler.removeCallbacks(mDialogDismiss);
		mHandler.removeCallbacks(mDialogDismissExport);
		// mHandler.removeCallbacks(mFlingEffect);
	}

	@Override
	public void onStop() {
		super.onStop();
		unregisterReceiver(uploadStateReceiver);
		mHandler.removeCallbacks(mDialogDismiss);
		mHandler.removeCallbacks(mDialogDismissExport);
		// mHandler.removeCallbacks(mFlingEffect);
		EasyTracker.getInstance().activityStop(this); // Add this method.
	}


	private BroadcastReceiver uploadStateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle b = intent.getExtras();
			// status.setText(b.getString(UploadService.MSG_EXTRA));
			int percent = b.getInt(UploadService.PERCENT_EXTRA);
			// progress.setIndeterminate(percent < 0);
			// progress.setProgress(percent);
		}
	};

	private String getPathFromContentUri(Uri uri) {
		String path = uri.getPath();
		if (uri.toString().startsWith("content://")) {
			String[] projection = { MediaStore.MediaColumns.DATA };
			ContentResolver cr = getApplicationContext().getContentResolver();
			Cursor cursor = cr.query(uri, projection, null, null, null);
			if (cursor != null) {
				try {
					if (cursor.moveToFirst()) {
						path = cursor.getString(0);
					}
				} finally {
					cursor.close();
				}
			}

		}
		return path;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mHandler.removeCallbacks(mDialogDismissExport);
		// if (mFilesThumb != null) {
		// int len = mFilesThumb.size();
		// for (int x = 0; x < len; x++) {
		// if (mBitmap.get(0) != null)
		// mBitmap.get(0).recycle();
		// mBitmap.remove(0);
		// }
		// mBitmap.clear();
		// /*
		// * int len = mFilesThumb.size(); for (int x=0; x<len; x++) {
		// * //mBitmap[x].recycle(); //mBitmap[x] = null; }
		// */
		// }
	}

	// protected static final int MENU_PREF = Menu.FIRST+1;
	// protected static final int MENU_TYPE = Menu.FIRST;
	protected static final int MENU_ABOUT = Menu.FIRST;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// menu.add(0, MENU_PREF, 0,
		// getText(R.string.set_preferences)).setIcon(android.R.drawable.ic_menu_preferences);
		// menu.add(0, MENU_TYPE, 0,
		// getText(R.string.dialog_camera)).setIcon(android.R.drawable.ic_menu_camera);
		menu.add(0, MENU_ABOUT, 0, getText(R.string.dialog_about)).setIcon(
				android.R.drawable.ic_menu_info_details);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
		super.onOptionsItemSelected(item);
		switch (item.getItemId()) {
		/*
		 * case MENU_PREF: Intent settingsActivity = new
		 * Intent(getBaseContext(), Preferences.class);
		 * startActivity(settingsActivity); break; case MENU_TYPE: Intent i =
		 * new Intent(getBaseContext(), TakePhoto.class); startActivity(i);
		 * break;
		 */
		case MENU_ABOUT:
			showDialog(DIALOG_MENU_ABOUT);
			break;
		}
		return true;
	}

	@Override
	public Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_MENU_ABOUT:
			final TextView message = new TextView(this);
			SpannableString s = new SpannableString(
					this.getText(R.string.dialog_about_1));
			Linkify.addLinks(s, Linkify.EMAIL_ADDRESSES);
			message.setText(s);
			message.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
			s = new SpannableString(this.getText(R.string.dialog_about_2));
			Linkify.addLinks(s, Linkify.WEB_URLS);
			message.append(s);
			s = new SpannableString(this.getText(R.string.dialog_about_3));
			message.append(s);
			message.setMovementMethod(LinkMovementMethod.getInstance());
			message.setPadding(20, 20, 20, 20);
			return new AlertDialog.Builder(this)
					.setTitle(R.string.dialog_about)
					// .setMessage(R.string.dialog_about_detail)
					.setView(message)
					.setPositiveButton(R.string.OK,
							new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface dialoginterface, int i) {
									dialoginterface.cancel();
								}
							}).show();
		case DIALOG_ENCODE:
		}
		return null;
	}

	public class ImageAdapter extends BaseAdapter {
		Bitmap myBitmap;

		public ImageAdapter(Context c) {
			mContext = c;
		}

		public int getCount() {
			return mFilesThumb.size();
		}

		public String getItem(int position) {
			return mFilesThumb.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ImageView imageView;
			if (convertView == null) {
				imageView = new ImageView(mContext);
				// imageView.setLayoutParams(new
				// GridView.LayoutParams(mColWidth, mColWidth));
				imageView.setLayoutParams(new GridView.LayoutParams(mColWidth,
						mColWidth));
				imageView.setAdjustViewBounds(false);
				imageView.setScaleType(ScaleType.CENTER_CROP);
				imageView.setPadding(4, 4, 4, 4);
			} else {
				imageView = (ImageView) convertView;
			}

			// imageView.setImageBitmap(mBitmap.get(position));
			Picasso.with(Gallery.this).load(new File(getItem(position)))
					.resize(mColWidth, mColWidth).centerCrop().into(imageView);
			return imageView;
		}

		private Context mContext;
	}

	/*
	 * public static String doFileUpload(String filename, boolean last) {
	 * HttpURLConnection conn = null; DataOutputStream dos = null;
	 * DataInputStream inStream = null; String exsistingFileName = filename;
	 * String str = null;
	 * 
	 * // Is this the place are you doing something wrong. String lineEnd =
	 * "\r\n"; String twoHyphens = "--"; String boundary = "*****";
	 * 
	 * int bytesRead, bytesAvailable, bufferSize; byte[] buffer; int
	 * maxBufferSize = 1 * 1024 * 1024; String responseFromServer = ""; String
	 * urlString; if (last) urlString =
	 * "http://www.allenchan.net/android/Camera3D/handle_upload_last.php"; else
	 * urlString =
	 * "http://www.allenchan.net/android/Camera3D/handle_upload.php";
	 * 
	 * try { // ------------------ CLIENT REQUEST Log.e("Inside second Method");
	 * FileInputStream fileInputStream = new FileInputStream(new File(
	 * exsistingFileName));
	 * 
	 * // open a URL connection to the Servlet URL url = new URL(urlString);
	 * 
	 * // Open a HTTP connection to the URL conn = (HttpURLConnection)
	 * url.openConnection();
	 * 
	 * // Allow Inputs conn.setDoInput(true);
	 * 
	 * // Allow Outputs conn.setDoOutput(true);
	 * 
	 * // Don't use a cached copy. conn.setUseCaches(false);
	 * 
	 * // Use a post method. conn.setRequestMethod("POST");
	 * conn.setRequestProperty("Connection", "Keep-Alive");
	 * conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" +
	 * boundary);
	 * 
	 * dos = new DataOutputStream(conn.getOutputStream());
	 * dos.writeBytes(twoHyphens + boundary + lineEnd); dos.writeBytes(
	 * "Content-Disposition: form-data; name=\"uploadedfile\"; filename=\"" +
	 * exsistingFileName + "\"" + lineEnd); dos.writeBytes(lineEnd);
	 * Log.e("Headers are written");
	 * 
	 * // create a buffer of maximum size bytesAvailable =
	 * fileInputStream.available(); bufferSize = Math.min(bytesAvailable,
	 * maxBufferSize); buffer = new byte[bufferSize];
	 * 
	 * // read file and write it into form... bytesRead =
	 * fileInputStream.read(buffer, 0, bufferSize);
	 * 
	 * while (bytesRead > 0) { dos.write(buffer, 0, bufferSize); bytesAvailable
	 * = fileInputStream.available(); bufferSize = Math.min(bytesAvailable,
	 * maxBufferSize); bytesRead = fileInputStream.read(buffer, 0, bufferSize);
	 * }
	 * 
	 * // send multipart form data necesssary after file data...
	 * dos.writeBytes(lineEnd); dos.writeBytes(twoHyphens + boundary +
	 * twoHyphens + lineEnd);
	 * 
	 * // close streams Log.e("File is written"); fileInputStream.close();
	 * dos.flush(); dos.close();
	 * 
	 * } catch (MalformedURLException ex) { Log.e("error: " + ex.getMessage(),
	 * ex); } catch (IOException ioe) { Log.e("error: " + ioe.getMessage(),
	 * ioe); }
	 * 
	 * // ------------------ read the SERVER RESPONSE try { inStream = new
	 * DataInputStream(conn.getInputStream());
	 * 
	 * while ((str = inStream.readLine()) != null) { Log.e("Server Response" +
	 * str); }
	 * 
	 * inStream.close(); } catch (IOException ioex) { Log.e("error: " +
	 * ioex.getMessage(), ioex); } return str; }
	 */
}
