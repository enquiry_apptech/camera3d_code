package com.siulun.Camera3D;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;

public class ImageItem {
    private static ImageItem sInstance;
//    private Context mContext;

	private final static String LEFTLEFT = Camera3D.FILE_PREFIX + "LL.jpg";
	private final static String LEFT = Camera3D.FILE_PREFIX + "L.jpg";
	private final static String CENTER = Camera3D.FILE_PREFIX + "C.jpg";
	private final static String RIGHT = Camera3D.FILE_PREFIX + "R.jpg";
	private final static String RIGHTRIGHT = Camera3D.FILE_PREFIX + "RR.jpg";

	private static ArrayList<File> mCache = new ArrayList<File>();
	
	private static int mWidth = 800;
	private boolean mHasCache = false;
    private String mAbsPath;
    private File mCacheDir;
    
    public static ImageItem getInstance(Context c) {
        if (sInstance == null) {
            sInstance = new ImageItem(c.getApplicationContext());
        }
        return sInstance;
    }
    
    protected void finalize ()  {
        mBmp.recycle();
    }
    
    private ImageItem(Context c) {
    //    mContext = c;
        mAbsPath = Utils.getAbsPath();
		
        mCacheDir = Utils.getExternalCacheDir(c);
		if (!mCacheDir.exists()) {
			mCacheDir.mkdir();
		}
        //mCount = 0;
        mHasCache = false;
    }

    final Handler mHandler = new Handler();

    /*
    final Runnable mUpdateResults = new Runnable() {
        public void run() {
        }
    };*/
    private Bitmap mBmp;
    private int mPos;
    public void add(Bitmap bmp, int pos) {
		
    	mBmp = bmp;
    	mPos = pos;
		//mImage[pos] = bmp;
        Thread t = new Thread() {
            public void run() {
            	SaveImage();
                //mHandler.post(mUpdateResults);
            }
        };
        t.start();
//        mSaveImage.executeOnExecutor(exec, bmp);
    }
    
    public void add(final byte[] data, int pos) {
		
    	mPos = pos;
		//mImage[pos] = bmp;
        Thread t = new Thread() {
            public void run() {
            	SaveImage(data);
            }
        };
        t.start();
    }
    
//    AsyncTask<Bitmap, Void, Void> mSaveImage = new AsyncTask<Bitmap, Void, Void>() {
//
//		@Override
//		protected Void doInBackground(Bitmap... params) {
//			Bitmap bmp = params[0];
//        	SaveImage(bmp);
//			return null;
//		}
//		
//	};
    
    private String SaveImage(byte[] data){
		File f = new File(mCacheDir, Camera3D.FILE_PREFIX + Utils.addZero(mPos, 2) + ".jpg");
		try {
			/*
			if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
				String storage = Environment.getExternalStorageDirectory().toString();
				StatFs fs = new StatFs(storage);
				long available = fs.getAvailableBlocks()*fs.getBlockSize();
				if(available<data.length){
					return null;
				}
			}*/
			if(!f.exists())
				f.createNewFile();
    		mCache.add(f);
			FileOutputStream fos = new FileOutputStream(f);
			fos.write(data);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return f.getAbsolutePath();
    }
    
    private void SaveImage(){
    	try {
    		File f = new File(mCacheDir, Camera3D.FILE_PREFIX + Utils.addZero(mPos, 2) + ".jpg");
    		mCache.add(f);
			FileOutputStream bos = new FileOutputStream(mCache.get(mPos));
			boolean success = mBmp.compress(Bitmap.CompressFormat.JPEG, 100, bos);

			bos.flush();
			bos.close();
			if (mBmp != null) {
				mBmp.recycle();
				mBmp = null;
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	public CharSequence SaveAllImage(){
    	try {
    		
    		File f = new File(mAbsPath, Camera3D.FOLDER_NAME);
    		if (!f.exists()) {
    			f.mkdir();
    		}
    		File thumb_f = new File(mAbsPath, Camera3D.FOLDER_NAME_THUMB);
    		if (!thumb_f.exists()) {
    			thumb_f.mkdir();
    		}
    		
    		File n = null;
    		FileOutputStream bos = null;
    		CharSequence dateStr = android.text.format.DateFormat.format("yyyyMMdd_kkmmss", new java.util.Date());
    		Bitmap scaled;
            //mImageCount = mImageItem.check();
    		int count = mCache.size();
    		for (int pos = 0; pos <count; pos ++) {
    			
				n = new File(f, Camera3D.FILE_PREFIX + dateStr + "_" + Utils.addZero(pos, 2) + ".jpg");
				bos = new FileOutputStream(n);
				//scaled = Bitmap.createScaledBitmap(mImageItem.get(pos), 800, 600, true);
				//scaled.compress(Bitmap.CompressFormat.JPEG, 85, bos);
				get(pos, 800).compress(Bitmap.CompressFormat.JPEG, 85, bos);
				bos.flush();
				bos.close();
    		}
    		
    		n = new File(thumb_f, Camera3D.FILE_PREFIX + dateStr + "_T" + ".jpg");
    		bos = new FileOutputStream(n);
        	if (mOrientation == TakePhoto.ORIEN_LAND)
        		scaled = Bitmap.createScaledBitmap(get(Camera3D.LEFT), 200, 150, true);	
        	else
        		scaled = Bitmap.createScaledBitmap(get(Camera3D.LEFT), 150, 200, true);	
    		scaled.compress(Bitmap.CompressFormat.JPEG, 60, bos);
    		bos.flush();
    		bos.close();
    		/*
    	    JavaGIFEncoder e = new JavaGIFEncoder();
    		n = new File(f, Camera3D.FILE_PREFIX + dateStr + ".gif");
    		bos = new FileOutputStream(n.getAbsolutePath());
    	    e.start(bos);
    	    e.setSize(400, 300);
    	    e.setDelay(100);   // 1 frame per sec
    	    e.setRepeat(0);
    		scaled = Bitmap.createScaledBitmap(get(Camera3D.LEFT), 400, 300, true);
    	    e.addFrame(scaled);
    	    scaled.recycle();
    		scaled = Bitmap.createScaledBitmap(get(Camera3D.LEFTLEFT), 400, 300, true);
    	    e.addFrame(scaled);
    	    scaled.recycle();
    	    e.finish();
    	    System.gc();
    		bos.flush();
    		bos.close();
    	    */
    	    scaled.recycle();
    	    return dateStr;
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
		return null;
	}
	
    public void clear() {
    	if (mCache != null) {
        	delete_photo();
    		mCache.clear();
    	}
    	//mCount = 0;
    	/*
		int len = mImage.length;
        for (int x=0; x<len; x++) {
        	mImage[x].recycle();
        }
		mImage = new Bitmap[Camera3D.MAX_VIEW];*/    	
    }
    
    private boolean delete_photo() {

        File f = new File(mCacheDir, LEFT);
		if (f.exists()) {
	    	BitmapUtils.delete_file(mCacheDir + "/" + LEFTLEFT);  
	    	BitmapUtils.delete_file(mCacheDir + "/" + LEFT); 
	    	BitmapUtils.delete_file(mCacheDir + "/" + CENTER); 
	    	BitmapUtils.delete_file(mCacheDir + "/" + RIGHT);
	    	BitmapUtils.delete_file(mCacheDir + "/" + RIGHTRIGHT);
		} else {
			boolean done = false;
			int count = 0; 
			do {
				done = BitmapUtils.delete_file(mCacheDir + "/" + Camera3D.FILE_PREFIX + Utils.addZero( count, 2) + ".jpg");
				count++;
			} while(done);
		}
    	return true;    	
    }
    
    public void deleteCache() {
    	mHasCache = false;
    	if (mCache == null) return;
    	int len = mCache.size();
    	for (int x=0; x<len; x++)
    		BitmapUtils.delete_file(mCache.get(x));
    }

    public int getImageCount() {
    	if (mCache != null)
    		return mCache.size();
		return 0;
    	/*
    	int x;
    	for (x=0; x<Camera3D.MAX_VIEW;x++)
    		if (mImage[x] == null)    			
    			return x;
    	return x;*/
    }
    
    public Bitmap get(int pos) {
    	return decodeFile(mCache.get(pos), mWidth);
    	//return mImage[pos];
    }
    
    public Bitmap get(int pos, int width) {
    	return decodeFile(mCache.get(pos), width);
    	//return mImage[pos];
    }
    
    public void setColWidth(int w){
    	mWidth = w;
    }
    
    public void setHasCache(boolean b){
    	mHasCache = b;
    }

    public boolean getHasCache(){
    	return mHasCache;
    }
    private int mOrientation; 
    public int getOrientation() {
    	return mOrientation;
    }
    
    public void setOrientation(int o) {
    	mOrientation = o;
    	if (mOrientation == TakePhoto.ORIEN_LAND || mOrientation == TakePhoto.ORIEN_REV_LAND)
    		mWidth = (int) TakePhoto.LAND_WIDTH / 2;
    	else
    		mWidth = (int) TakePhoto.PORT_WIDTH / 2;
    }
    
	public Bitmap decodeFile(File file, int maxSize){
	    try {
	        //Decode image size
	        BitmapFactory.Options o = new BitmapFactory.Options();
	        o.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(new FileInputStream(file), null, o);
	        int scale = 1;
	        if (o.outHeight > maxSize || o.outWidth > maxSize) {
	            scale = (int) Math.pow(2, (int) Math.round(Math.log(maxSize / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
	        }
	        
	        //Decode with inSampleSize
	        BitmapFactory.Options o2 = new BitmapFactory.Options();
	        o2.inSampleSize=scale;
	        return BitmapFactory.decodeStream(new FileInputStream(file), null, o2);
	    } catch (FileNotFoundException e) {
	    	//Log.e(e.toString());
		    return null;
	    }
	}
}
