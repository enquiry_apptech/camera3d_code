package com.siulun.Camera3D;

import java.lang.reflect.Method;
import java.util.List;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.Camera.Size;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;

public class TakePhoto extends Activity implements SurfaceHolder.Callback,
		OnClickListener {

	private Camera mCamera;
	private ImageButton mBtnBurst, mBtnTake, mBtnFocus, mBtnFlash;
	private Button mBtnSave, mBtnCancel, mBtnVib, mBtnFirst;
	private SurfaceView mSurfaceView;
	private SurfaceHolder holder;
	private AutoFocusCallback mAutoFocusCallback = new AutoFocusCallback();
	private Bitmap bmp, mFirstBmp;
	private int mPos = 0, mAlpha, mFlashState, mDisplayWidth = 0,
			mDisplayHeight = 0;
	private ImageItem mImageItem;
	private ImageView mImageOverlay, mIndicator, mFocus;
	private TextView mTextAlpha, mTextFrame;
	private boolean mBurstModeOn = false;
	private int mOffset = 0;

	public static final int ACTIVITY_DELETE = 0x1001;
	private static final int DIALOG_MENU_ABOUT = 0;
	private static final int FLASH_OFF = 0;
	private static final int FLASH_AUTO = 1;
	private static final int FLASH_ON = 2;
	private static final int FLASH_TORCH = 3;

	private boolean mFlashAvaliable, mAutoFocusAvaliable, mPreviewSupport,
			mOverlayFirst, mHardKey;
	private Size mOptimalSize = null;

	private GestureDetector detector;
	private myGestureListener gListener;
	private SharedPreferences mPrefs;
	private int mTryCount = 0;
	private final Handler mHandler = new Handler();
	private int mW, mH;
	private boolean mFlashOff = false, mFlashOn = false, mFlashAuto = false,
			mFlashTorch = false;

	private OrientationEventListener myOrientationEventListener;
	public static final int ORIEN_LAND = 2;
	public static final int ORIEN_PORT = 1;
	public static final int ORIEN_REV_LAND = 3;
	private int myOrientation = ORIEN_PORT;
	public final static int LAND_WIDTH = 1024;
	public final static int LAND_HEIGHT = 768;
	public final static int PORT_WIDTH = 768;
	public final static int PORT_HEIGHT = 1024;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		// this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		setContentView(R.layout.camera);

		try {
			Method HasSystemFeature = PackageManager.class.getMethod(
					"hasSystemFeature", new Class[] { String.class });
			mFlashAvaliable = CameraParameters
					.getFlashSupport(getBaseContext());
			mAutoFocusAvaliable = CameraParameters
					.getAutoFocusSupport(getBaseContext());
		} catch (NoSuchMethodException nsme) {
			mFlashAvaliable = false;
			mAutoFocusAvaliable = false;
		}

		if (!Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState())) {

			Toast.makeText(TakePhoto.this, getText(R.string.toast_ext_storage),
					Toast.LENGTH_LONG).show();

			finish();
		}
		mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		if (mPrefs != null) {
			mFlashState = mPrefs.getInt("flashstate", 0);
			mAlpha = mPrefs.getInt("alpha", 100);
		}

		DisplayMetrics metrics = getResources().getDisplayMetrics();

		mDisplayHeight = metrics.heightPixels;
		mDisplayWidth = metrics.widthPixels;
		// mDisplayWidth = (metrics.heightPixels / 9) * 16;
		// if (mDisplayWidth > metrics.widthPixels) {
		// mDisplayWidth = metrics.widthPixels;
		// mDisplayHeight = (metrics.widthPixels / 16) * 9;
		// }

		mTextFrame = (TextView) findViewById(R.id.mFrame);
		mTextAlpha = (TextView) findViewById(R.id.mAlpha);
		mTextAlpha.setText("Alpha Overlay: " + mAlpha);
		mImageItem = ImageItem.getInstance(this);
		mImageItem.setColWidth(mDisplayWidth);
		mSurfaceView = (SurfaceView) findViewById(R.id.mSurfaceView);
		FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
				metrics.widthPixels, metrics.widthPixels);
		mSurfaceView.setLayoutParams(layoutParams);
		mImageOverlay = (ImageView) findViewById(R.id.mImageView);
		mImageOverlay.setScaleType(ImageView.ScaleType.FIT_XY);
		mImageOverlay.setLayoutParams(layoutParams);

		mIndicator = (ImageView) findViewById(R.id.mIndicator);
		mFocus = (ImageView) findViewById(R.id.mFocus);
		mFocus.setVisibility(View.GONE);
		// mIndicator.setAlpha(120);
		// mCachePath = ;

		holder = mSurfaceView.getHolder();
		holder.addCallback(TakePhoto.this);
		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		mPos = 0;

		mBtnBurst = (ImageButton) findViewById(R.id.mBtnBurst);
		mBtnTake = (ImageButton) findViewById(R.id.mBtnTake);
		mBtnFocus = (ImageButton) findViewById(R.id.mBtnFocus);
		mBtnSave = (Button) findViewById(R.id.mBtnSave);
		mBtnVib = (Button) findViewById(R.id.mBtnVib);
		mBtnCancel = (Button) findViewById(R.id.mBtnCancel);
		mBtnFirst = (Button) findViewById(R.id.mBtnFirst);
		mBtnFlash = (ImageButton) findViewById(R.id.mBtnFlash);
		gListener = new myGestureListener();
		detector = new GestureDetector(this, gListener);

		if (mFlashAvaliable) {
			mBtnFlash.setOnClickListener(this);
		} else {
			mBtnFlash.setVisibility(View.INVISIBLE);
		}
		mBtnBurst.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					mBurstModeOn = !mBurstModeOn;
					if (mBurstModeOn)
						mBtnFocus.performClick();
				}
				return true;
			}
		});
		mBtnSave.setOnClickListener(this);
		mBtnTake.setOnClickListener(this);
		mBtnFocus.setOnClickListener(this);
		mBtnVib.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		mBtnFirst.setOnClickListener(this);

		mBtnCancel.setVisibility(View.GONE);
		mBtnSave.setVisibility(View.GONE);
		mBtnVib.setVisibility(View.GONE);
		mBtnFirst.setVisibility(View.GONE);
		mOverlayFirst = false;

		myOrientationEventListener = new OrientationEventListener(this,
				SensorManager.SENSOR_DELAY_NORMAL) {

			@Override
			public void onOrientationChanged(int arg0) {
				// Log.v("Orientation: " + String.valueOf(arg0));
				if (myOrientation != ORIEN_PORT && (arg0 >= 315 && arg0 <= 360)
						|| (arg0 > 0 && arg0 <= 180)) {
					// Animation ccw=
					//
					// AnimationUtils.loadAnimation(getBaseContext(),
					// R.anim.rotate_ccw);
					// ccw.setFillAfter(true);
					// mBtnTake.startAnimation(ccw);
					// mBtnFocus.startAnimation(ccw);
					// if (mPos == 0) {
					// ccw=
					//
					// AnimationUtils.loadAnimation(getBaseContext(),
					// R.anim.rotate_ccw_a);
					// ccw.setFillAfter(true);
					// }
					// mBtnCancel.startAnimation(ccw);
					// mBtnPreview.startAnimation(ccw);
					// myOrientation = ORIEN_PORT;
					fixRatio = false;
					mHandler.removeCallbacks(mChangeOrientation);
					// } else if (myOrientation != ORIEN_REV_LAND
					// && (arg0 > 0 && arg0 <= 180)) {
					// myOrientation = ORIEN_REV_LAND;
					// fixRatio = false;
					mChangeStatus = true;
				} else if (myOrientation != ORIEN_LAND
						&& (arg0 > 180 && arg0 < 315)) {
					// Animation cw=
					//
					// AnimationUtils.loadAnimation(getBaseContext(),
					// R.anim.rotate_cw);
					// cw.setFillAfter(true);
					// mBtnTake.startAnimation(cw);
					// mBtnFocus.startAnimation(cw);
					// if (mPos == 0) {
					// cw=
					//
					// AnimationUtils.loadAnimation(getBaseContext(),
					// R.anim.rotate_cw_a);
					// cw.setFillAfter(true);
					// }
					// mBtnCancel.startAnimation(cw);
					// mBtnPreview.startAnimation(cw);
					// myOrientation = ORIEN_LAND;
					fixRatio = false;
					if (mChangeStatus) {
						mHandler.postDelayed(mChangeOrientation, 1000);
						mChangeStatus = false;
					}

					// Intent i = new Intent(TakePhoto.this,
					// TakePhotoLand.class);
					// startActivity(i);
					// finish();
				}
			}
		};

		if (myOrientationEventListener.canDetectOrientation()) {
			myOrientationEventListener.enable();
		} else {
			Toast.makeText(this, "Can't DetectOrientation", Toast.LENGTH_LONG)
					.show();
		}
	}

	boolean mChangeStatus = true;
	Runnable mChangeOrientation = new Runnable() {

		@Override
		public void run() {
			Intent i = new Intent(TakePhoto.this, TakePhotoLand.class);
			startActivity(i);
			finish();
		}
	};

	@Override
	public void onClick(View v) {
		int id = v.getId();
		Intent i = new Intent(new Intent(getBaseContext(), Preview3D.class));
		switch (id) {
		case (R.id.mBtnFlash):
			Camera.Parameters parameters = mCamera.getParameters();
			switch (mFlashState) {
			case (FLASH_TORCH):
				if (mFlashAuto) {
					mFlashState = FLASH_AUTO;
					mBtnFlash.setImageResource(R.drawable.flash_auto);
					parameters = setFlash(FLASH_AUTO, parameters);
					break;
				}
			case (FLASH_AUTO):
				if (mFlashOn) {
					mFlashState = FLASH_ON;
					mBtnFlash.setImageResource(R.drawable.flash_on);
					parameters = setFlash(FLASH_ON, parameters);
					break;
				}
			case (FLASH_ON):
				if (mFlashOff) {
					mFlashState = FLASH_OFF;
					mBtnFlash.setImageResource(R.drawable.flash_off);
					parameters = setFlash(FLASH_OFF, parameters);
					break;
				}
			case (FLASH_OFF):
				if (mFlashTorch) {
					mFlashState = FLASH_TORCH;
					mBtnFlash.setImageResource(R.drawable.flash_torch);
					parameters = setFlash(FLASH_TORCH, parameters);
					break;
				} else {
					mFlashState = FLASH_AUTO;
					mBtnFlash.setImageResource(R.drawable.flash_auto);
					parameters = setFlash(FLASH_AUTO, parameters);
					break;
				}
			}
			mCamera.setParameters(parameters);
			break;
		case (R.id.mBtnSave):
			mBurstModeOn = false;
			mPos = 0;
			mIndicator.setImageResource(R.drawable.s1);
			mImageOverlay.setVisibility(View.GONE);
			mBtnCancel.setVisibility(View.GONE);
			mBtnVib.setVisibility(View.GONE);
			mBtnSave.setVisibility(View.GONE);
			mTextAlpha.setVisibility(View.GONE);
			mBtnFirst.setVisibility(View.GONE);
			mImageOverlay.setImageBitmap(null);
			if (bmp != null) {
				bmp.recycle();
				bmp = null;
			}
			if (mFirstBmp != null) {
				mFirstBmp.recycle();
				mFirstBmp = null;
			}
			i.putExtra("isLand", false);
			startActivity(i);
			break;
		case (R.id.mBtnTake):
			/*
			 * try { parameters = mCamera.getParameters();
			 * Camera.Parameters.class.getMethod("getSupportedFocusModes", new
			 * Class[] { }); parameters =
			 * CameraParameters.setFocusModeInfinity(parameters);
			 * mCamera.setParameters(parameters); } catch (NoSuchMethodException
			 * nsme) { }
			 */
			// if (mBurstModeOn) {
			// mBurstModeOn = false;
			// return;
			// }
			mBtnTake.setEnabled(false);
			mBtnFocus.setEnabled(false);
			takePicture();
			break;
		case (R.id.mBtnFocus):
			/*
			 * try { parameters = mCamera.getParameters();
			 * Camera.Parameters.class.getMethod("getSupportedFocusModes", new
			 * Class[] { }); parameters =
			 * CameraParameters.setFocusModeAuto(parameters);
			 * mCamera.setParameters(parameters); } catch (NoSuchMethodException
			 * nsme) { }
			 */
			mBtnTake.setEnabled(false);
			mBtnFocus.setEnabled(false);
			if (mAutoFocusAvaliable) {
				try {
					mCamera.autoFocus(mAutoFocusCallback);
				} catch (RuntimeException e) {
					e.printStackTrace();
					takePicture();
				}
			} else
				takePicture();
			break;
		case (R.id.mBtnVib):
			mPos = 0;
			mTextFrame.setText("" + mPos);
			mImageOverlay.setVisibility(View.GONE);
			mBtnCancel.setVisibility(View.GONE);
			mBtnVib.setVisibility(View.GONE);
			mBtnSave.setVisibility(View.GONE);
			mBtnFirst.setVisibility(View.GONE);
			mImageOverlay.setImageBitmap(null);
			if (bmp != null) {
				bmp.recycle();
				bmp = null;
			}
			if (mFirstBmp != null) {
				mFirstBmp.recycle();
				mFirstBmp = null;
			}
			i.putExtra("isLand", false);
			startActivity(i);
			break;
		case (R.id.mBtnCancel):
			mBurstModeOn = false;
			mBtnCancel.setVisibility(View.GONE);
			mImageOverlay.setVisibility(View.GONE);
			mBtnVib.setVisibility(View.GONE);
			mBtnSave.setVisibility(View.GONE);
			mTextAlpha.setVisibility(View.GONE);
			mBtnFirst.setVisibility(View.GONE);
			mIndicator.setImageResource(R.drawable.s1);
			mImageItem.clear();
			mImageOverlay.setImageBitmap(null);
			if (bmp != null) {
				bmp.recycle();
				bmp = null;
			}
			if (mFirstBmp != null) {
				mFirstBmp.recycle();
				mFirstBmp = null;
			}
			mPos = 0;
			mTextFrame.setText("" + mPos);
			myOrientationEventListener.enable();
			// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
			break;
		case (R.id.mBtnFirst):
			if (mOverlayFirst) {
				mImageOverlay.setImageBitmap(bmp);
				mBtnFirst.setText(getText(R.string.str_last));
				mOverlayFirst = false;
			} else {
				mImageOverlay.setImageBitmap(mFirstBmp);
				mBtnFirst.setText(getText(R.string.str_first));
				mOverlayFirst = true;
			}
			break;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_CAMERA
				|| keyCode == KeyEvent.KEYCODE_SEARCH) {
			mBtnTake.setEnabled(false);
			mBtnFocus.setEnabled(false);
			if (mHardKey) {
				takePicture();
				return (true);
			}
			if (mAutoFocusAvaliable) {
				mHardKey = false;
				mCamera.autoFocus(mAutoFocusCallback);
			} else
				takePicture();
			return (true);
		} else if (keyCode == KeyEvent.KEYCODE_FOCUS) {
			if (mAutoFocusAvaliable) {
				mHardKey = true;
				mCamera.autoFocus(mAutoFocusCallback);
			} else
				takePicture();
		}
		return (super.onKeyDown(keyCode, event));
	}

	@Override
	public void surfaceCreated(SurfaceHolder surfaceholder) {
		// try {
		try{
		 if (mCamera == null) {
			 mCamera = Camera.open();
   		     mCamera.setDisplayOrientation(90);
		 }
		} catch (Exception e) {
			mTryCount = 0;
			CameraReconnect();
		}
		// } catch (Exception e) {
		// mTryCount = 0;
		// CameraReconnect();
		// if (mTryCount > 10)
		// return;
		// }
		// try {
		// mCamera.setPreviewDisplay(holder);
		// mCamera.setPreviewCallback(new PreviewCallback() {
		//
		// public void onPreviewFrame(byte[] data, Camera camera) {
		// Log.v("onPreviewFrame called at: "
		// + System.currentTimeMillis());
		// mSurfaceView.invalidate();
		// }
		// });
		// } catch (IOException exception) {
		// cameraNotFound();
		// return;
		// } catch (Exception exception) {
		// cameraNotFound();
		// return;
		// }
	}

	private void cameraNotFound() {
		mCamera.stopPreview();
		mCamera.setPreviewCallback(null);
		mCamera.release();
		mCamera = null;
		Toast.makeText(TakePhoto.this, getText(R.string.toast_no_camera),
				Toast.LENGTH_LONG).show();
		finish();
	}

	private void CameraReconnect() {
		mTryCount++;
		if (mTryCount > 10) {
			cameraNotFound();
			return;
		}
		try {
			if (mCamera != null) {
				mCamera.stopPreview();
				mCamera.setPreviewCallback(null);
				mCamera.release();
				mCamera = null;
			}
			mCamera = null;
			mCamera = Camera.open();
		} catch (Exception e) {
			e.printStackTrace();
			CameraReconnect();
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder surfaceholder, int format, int w,
			int h) {
		mW = w;
		mH = h;
		InitCameraTask initc = new InitCameraTask();
		initc.execute();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder surfaceholder) {
		stopCamera();
		if (mCamera != null) {
			mCamera.stopPreview();
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}

	private void takePicture() {
		if (mCamera != null) {
			try {
				mCamera.takePicture(shutterCallback, rawCallback, jpegCallback);
			} catch (RuntimeException e) {
				Toast.makeText(TakePhoto.this,
						getText(R.string.toast_no_camera), Toast.LENGTH_LONG)
						.show();
				finish();
			}
		}
	}

	private ShutterCallback shutterCallback = new ShutterCallback() {
		public void onShutter() {

		}
	};

	private PictureCallback rawCallback = new PictureCallback() {
		public void onPictureTaken(byte[] _data, Camera _camera) {

		}
	};

	private PictureCallback jpegCallback = new PictureCallback() {
		public void onPictureTaken(byte[] _data, Camera _camera) {

			try {
				// stopCamera();
				mCamera.stopPreview();

				if (mPos == 0) {
					mImageItem.deleteCache();
					mImageItem.setOrientation(myOrientation);
					myOrientationEventListener.disable();
					// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
				}
				Bitmap resizedBitmap = null;
				if (myOrientation == TakePhoto.ORIEN_PORT) {
					Bitmap pbmp = decodeByte(_data, LAND_WIDTH);

					resizedBitmap = scaleBitmap(pbmp, Camera3D.MAX_3d_LENGTH,
							true);
					if (resizedBitmap != pbmp) {
						pbmp.recycle();
					}
					mImageItem.add(resizedBitmap, mPos);
					
					bmp = decodeByte(_data, (int) ((float) mDisplayWidth * .8f));
					bmp = scaleBitmap(bmp, (int) ((float) mDisplayWidth * .8f),
							true);
				} else {
					mImageItem.add(_data, mPos);
					bmp = decodeByte(_data, (int) ((float) mDisplayWidth * .8f));
				}

				if (bmp != null) {
					mPos++;
				}

				if (mPos == 99) {
					mPos = 0;
					mIndicator.setImageResource(R.drawable.s1);
					mImageOverlay.setVisibility(View.GONE);
					mBtnCancel.setVisibility(View.GONE);
					mBtnVib.setVisibility(View.GONE);
					mBtnSave.setVisibility(View.GONE);
					mTextAlpha.setVisibility(View.GONE);
					mBtnFirst.setVisibility(View.GONE);
					Intent i = new Intent(getBaseContext(), Preview3D.class);
					i.putExtra("isLand", false);
					startActivity(i);
				} else {
					mImageOverlay.setImageBitmap(bmp);
					mImageOverlay.setVisibility(View.VISIBLE);
					mImageOverlay.setAlpha(mAlpha);
					switch (mPos) {
					case 0:
						break;
					case 1:
						
						try { 
							mFirstBmp = bmp.copy(bmp.getConfig(), true);
						} catch (OutOfMemoryError e) {
							//scale down the bmp to use less memory
							Bitmap scale_down = Bitmap.createScaledBitmap(bmp, bmp.getWidth()/2, bmp.getHeight()/2, true);
							mFirstBmp = scale_down.copy(scale_down.getConfig(), true);
							scale_down.recycle();
							
						}
						mTextAlpha.setVisibility(View.VISIBLE);
						mIndicator.setImageResource(R.drawable.s2);
						mBtnCancel.setVisibility(View.VISIBLE);
						break;
					case 2:
						mBtnFirst.setVisibility(View.VISIBLE);
						mIndicator.setImageResource(R.drawable.s3);
						mBtnVib.setVisibility(View.VISIBLE);
						break;
					case 3:
						mIndicator.setImageResource(R.drawable.s4);
						mBtnVib.setVisibility(View.GONE);
						mBtnSave.setVisibility(View.VISIBLE);
						break;
					default:
						mIndicator.setImageResource(R.drawable.s5);
						break;
					}
					// initCamera();
					mCamera.startPreview();
				}
				mTextFrame.setText("" + mPos);
				mHandler.postDelayed(restartCameraButton, 100);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	private Bitmap scaleBitmap(Bitmap source, int maxLength, boolean rotate) {
		Matrix matrix = new Matrix();

		float newWidth, newHeight;
		if (source.getWidth() > source.getHeight()) {
			newWidth = Camera3D.MAX_3d_LENGTH;
			newHeight = (int) ((float) source.getHeight()
					/ (float) source.getWidth() * (float) newWidth);
		} else {
			newHeight = Camera3D.MAX_3d_LENGTH;
			newWidth = (int) ((float) source.getWidth()
					/ (float) source.getHeight() * (float) newHeight);
		}

		float scaleWidth = (float) ((float) newWidth / (float) source
				.getWidth());
		float scaleHeight = (float) ((float) newHeight / (float) source
				.getHeight());

		matrix.postScale(scaleWidth, scaleHeight);
		if (rotate)
			matrix.postRotate(90);

		return Bitmap.createBitmap(source, 0, 0, source.getWidth(),
				source.getHeight(), matrix, true);
	}

	final Runnable restartCameraButton = new Runnable() {
		@Override
		public void run() {
			mBtnTake.setEnabled(true);
			mBtnFocus.setEnabled(true);
			mFocus.setVisibility(View.GONE);
			if (mBurstModeOn) {
				mBtnTake.performClick();
			}
		}
	};

	public final class AutoFocusCallback implements
			android.hardware.Camera.AutoFocusCallback {
		public void onAutoFocus(boolean focused, Camera camera) {

			if (camera == null)
				return;

			mFocus.setVisibility(View.VISIBLE);
			if (mHardKey) {
				mFocus.setImageResource(R.drawable.focus_green);
				mHandler.removeCallbacks(mOffFocus);
				return;
			}
			if (focused) {
				mFocus.setImageResource(R.drawable.focus_green);
				mHandler.removeCallbacks(mOffFocus);
				takePicture();
			} else {
				mBtnTake.setEnabled(true);
				mBtnFocus.setEnabled(true);
				mFocus.setImageResource(R.drawable.focus_red);
				mHandler.postDelayed(mOffFocus, 2000);
			}
		}
	};

	final Runnable mOffFocus = new Runnable() {
		public void run() {
			mFocus.setVisibility(View.GONE);
		}
	};

	private boolean fixRatio = false;

	private class InitCameraTask extends AsyncTask<String, Integer, String> {
		protected String doInBackground(String... param) {
			return "";
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			initCamera();
		}
	}

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	private void initCamera() {
		if (mCamera == null) {
			mCamera = Camera.open();
		}

		if (mCamera != null) {
			try {
				Camera.Parameters parameters = mCamera.getParameters();
				if (Utils.hasJPEGFormat())
					parameters.setPictureFormat(ImageFormat.JPEG);
				else
					parameters.setPictureFormat(PixelFormat.JPEG);
				// if (Utils.hasCameraFacing())
				// parameters.set("camera-id", CameraInfo.CAMERA_FACING_BACK);
				// else
				// parameters.set("camera-id", 0);

				try { // check if method is available
					Camera.Parameters.class.getMethod(
							"getSupportedPictureSizes", new Class[] {});
					parameters = CameraParameters.setPictureMaxSize(parameters);
				} catch (NoSuchMethodException nsme) {
					nsme.printStackTrace();
				}

				// parameters.setPreviewSize(500, 500);

				DisplayMetrics metrics = getResources().getDisplayMetrics();

				mDisplayHeight = metrics.heightPixels;
				mDisplayWidth = metrics.widthPixels;
				Size size = parameters.getPictureSize();
				if (myOrientation == TakePhoto.ORIEN_LAND)
					mDisplayWidth = (int) ((float) mDisplayHeight
							/ (float) size.height * (float) size.width);
				else
					mDisplayHeight = (int) ((float) mDisplayWidth
							* (float) size.width / (float) size.height);

				mImageItem.setColWidth(Math.max(mDisplayHeight, mDisplayWidth));

				if (!fixRatio) {
					FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
							mDisplayWidth, mDisplayHeight);
					layoutParams.gravity = Gravity.CENTER;
					mSurfaceView.setLayoutParams(layoutParams);

					mImageOverlay.setScaleType(ScaleType.FIT_XY);
					mImageOverlay.setAdjustViewBounds(false);
					layoutParams = new FrameLayout.LayoutParams(mDisplayWidth,
							mDisplayHeight - mOffset);
					layoutParams.gravity = Gravity.CENTER;
					mImageOverlay.setLayoutParams(layoutParams);

					fixRatio = true;
					return;
				}

				try { // check if method is available
					Camera.Parameters.class.getMethod("getSupportedFlashModes",
							new Class[] {});
					List<String> modes = CameraParameters
							.getFlashModes(parameters);
					if (modes != null) {
						for (String mode : modes) {
							if (mode.equals("off"))
								mFlashOff = true;
							else if (mode.equals("on"))
								mFlashOn = true;
							else if (mode.equals("auto"))
								mFlashAuto = true;
							else if (mode.equals("torch"))
								mFlashTorch = true;
						}
					} else {
						mFlashOff = true;
						mFlashOn = true;
						mFlashAuto = true;
						mFlashTorch = true;
					}
				} catch (NoSuchMethodException nsme) {
					mFlashOff = true;
					mFlashOn = true;
					mFlashAuto = true;
					mFlashTorch = false;
				} catch (NullPointerException npe) {
					mFlashOff = true;
					mFlashOn = true;
					mFlashAuto = true;
					mFlashTorch = false;
				}

				if (mFlashAvaliable) {
					switch (mFlashState) {
					case (FLASH_OFF):
						mBtnFlash.setImageResource(R.drawable.flash_off);
						parameters = setFlash(FLASH_OFF, parameters);
						break;
					case (FLASH_AUTO):
						mBtnFlash.setImageResource(R.drawable.flash_auto);
						parameters = setFlash(FLASH_AUTO, parameters);
						break;
					case (FLASH_ON):
						mBtnFlash.setImageResource(R.drawable.flash_on);
						parameters = setFlash(FLASH_ON, parameters);
						break;
					case (FLASH_TORCH):
						mBtnFlash.setImageResource(R.drawable.flash_torch);
						parameters = setFlash(FLASH_TORCH, parameters);
						break;
					}
				}
				/*
				 * if (mOptimalSize == null) { try { //check if method is
				 * available
				 * Camera.Parameters.class.getMethod("getSupportedPreviewSizes",
				 * new Class[] { }); mOptimalSize =
				 * CameraParameters.setPreviewSizes(parameters, mW, mH);
				 * parameters.setPreviewSize(mOptimalSize.width,
				 * mOptimalSize.height); mPreviewSupport = true; } catch
				 * (NoSuchMethodException nsme) { parameters.setPreviewSize(mW,
				 * mH); mPreviewSupport = false; } } if (mPreviewSupport)
				 * mImageOverlay.setScaleType(ImageView.ScaleType.CENTER_CROP);
				 * else mImageOverlay.setScaleType(ImageView.ScaleType.FIT_XY);
				 */
				try {
					Camera.Parameters.class.getMethod("getSupportedFocusModes",
							new Class[] {});
					parameters = CameraParameters.setFocusModeAuto(parameters);
				} catch (NoSuchMethodException nsme) {
					nsme.printStackTrace();
				}

				mCamera.setParameters(parameters);

				if (myOrientation == TakePhoto.ORIEN_PORT) {
					mCamera.setDisplayOrientation(90);
					// } else if (myOrientation == TakePhoto.ORIEN_REV_LAND){
					// mCamera.setDisplayOrientation(180);
				}

				mCamera.setPreviewDisplay(holder);
				mCamera.startPreview();
				myOrientationEventListener.enable();
			} catch (Exception e) {
				mCamera.release();
				mCamera = null;
				e.printStackTrace();
			}
		}
	}

	// private Camera openFrontFacingCameraGingerbread() {
	// int cameraCount = 0;
	// Camera cam = null;
	// Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
	// cameraCount = Camera.getNumberOfCameras();
	// for ( int camIdx = 0; camIdx < cameraCount; camIdx++ ) {
	// Camera.getCameraInfo( camIdx, cameraInfo );
	// if ( cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT ) {
	// try {
	// cam = Camera.open( camIdx );
	// } catch (RuntimeException e) {
	// Log.e("Camera failed to open: " + e.getLocalizedMessage());
	// }
	// }
	// }
	// return cam;
	// }

	private Camera.Parameters setFlash(int flashstate,
			Camera.Parameters parameters) {
		switch (flashstate) {
		case (FLASH_OFF):
			try {
				Camera.Parameters.class.getMethod("setFlashMode",
						new Class[] {});
				parameters = CameraParameters.setFlashMode(parameters,
						FLASH_OFF);
			} catch (NoSuchMethodException nsme) {
				parameters.set("flash-mode", "off");
			}
			break;
		case (FLASH_AUTO):
			try {
				Camera.Parameters.class.getMethod("setFlashMode",
						new Class[] {});
				parameters = CameraParameters.setFlashMode(parameters,
						FLASH_AUTO);
			} catch (NoSuchMethodException nsme) {
				parameters.set("flash-mode", "auto");
			}
			break;
		case (FLASH_ON):
			try {
				Camera.Parameters.class.getMethod("setFlashMode",
						new Class[] {});
				parameters = CameraParameters
						.setFlashMode(parameters, FLASH_ON);

			} catch (NoSuchMethodException nsme) {
				parameters.set("flash-mode", "on");
			}
			break;
		case (FLASH_TORCH):
			try {
				Camera.Parameters.class.getMethod("setFlashMode",
						new Class[] {});
				parameters = CameraParameters.setFlashMode(parameters,
						FLASH_TORCH);

			} catch (NoSuchMethodException nsme) {
				parameters.set("flash-mode", "torch");
			}
			break;
		}
		return parameters;
	}

	private void stopCamera() {
		if (mCamera != null) {
			try {
				mCamera.stopPreview();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public Bitmap decodeByte(byte[] _data, int maxSize) {
		// Decode image size
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeByteArray(_data, 0, _data.length, o);
		int scale = 1;
		if (o.outHeight > maxSize || o.outWidth > maxSize) {
			scale = (int) Math.pow(
					2,
					(int) Math.round(Math.log(maxSize
							/ (double) Math.max(o.outHeight, o.outWidth))
							/ Math.log(0.5)));
		}

		// Decode with inSampleSize
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeByteArray(_data, 0, _data.length, o2);
	}

	private void SetPref() {
		if (mPrefs != null) {
			SharedPreferences.Editor ed = mPrefs.edit();
			// ed.putInt("takecount", mTakeCount);
			ed.putInt("flashstate", mFlashState);
			ed.putInt("alpha", mAlpha);
			ed.commit();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		// myOrientationEventListener.enable();
		// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
		// myOrientation = getResources().getConfiguration().orientation;

		// switch (getRotation()) {
		// case Surface.ROTATION_0:
		// mCamera.setDisplayOrientation(90);
		// break;
		// case Surface.ROTATION_90:
		// mCamera.setDisplayOrientation(180);
		// break;
		// case Surface.ROTATION_180:
		// mCamera.setDisplayOrientation(270);
		// break;
		// case Surface.ROTATION_270:
		// // mCamera.setDisplayOrientation(90);
		// break;
		// default:
		// break;
		// }

		mImageItem.clear();
		mPos = 0;
		mTextFrame.setText("" + mPos);
		mImageOverlay.setVisibility(View.GONE);
		mBtnVib.setVisibility(View.GONE);
		mBtnSave.setVisibility(View.GONE);
		mBtnFirst.setVisibility(View.GONE);
		mIndicator.setImageResource(R.drawable.s1);
	}

	public int getRotation() {
		return ((WindowManager) getSystemService(WINDOW_SERVICE))
				.getDefaultDisplay().getRotation();
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this); // Add this method.
		myOrientationEventListener.enable();
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance().activityStop(this); // Add this method.
		myOrientationEventListener.disable();
		mHandler.removeCallbacks(mChangeOrientation);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		SetPref();
		if (bmp != null) {
			bmp.recycle();
			bmp = null;
		}
		if (mFirstBmp != null) {
			mFirstBmp.recycle();
			mFirstBmp = null;
		}
		mHandler.removeCallbacks(mOffFocus);
		myOrientationEventListener.disable();
		mHandler.removeCallbacks(mChangeOrientation);
	}

	// protected static final int MENU_TYPE = Menu.FIRST;
	// protected static final int MENU_PREF = Menu.FIRST+2;
	protected static final int MENU_ABOUT = Menu.FIRST;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// menu.add(0, MENU_NEWPOST, 0,
		// getText(R.string.new_post)).setIcon(R.drawable.ic_menu_compose);
		// menu.add(0, MENU_TYPE, 0,
		// getText(R.string.dialog_view)).setIcon(android.R.drawable.ic_menu_gallery);
		// menu.add(0, MENU_PREF, 0,
		// getText(R.string.set_preferences)).setIcon(android.R.drawable.ic_menu_preferences);
		menu.add(0, MENU_ABOUT, 0, getText(R.string.dialog_about)).setIcon(
				android.R.drawable.ic_menu_info_details);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		switch (item.getItemId()) {
		/*
		 * case MENU_TYPE: Intent i = new Intent(getBaseContext(),
		 * Gallery.class); startActivity(i); break;
		 * 
		 * case MENU_PREF: Intent settingsActivity = new
		 * Intent(getBaseContext(), Preferences.class);
		 * startActivity(settingsActivity); break;
		 */
		case MENU_ABOUT:
			showDialog(DIALOG_MENU_ABOUT);
			break;
		}
		return true;
	}

	@Override
	public Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_MENU_ABOUT:
			final TextView message = new TextView(this);
			SpannableString s = new SpannableString(
					this.getText(R.string.dialog_about_1));
			Linkify.addLinks(s, Linkify.EMAIL_ADDRESSES);
			message.setText(s);
			s = new SpannableString(this.getText(R.string.dialog_about_2));
			Linkify.addLinks(s, Linkify.WEB_URLS);
			message.append(s);
			message.setMovementMethod(LinkMovementMethod.getInstance());

			return new AlertDialog.Builder(this)
					.setTitle(R.string.dialog_about)
					// .setMessage(R.string.dialog_about_detail)
					.setView(message)
					.setPositiveButton(R.string.OK,
							new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface dialoginterface, int i) {
									dialoginterface.cancel();
								}
							}).show();
		}
		return null;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getY() > 90) {
			if (detector.onTouchEvent(event)) {
				return detector.onTouchEvent(event);
			} else {
				return super.onTouchEvent(event);
			}
		} else {
			return false;
		}
	}

	public class myGestureListener implements GestureDetector.OnGestureListener {
		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			if (mPos == 0)
				return false;
			if (distanceX < 0 && mAlpha < 240) {
				mAlpha += 20;
			} else if (distanceX > 0 && mAlpha > 0) {
				mAlpha -= 20;
			}
			mTextAlpha.setText("Alpha Overlay: " + mAlpha);
			mImageOverlay.setAlpha(mAlpha);
			return false;
		}

		@Override
		public boolean onDown(MotionEvent arg0) {
			return false;
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			return false;
		}

		@Override
		public void onLongPress(MotionEvent e) {
			/*
			 * if (bm != null) {
			 * 
			 * }
			 */
		}

		@Override
		public void onShowPress(MotionEvent e) {
		}

		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			return false;
		}
	}
}